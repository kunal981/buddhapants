//
//  LeftTableViewCell.m
//  VIDIT
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ArtpantsCell.h"

@implementation ArtpantsCell
@synthesize uploadImageView,voteBtn,lblVoteValue1,lblVoteValue2,lblVoteValue3,lblVoteValue4,lblVoteValue5;
@synthesize lblTittle,lblUsername,description;


- (void)awakeFromNib {
    
   
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
      
        NSLog(@"%f",self.frame.size.width);
        NSLog(@"%f",self.contentView.frame.size.width-5);
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        
        NSLog(@"%f",width);
        uploadImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, 320)];
        uploadImageView.backgroundColor=[UIColor whiteColor];
       // [uploadImageView setContentMode:UIViewContentModeScaleAspec];
                
        [self addSubview:uploadImageView];

       
        
        CGFloat x=0;
        CGFloat h=355;
                    
            lblVoteValue1=[[UILabel alloc]initWithFrame:CGRectMake(x, h, (width-5)/5, 15)];
            lblVoteValue1.font=[UIFont fontWithName:@"HelveticaNeue-Italic" size:12];
            lblVoteValue1.textColor=[UIColor darkGrayColor];
            lblVoteValue1.tag=1;
            lblVoteValue1.textAlignment=NSTextAlignmentCenter;
        
            
            [self addSubview:lblVoteValue1];
        
        
        lblVoteValue2=[[UILabel alloc]initWithFrame:CGRectMake(lblVoteValue1.frame.size.width+lblVoteValue1.frame.origin.x+1, h, (width-5)/5, 15)];
        lblVoteValue2.font=[UIFont fontWithName:@"HelveticaNeue-Italic" size:12];
        lblVoteValue2.textColor=[UIColor darkGrayColor];
        lblVoteValue2.tag=2;
        lblVoteValue2.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblVoteValue2];

        
        lblVoteValue3=[[UILabel alloc]initWithFrame:CGRectMake(lblVoteValue2.frame.size.width+lblVoteValue2.frame.origin.x+1, h, (width-5)/5, 15)];
        lblVoteValue3.font=[UIFont fontWithName:@"HelveticaNeue-Italic" size:12];
        lblVoteValue3.textColor=[UIColor darkGrayColor];
        lblVoteValue3.tag=3;
        lblVoteValue3.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblVoteValue3];
        
        lblVoteValue4=[[UILabel alloc]initWithFrame:CGRectMake(lblVoteValue3.frame.size.width+lblVoteValue3.frame.origin.x+1, h, (width-5)/5, 15)];
        lblVoteValue4.font=[UIFont fontWithName:@"HelveticaNeue-Italic" size:12];
        lblVoteValue4.textColor=[UIColor darkGrayColor];
        lblVoteValue4.tag=4;
        lblVoteValue4.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblVoteValue4];
        
        
        lblVoteValue5=[[UILabel alloc]initWithFrame:CGRectMake(lblVoteValue4.frame.size.width+lblVoteValue4.frame.origin.x+1, h, (width-5)/5, 15)];
        lblVoteValue5.font=[UIFont fontWithName:@"HelveticaNeue-Italic" size:12];
        lblVoteValue5.textColor=[UIColor darkGrayColor];
        lblVoteValue5.tag=5;
        lblVoteValue5.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblVoteValue5];

        lblOneVote=[[UILabel alloc]initWithFrame:CGRectMake(0,lblVoteValue5.frame.size.height+lblVoteValue5.frame.origin.y+10,width , 20)];
        lblOneVote.textColor=[UIColor lightGrayColor];
        lblOneVote.text = @"One vote per person // per art piece.";
        
        lblOneVote.font=[UIFont boldSystemFontOfSize:11];
        lblOneVote.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblOneVote];
 
        
        lblTittle=[[UILabel alloc]initWithFrame:CGRectMake(0,lblOneVote.frame.size.height+lblOneVote.frame.origin.y+10,width , 20)];
                 lblTittle.textColor=[UIColor darkGrayColor];
        
        lblTittle.font=[UIFont boldSystemFontOfSize:15];
        lblTittle.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblTittle];

        lblUsername=[[UILabel alloc]initWithFrame:CGRectMake(20,lblTittle.frame.size.height+lblTittle.frame.origin.y+5,width-40 , 20)];
        lblUsername.textColor=[UIColor colorWithRed:0.2863 green:0.8000 blue:0.7490 alpha:1.0];
        lblUsername.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];

        
        lblUsername.textAlignment=NSTextAlignmentCenter;
       // lblUsername.backgroundColor =  [UIColor redColor];
        [self addSubview:lblUsername];
        
        
        description=[[UITextView alloc]initWithFrame:CGRectMake(0,lblUsername.frame.size.height+lblUsername.frame.origin.y+5,width , 40)];
        description.textColor=[UIColor lightGrayColor];
       // description.backgroundColor=[UIColor greenColor];
        description.editable=NO;
        
        description.scrollEnabled=NO;
        description.font=[UIFont systemFontOfSize:11];
        description.textAlignment=NSTextAlignmentCenter;
        [self addSubview:description];

        

        
        
        
        
//        leftImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 40, 40)];
//        [self addSubview:leftImage];
//        
//        leftMsg=[[UIImageView alloc]initWithFrame:CGRectMake(45,20, self.contentView.frame.size.width-100, 40)];
//        leftMsg.image=[UIImage imageNamed:@"leftBubble"];
//        [self addSubview:leftMsg];
//        
//        msgLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.contentView.frame.size.width-105, 40)];
//        msgLbl.font=[UIFont systemFontOfSize:12];
//        msgLbl.numberOfLines=0;
//        msgLbl.textColor=[UIColor grayColor];
//        [leftMsg addSubview:msgLbl];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
