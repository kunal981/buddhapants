//
//  Retailer2ViewController.m
//  BuddhaPants
//
//  Created by brst on 24/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "Retailer2ViewController.h"

@interface Retailer2ViewController ()

@end

@implementation Retailer2ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tittleArr=[[NSMutableArray alloc]initWithObjects:@"CALIFORNIA",@"CANADA", @"FLORIDA",@"We Are Yoga",@"Pulse 163",@"AMBU Yoga",@"HAWAII",@"MAINE",@"SEATTLE",nil];
    secondTittleArr=[[NSMutableArray alloc]initWithObjects:@"Mount Madonna Center",@"Precious Earth", @"DK's Style Hut",@"",@"",@"",@"Noelani Studios",@"Niraj Yoga",@"Center for Yoga & Health",nil];
    
    adreesArr=[[NSMutableArray alloc]initWithObjects:@"445 Summit Road\nWatsonville, CA 95076",@"1213 15th Ave\nRegina, SK S4P 0Y8",@"8347 Overseas Hwy.\nMarathon, Florida 33050",@"138 W. Granada Blvd\nOrmond Beach, FL 32174", @"3447 NE 163rd Street\nNorth Miami Beach, FL 33160",@"5400 South Seas Plantation Road\nCaptiva, FL 33924",@"66-437 Kamehameha Highway\nHaleiwa, HI 96712",@"648 Congress Street\nPortland, Maine",@"5340 Ballard Ave NW\nSeattle, Washington 98107",nil];

    linkArr=[[NSMutableArray alloc]initWithObjects:@"mountmadonna.org",@"preciousearth.ca",@"DKsstylehut.com",@"weareyoga.com",@"Pulse163.com",@"AmbuYoga.com",@"Noelanistudios.com",@"",@"KulaMovement.com",nil];
    
    
    noArr=[[NSMutableArray alloc]initWithObjects:@"408.846.4064",@"306.205.6959", @"305.440.2738",@"386.677.9642",@"305.904.6444",@"239.314.9642",@"808.389.3709 ",@"207-318-1940",@"206.972.2999",nil];
   
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;

    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:scrollView];
    
    
    UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, scrollView.frame.size.width, 30)];
    lblHeader.text=@"RETAILERS";
    lblHeader.font=[UIFont boldSystemFontOfSize:20.0];
    lblHeader.textAlignment=NSTextAlignmentCenter;
    lblHeader.backgroundColor=[UIColor whiteColor];
    lblHeader.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    [scrollView addSubview:lblHeader];
    
  
     CGFloat y=lblHeader.frame.size.height+lblHeader.frame.origin.y+15;
    UILabel * lblTittle,*lblsecndTitl,*lblAdress;
    for (int j=0; j<tittleArr.count; j++)
    {
        lblTittle=[[UILabel alloc]initWithFrame:CGRectMake(0, y, self.view.frame.size.width, 20)];
        lblTittle.text=[tittleArr objectAtIndex:j];
        lblTittle.font=[UIFont italicSystemFontOfSize:19];
        lblTittle.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        lblTittle.textAlignment=NSTextAlignmentCenter;
         [scrollView addSubview:lblTittle];
        
        lblsecndTitl=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTittle.frame.size.height+lblTittle.frame.origin.y+20, self.view.frame.size.width, 20)];
        lblsecndTitl.text=[secondTittleArr objectAtIndex:j];
        lblsecndTitl.font=[UIFont italicSystemFontOfSize:19];
        lblsecndTitl.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        lblsecndTitl.textAlignment=NSTextAlignmentCenter;
        [scrollView addSubview:lblsecndTitl];
        
        
        lblAdress=[[UILabel alloc]initWithFrame:CGRectMake(0,lblsecndTitl.frame.size.height+lblsecndTitl.frame.origin.y+5, self.view.frame.size.width, 40)];
        lblAdress.text=[adreesArr objectAtIndex:j];
        lblAdress.numberOfLines=2;
        lblAdress.font=[UIFont italicSystemFontOfSize:15];
        lblAdress.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        lblAdress.textAlignment=NSTextAlignmentCenter;
        [scrollView addSubview:lblAdress];

         salesBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        salesBtn.frame=CGRectMake(0,lblAdress.frame.size.height+lblAdress.frame.origin.y,self.view.frame.size.width, 15);
        salesBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
        salesBtn.tag=j;
        [salesBtn setTitle:[linkArr objectAtIndex:j] forState:UIControlStateNormal];
        [salesBtn setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
        
        [salesBtn addTarget:self action:@selector(salesBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        salesBtn.titleLabel.font=[UIFont italicSystemFontOfSize:15];
        [scrollView addSubview:salesBtn];
        
        if (j!=7)
        {
            
            NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:[linkArr objectAtIndex:j]];
            
            [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
            
            [salesBtn setAttributedTitle:commentString forState:UIControlStateNormal];
            


        }
        
        callBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        callBtn.frame=CGRectMake(0,salesBtn.frame.size.height+salesBtn.frame.origin.y+5, self.view.frame.size.width, 20);
        callBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
        callBtn.tag=j;
        [callBtn setTitle:[linkArr objectAtIndex:j] forState:UIControlStateNormal];
        [callBtn setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
        
        [callBtn addTarget:self action:@selector(callBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        callBtn.titleLabel.font=[UIFont italicSystemFontOfSize:15];
        [scrollView addSubview:callBtn];

        
//        ca=[[UILabel alloc]initWithFrame:CGRectMake(0,salesBtn.frame.size.height+salesBtn.frame.origin.y+5, self.view.frame.size.width, 20)];
//        lblNumber.text=[noArr objectAtIndex:j];
//        lblNumber.font=[UIFont italicSystemFontOfSize:15];
//        lblNumber.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
//        lblNumber.textAlignment=NSTextAlignmentCenter;
//        [scrollView addSubview:lblNumber];

        
            
            NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:[noArr objectAtIndex:j]];
            
            [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
            
          [callBtn setAttributedTitle:commentString forState:UIControlStateNormal];
            
            
            
        
 
        
        y+=170;
    }
    

   scrollView.contentSize=CGSizeMake(self.view.frame.size.width, salesBtn.frame.size.height+salesBtn.frame.origin.y+100);
    // Do any additional setup after loading the view.
}

#pragma mark Left bar button

-(void)barbuttonPressed
{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0)
    {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }
    

}

-(void)callBtnPressed:(UIButton *)sender
{
    if (sender.tag==0)
    {
        
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:0]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];

    }
    else if (sender.tag==1){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:1]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];

    }
    else if (sender.tag==2){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:2]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    else if (sender.tag==3){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:3]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    else if (sender.tag==4){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:4]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    else if (sender.tag==5){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:5]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    else if (sender.tag==6){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:6]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    else if (sender.tag==7){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:7]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    else if (sender.tag==8){
        UIApplication *myApp = [UIApplication sharedApplication];
        
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",[noArr objectAtIndex:8]];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
        
    }
    
   
    
    
}
-(void)salesBtnPressed:(UIButton *)sender
{
    if (sender.tag==0)
    {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://mountmadonna.org"]];

    }else if (sender.tag==1)
    {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://preciousearth.ca"]];
    }else if (sender.tag==2)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://dksstylehut.com"]];
    }else if (sender.tag==3)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.weareyoga.com"]];
    }else if (sender.tag==4)
    {  [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://pulse163.com"]];
        
    }else if (sender.tag==5)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.ambuyoga.com"]];
    }else if (sender.tag==6)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.noelanistudios.com"]];
    }
    else if (sender.tag==8)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.kulamovement.com"]];
    }
   
}





-(void)cartbuttonPressed{
    if (addtoCart) {
        
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else {
       subTotal= 0;
    [self coreDataFetch];
    
    
    
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
    arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
    [self.view addSubview:arwView];
    
    view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    
    //    view1.layer.borderWidth = 2.0;
    //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
    
    
    
    UIView * backView = [[UIView alloc]init];
    
    backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView];
    
    
    lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.textAlignment=NSTextAlignmentRight;
    lblPrice1.adjustsFontSizeToFitWidth=YES;
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
    
    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
    lblSubTotal.textAlignment=NSTextAlignmentRight;
     
    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [view1 addSubview:tableView_Cart];
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
    
    }
        addtoCart = false;
    }
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
        
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
    
    return cell;
}
-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}


-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
