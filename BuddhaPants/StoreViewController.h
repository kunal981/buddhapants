//
//  StoreViewController.h
//  BuddhaPants
//
//  Created by brst on 03/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *product_Array;
    NSMutableArray *title_Array;
    NSMutableArray *src_Array;
    NSMutableArray *image_array;
    NSMutableArray *varrient_array;
    NSMutableArray *bodyHtml_Array;
    NSMutableArray *handleArray;
    NSMutableArray *idOfproductsArray;
    
    
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
    NSManagedObjectContext *managedObjectContext;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    UILabel *lblPrice1;
    BOOL addtoCart;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
