//
//  SuccessViewController.m
//  BuddhaPants
//
//  Created by brst on 28/05/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "SuccessViewController.h"

@interface SuccessViewController ()

@end

@implementation SuccessViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    self.lblSuccess.text = [NSString stringWithFormat:@"Your orderID is %@ has been placed successfully",self.orderID];
    
    // Do any additional setup after loading the view from its nib.
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (IBAction)btnGoHomePreesed:(id)sender {
    HomeViewController *homeVC = [[HomeViewController alloc]init];
    [self.navigationController pushViewController:homeVC animated:NO];
}
@end
