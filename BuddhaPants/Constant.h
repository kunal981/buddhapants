//
//  Constant.h
//  CarDealer
//
//  Created by brst on 27/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject
    
extern NSString* const register_String;
extern NSString* const page_String;
extern NSString* const retailers_String;
extern NSString* const event_String;
extern NSString* const product_String;
extern NSString* const aboutUS_String;
extern NSString* const contactUS_String;
extern NSString* const brandReps_String;
extern NSString* const wholeSales_String;
extern NSString* const login_String;
extern NSString* const order_String;

extern NSString* const product_String_Home;



extern NSString* const about_String;
extern NSString* const upload_String;
extern NSString* const imageGet_String;
extern NSString* const rating_String;
extern NSString* const custom_Collection;

@end
