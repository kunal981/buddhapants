//
//  SuccessViewController.h
//  BuddhaPants
//
//  Created by brst on 28/05/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccessViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblSuccess;
- (IBAction)btnGoHomePreesed:(id)sender;
@property(nonatomic,strong) NSString* orderID;
@end
