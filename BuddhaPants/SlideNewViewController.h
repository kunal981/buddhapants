//
//  SlideNewViewController.h
//  BuddhaPants
//
//  Created by brst on 5/8/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "About2ViewController.h"
#import "WholeSale2ViewController.h"
#import "Contact2ViewController.h"
#import "BrandReps2ViewController.h"
#import "Retailer2ViewController.h"
#import "ArtPantsViewController.h"
@interface SlideNewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    
     UILabel *lblName;
    
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
    NSManagedObjectContext *managedObjectContext;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    UILabel* lblPrice1;
    
    NSMutableArray* imgArr;
}
@property(strong,nonatomic)UITableView *slide_tableView;
@end
