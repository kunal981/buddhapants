//
//  UploadViewController.m
//  BuddhaPants
//
//  Created by brst on 5/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "UploadViewController.h"

@interface UploadViewController ()
{
    Api_Wrapper *objApi;
}

@end

@implementation UploadViewController
-(void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    view.backgroundColor=[UIColor whiteColor];
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dictData=[[NSMutableDictionary alloc]init];
    
    selectedBtnColor=[UIColor colorWithRed:0.7176 green:0.7176 blue:0.7176 alpha:1.0];
    notselectedBtnColor=[UIColor colorWithRed:0.7765 green:0.7765 blue:0.7765 alpha:1.0];
    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
    pickerimage=[[UIImagePickerController alloc]init];
    pickerimage.delegate=self;
    pickerimage.allowsEditing=YES;
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+50)];
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = YES;
    scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scrollView];
    
    
    uploadImageView =[[UIImageView alloc]initWithFrame:CGRectMake( 0, 0,self.view.frame.size.width, 320)];
    //uploadImageView.backgroundColor=[UIColor lightGrayColor];
    uploadImageView.layer.masksToBounds=YES;
    uploadImageView.image =[UIImage imageNamed:@"loader.png"];
   // uploadImageView.contentMode=UIViewContentModeScaleAspectFit;
    [scrollView addSubview:uploadImageView];
    
    UITapGestureRecognizer *tapForPicture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePictureTap)];
    tapForPicture.numberOfTapsRequired = 1;
    tapForPicture.numberOfTouchesRequired=1;
    uploadImageView.userInteractionEnabled=YES;
    [uploadImageView addGestureRecognizer:tapForPicture];

    
    CGFloat y = uploadImageView.frame.origin.y+uploadImageView.frame.size.height+15;
    
    
    UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(20, y, self.view.frame.size.width-40, 20)];
    lblHeader.text= @"Title";
    lblHeader.font=[UIFont systemFontOfSize:15.0];
    lblHeader.textAlignment=NSTextAlignmentLeft;
    lblHeader.textColor=[UIColor  blackColor];
    [scrollView addSubview:lblHeader];
    
     txtField=[[UITextField alloc]initWithFrame:CGRectMake(20, lblHeader.frame.size.height+lblHeader.frame.origin.y+5, self.view.frame.size.width-40, 35)];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtField.leftView = paddingView;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    txtField.tag=1;
    txtField.backgroundColor=[UIColor whiteColor];
    txtField.layer.borderColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0].CGColor;
    txtField.delegate=self;
    txtField.returnKeyType=UIReturnKeyDone;
    txtField.layer.borderWidth=1.0;
    txtField.placeholder=@" ";
    y+=55+25;
    [scrollView addSubview:txtField];

    UILabel * lblDes=[[UILabel alloc]initWithFrame:CGRectMake(20, txtField.frame.size.height+txtField.frame.origin.y+5, self.view.frame.size.width-40, 20)];
    lblDes.text= @"Description";
    lblDes.font=[UIFont systemFontOfSize:15.0];
    lblDes.textAlignment=NSTextAlignmentLeft;
    lblDes.textColor=[UIColor  blackColor];
    [scrollView addSubview:lblDes];
    
     txtFieldDesc=[[UITextField alloc]initWithFrame:CGRectMake(20, lblDes.frame.size.height+lblDes.frame.origin.y+5, self.view.frame.size.width-40, 55)];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtFieldDesc.leftView = paddingView1;
     txtFieldDesc.tag=2;
    txtFieldDesc.textAlignment = NSTextAlignmentLeft;
    txtFieldDesc.leftViewMode = UITextFieldViewModeAlways;
    txtFieldDesc.returnKeyType=UIReturnKeyDone;
    txtFieldDesc.backgroundColor=[UIColor whiteColor];
    txtFieldDesc.layer.borderColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0].CGColor;
    txtFieldDesc.delegate=self;
    txtFieldDesc.layer.borderWidth=1.0;
    txtFieldDesc.placeholder=@" ";
 txtFieldDesc.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    [scrollView addSubview:txtFieldDesc];
    

  UIButton* submitButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    submitButton.frame = CGRectMake(30, txtFieldDesc.frame.size.height+txtFieldDesc.frame.origin.y+5, self.view.frame.size.width-60, 35);
    submitButton.backgroundColor =[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    [submitButton setTitle:@"UPLOAD " forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
    submitButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [submitButton addTarget:self action:@selector(submitButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:submitButton];
    
    if (([UIScreen mainScreen].bounds.size.height == 568) ||([UIScreen mainScreen].bounds.size.height== 480)) {
         scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+130);
    }else {

    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    }

    [self cameraMethod];
    
    [self lowerButton];
    // Do any additional setup after loading the view.
}
 -(void)profilePictureTap
{
    [self cameraMethod];
}
#pragma mark Camera Method
-(void)cameraMethod
{
    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:@"Choose Option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery" ,nil];
    
    [sheet showInView:self.view];

}

#pragma mark UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You don't have a camera for this device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [noCameraAlert show];
        }
        else
        {
            pickerimage.sourceType=UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:pickerimage animated:YES completion:nil];
        }
    }
    else if (buttonIndex==1)
    {         pickerimage.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:pickerimage animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@",info);

    //[uploadImageView setContentMode:UIViewContentModeScaleAspectFit];
    //uploadImageView.layer.masksToBounds = YES;
    uploadImage=[info objectForKey:UIImagePickerControllerEditedImage];
   // uploadImage = [self imageWithImage:uploadImage scaledToWidth:uploadImageView.frame.size.width];
    uploadImageView.image=uploadImage;
    base64String=[self encodeToBase64String:uploadImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    //DO WHATEVER WHEN YOU PRESS CANCEL BUTTON (PROBABLY DISMISSING THE POPOVER
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width,size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
 - (NSString *)encodeToBase64String:(UIImage *)image
{
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSString *check=[imageData base64EncodedStringWithOptions:0];
    if(check==nil)
        return @"nil";
    else
        return [imageData base64EncodedStringWithOptions:0];
    
}
#pragma mark Lower button
-(void)lowerButton
{
    
    
    CGFloat width=[UIScreen mainScreen].bounds.size.width;
    
    UIView * buttonLowerView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-84, width, 40)];
    buttonLowerView.backgroundColor=notselectedBtnColor;
    [self.view addSubview:buttonLowerView];
    
    NSLog(@"%@",NSStringFromCGRect(buttonLowerView.frame));
    
    voteButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    voteButton.frame = CGRectMake(0, 0, (self.view.frame.size.width)/2, 40);
    voteButton.backgroundColor = notselectedBtnColor;
    [voteButton setTitle:@"VOTE" forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    voteButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [voteButton addTarget:self action:@selector(voteButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:voteButton];
    
    uploadButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    uploadButton.frame = CGRectMake(voteButton.frame.size.width+voteButton.frame.origin.x, 0, (self.view.frame.size.width)/2, 40);
    uploadButton.backgroundColor = selectedBtnColor;
    [uploadButton setTitle:@"UPLOAD" forState:UIControlStateNormal];
    uploadButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [uploadButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    
    [buttonLowerView addSubview:uploadButton];
    
    
    
    
}
-(void)voteButtonPresssed
{
    voteButton.backgroundColor = notselectedBtnColor;
    uploadButton.backgroundColor=selectedBtnColor;
    [uploadButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    VoteViewController * voteVC=[[VoteViewController alloc]init];
    
    [self.navigationController pushViewController:voteVC animated:YES];
    
}
-(void)submitButtonPresssed
{
    
    if (txtField.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter title" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if (txtFieldDesc.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter description" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if(base64String.length==0 || [base64String isEqualToString:@"nil"] || [base64String isEqual:(id)[NSNull null]])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please upload the image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else
    {
        
        NSLog(@"%@ %@ %@ %@ ",txtField.text,txtFieldDesc.text,[[NSUserDefaults standardUserDefaults]valueForKey:@"firstName"],[[NSUserDefaults standardUserDefaults]valueForKey:@"uid"]);
        
        //base64String=[base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];

        NSLog(@"%@",[NSString stringWithFormat:@"%@",base64String]);
        
        [dictData setValue:txtField.text forKey:@"title"];
        [dictData setValue:txtFieldDesc.text forKey:@"description"];
        [dictData setValue:[NSString stringWithFormat:@"%@",base64String] forKey:@"image"];
        [dictData setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"firstName"] forKey:@"user_name"];
        [dictData setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"uid"]  forKey:@"id"];
        
        
        
        [self apihit:dictData];
       
        
    }
    
}
#pragma mark Upload Image

-(void)apihit:(NSMutableDictionary*)dict
{
    
    
    objApi = [Api_Wrapper shared];
    
  
    
    
    [objApi PostApi:dict string:upload_String
         completion:^(NSDictionary *json, BOOL sucess) {
              // [self showLoadingView];
             NSLog(@"%@",json);
             
             if ([[json valueForKey:@"status"] isEqualToString:@"true"])
             {
                 
                 UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Thank you for submission!" message:@"It is with the admin for approval" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 
                 [alert show];
                 
                 txtField.text=nil;
                 txtFieldDesc.text=nil;
                 uploadImageView.image=nil;
                 
                 [self voteButtonPresssed];
                 
                 //[self hideLoadingView];
                 
             }
             
             
           //  [self hideLoadingView];
             
             
         }];
    
    
}



// -(void)showLoadingView
//{
//    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.userInteractionEnabled = YES;
//    hud.labelText = @"Loading..";
//    hud.dimBackground = YES;
//    
//}
//-(void)hideLoadingView
//{
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//}


#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0) {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }
    
    
}
#pragma mark Right bar button

-(void)cartbuttonPressed{
    if (addtoCart) {
        
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else {
        subTotal= 0;
        [self coreDataFetch];
        
        
        
        backGroundView = [[UIView alloc]init];
        backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        backGroundView.backgroundColor = [UIColor blackColor];
        backGroundView.alpha =  0.50;
        
        [self.view addSubview:backGroundView];
        
        
        arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
        
        arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
        
        [self.view addSubview:arwView];
        
        view1 = [[UIView alloc]init];
        
        view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
        view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
        
        //    view1.layer.borderWidth = 2.0;
        //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
        [self.view addSubview:view1];
        
        
        
        
        UIView * backView = [[UIView alloc]init];
        
        backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
        
        backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
        backView.layer.cornerRadius = 7.0;
        
        [view1 addSubview:backView];
        
        
        lblPrice1  = [[UILabel alloc]init];
        lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        lblPrice1.textAlignment=NSTextAlignmentRight;
        lblPrice1.adjustsFontSizeToFitWidth=YES;
        lblPrice1.font = [UIFont boldSystemFontOfSize:17];
        lblPrice1.textColor = [UIColor lightGrayColor];
        
        [backView addSubview:lblPrice1];
        
        
        UILabel *lblSubTotal  = [[UILabel alloc]init];
        lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
        lblSubTotal.text = @"SubTotal";
        lblSubTotal.textAlignment=NSTextAlignmentRight;
        
        lblSubTotal.textColor = [UIColor lightGrayColor];
        lblSubTotal.font = [UIFont systemFontOfSize:13];
        [backView addSubview:lblSubTotal];
        
        
        
        tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
        
        tableView_Cart.dataSource=self;
        tableView_Cart.delegate=self;
        tableView_Cart.backgroundColor=[UIColor whiteColor];
        [view1 addSubview:tableView_Cart];
        
        UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeSystem];
        
        btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
        btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
        [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
        [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnCheckOut];
        
    }
        addtoCart = false;
    }
    
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
        
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
  
    
    return cell;
}
-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}



-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect frame=self.view.frame;
    frame.origin.y=-90;
    self.view.frame=frame;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect frame=self.view.frame;
    frame.origin.y=64;
    self.view.frame=frame;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
