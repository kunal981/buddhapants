//
//  UIButtonViewController.m
//  BuddhaPants
//
//  Created by brst on 5/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "UIButtonViewController.h"

@interface UIButtonViewController ()

@end

@implementation UIButtonViewController

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    // Do any additional setup after loading the view.
//    
//    
//    
//    
//    
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
#pragma mark Lower button
-(UIView *)lowerButton:(CGRect)frame
{
    
   selectedBtnColor=[UIColor colorWithRed:0.7176 green:0.7176 blue:0.7176 alpha:1.0];
  notselectedBtnColor=[UIColor colorWithRed:0.7765 green:0.7765 blue:0.7765 alpha:1.0];
    CGFloat width=[UIScreen mainScreen].bounds.size.width;
    
    UIView * buttonLowerView=[[UIView alloc]initWithFrame:CGRectMake(0,  frame.size.height-84, width, 40)];
    buttonLowerView.backgroundColor=notselectedBtnColor;
  //  [self.view addSubview:buttonLowerView];
    
    NSLog(@"%@",NSStringFromCGRect(buttonLowerView.frame));
    
    voteButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    voteButton.frame = CGRectMake(0, 0, (frame.size.width)/2, 40);
    voteButton.backgroundColor = notselectedBtnColor;
    [voteButton setTitle:@"VOTE" forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    voteButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [voteButton addTarget:self action:@selector(voteButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:voteButton];
    
    uploadButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    uploadButton.frame = CGRectMake(voteButton.frame.size.width+voteButton.frame.origin.x, 0, (frame.size.width)/2, 40);
    uploadButton.backgroundColor = notselectedBtnColor;
    [uploadButton setTitle:@"UPLOAD" forState:UIControlStateNormal];
    uploadButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [uploadButton addTarget:self action:@selector(uploadButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:uploadButton];
    
    return buttonLowerView;
}
-(void)voteButtonPresssed
{
    voteButton.backgroundColor = selectedBtnColor;
    uploadButton.backgroundColor=notselectedBtnColor;
    [voteButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
}
-(void)uploadButtonPresssed
{
    voteButton.backgroundColor = notselectedBtnColor;
    uploadButton.backgroundColor=selectedBtnColor;
    [uploadButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
