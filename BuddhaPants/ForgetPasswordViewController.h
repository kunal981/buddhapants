//
//  ForgetPasswordViewController.h
//  BuddhaPants
//
//  Created by brst on 25/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPasswordViewController : UIViewController<UITextFieldDelegate>
{
    UITextField * txtEmail;
}

@end
