//
//  Retailer2ViewController.h
//  BuddhaPants
//
//  Created by brst on 24/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Retailer2ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UIScrollView * scrollView;
    NSMutableArray * tittleArr,*secondTittleArr,*adreesArr,*linkArr,*noArr;

    UIButton * salesBtn,*callBtn;
    UILabel* lblPrice1;

    
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
     NSManagedObjectContext *managedObjectContext;
     NSArray *fetchedObjects;
     UIView *cellView;
     UIImageView *profileImageView;
     BOOL addtoCart;
}

@end
