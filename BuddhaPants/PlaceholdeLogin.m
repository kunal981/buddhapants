//
//  PlaceholdeLogin.m
//  BuddhaPants
//
//  Created by brst on 24/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "PlaceholdeLogin.h"

@implementation PlaceholdeLogin

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) drawPlaceholderInRect:(CGRect)rect {
    UIColor *placeHolderTextColor;
    if (self.placeholder)
    {
        
        // color of placeholder text
        
        
        placeHolderTextColor = [UIColor whiteColor];
        
        CGSize drawSize = [self.placeholder sizeWithAttributes:[NSDictionary dictionaryWithObject:self.font forKey:NSFontAttributeName]];
        CGRect drawRect = rect;
        
        // verticially align text
        drawRect.origin.y = (rect.size.height - drawSize.height) * 0.5;
        
        // set alignment
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = self.textAlignment;
        
        // dictionary of attributes, font, paragraphstyle, and color
        NSDictionary *drawAttributes = @{NSFontAttributeName: self.font,
                                         NSParagraphStyleAttributeName : paragraphStyle,
                                         NSForegroundColorAttributeName : placeHolderTextColor};
        
        
        // draw
        [self.placeholder drawInRect:drawRect withAttributes:drawAttributes];
        
    }
}


@end
