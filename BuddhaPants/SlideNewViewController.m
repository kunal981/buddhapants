//
//  SlideNewViewController.m
//  BuddhaPants
//
//  Created by brst on 5/8/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "SlideNewViewController.h"

@interface SlideNewViewController ()

@end

@implementation SlideNewViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden =NO;
    self.navigationItem.hidesBackButton = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    imgArr=[[NSMutableArray alloc]initWithObjects:@"twitter.png", @"facebook.png", @"tumblr.png", @"Instagram.png", @"pinterest.png", nil];
    
    self.view.backgroundColor=[UIColor colorWithRed:0.2784 green:0.8431 blue:0.7922 alpha:1.0];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
//    UIBarButtonItem * rightBar=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart12"] style:UIBarButtonItemStylePlain target:self action:@selector(cartbuttonPressed)];
//    self.navigationItem.rightBarButtonItem=rightBar;
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
   // self.slide_tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    CGFloat width=[UIScreen mainScreen].bounds.size.width;
//    
//    UISearchBar * searchBbar=[[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, width, 44)];
//    searchBbar.backgroundColor=[UIColor clearColor];
//    
//    
//    [ searchBbar setSearchBarStyle:UISearchBarStyleMinimal];
//    [self.view addSubview:searchBbar];
    
    UIView * view=[[UIView alloc]initWithFrame:CGRectMake(0, 5, width, 1)];
    view.backgroundColor=[UIColor whiteColor];
   // [self.view addSubview:view];
    
    self.slide_tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, width, self.view.frame.size.height-150)];
    self.slide_tableView.backgroundColor=[UIColor colorWithRed:0.2784 green:0.8431 blue:0.7922 alpha:1.0];
    self.slide_tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    self.slide_tableView.delegate=self;
    self.slide_tableView.separatorColor=[UIColor whiteColor];
    self.slide_tableView.dataSource=self;
    [self.view addSubview:self.slide_tableView];
    
    CGFloat x=(self.view.frame.size.width-240)/2;
    
    for (int i=0;i<5; i++)
    {
        UIButton* shareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        shareBtn.tag=i;
        shareBtn.frame=CGRectMake(x,self.slide_tableView.frame.size.height+17, 35, 35);
        [shareBtn setImage:[UIImage imageNamed:[imgArr objectAtIndex:i]] forState:UIControlStateNormal];
        [shareBtn addTarget:self action:@selector(shareBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        shareBtn.backgroundColor=[UIColor clearColor];
        x=x+50;
        [self.view addSubview:shareBtn];
        
    }
    

    // Do any additional setup after loading the view.
}
#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    NSInteger row = indexPath.row;
    
    
    
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor colorWithRed:0.2784 green:0.8431 blue:0.7922 alpha:1.0];
        
        lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, 40)];
        lblName.textColor = [UIColor whiteColor];
        lblName.font = [UIFont systemFontOfSize:16];
        lblName.textAlignment =NSTextAlignmentCenter;
        [cell.contentView addSubview:lblName];
        
        
    }
    
    NSString *text = nil;
    if (row == 0)
    {
        text = @"HOME";
        
        
    }
    else if (row == 1)
    {
        text = @"STORE";
            }
        else if (row == 2)
        {
            text = @"ART PANTS";
           
        }
    else if (row==3){
        text = @"SHOWS";
        
        
    }
    else if (row==4){
        text = @"RETAILERS";
        
        
    }
    else if (row==5){
        text = @"ABOUT";
        
        
    }
    else if (row==6){
        text = @"CONTACT US!";
        
        
    }
    else if (row==7){
        text = @"GET INVOLVED";
        
        
    }
         else if (row==8){
        text = @"WHOLESALE";
        
        
    }
    
    
    
    lblName.text = NSLocalizedString( text,nil );
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        HomeViewController *homeVC = [[HomeViewController alloc]init];
        [self.navigationController pushViewController:homeVC animated:NO];
    }
    
    else if (indexPath.row==1){
        StoreViewController *storeVC= [[StoreViewController alloc]init];
        
        [self.navigationController pushViewController:storeVC animated:NO];
        
    }else if (indexPath.row==2){
        ArtPantsViewController *eventVC= [[ArtPantsViewController alloc]init];
        
        [self.navigationController pushViewController:eventVC animated:NO];
    }

    
    else if (indexPath.row==3){
        EventsViewController *eventVC= [[EventsViewController alloc]init];
        
        [self.navigationController pushViewController:eventVC animated:NO];
    }
    
    else if (indexPath.row==4){
        RetailerViewController *retailerVC= [[RetailerViewController alloc]init];
        
        [self.navigationController pushViewController:retailerVC animated:NO];
    }
    else if (indexPath.row==5)
    {
        About2ViewController *aboutVC= [[About2ViewController alloc]init];
        
        [self.navigationController pushViewController:aboutVC animated:NO];
    }
    else if (indexPath.row==6){
        Contact2ViewController *conVC= [[Contact2ViewController alloc]init];
        
        [self.navigationController pushViewController:conVC animated:NO];
    }
    
    else if (indexPath.row==7){
        BrandReps2ViewController *brandVC= [[BrandReps2ViewController alloc]init];
        
        [self.navigationController pushViewController:brandVC animated:NO];
    }
    else if (indexPath.row==8){
        WholeSale2ViewController *wholeVC= [[WholeSale2ViewController alloc]init];
        
        [self.navigationController pushViewController:wholeVC animated:NO];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}



#pragma mark - UIBarbutton Action's
-(void)barbuttonPressed
{
    
}
-(void)cartbuttonPressed
{
    //    if(fetchedObjects.count==0){
    //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        [alert show];
    //        return;
    //    }else {
    //        [self coreDataFetch];
    //        [lblBadage setHidden:YES];
    //
    //
    //
    //        backGroundView = [[UIView alloc]init];
    //        backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //
    //        backGroundView.backgroundColor = [UIColor blackColor];
    //        backGroundView.alpha =  0.50;
    //
    //        [self.view addSubview:backGroundView];
    //
    //
    //        arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    //
    //        arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    //
    //        [self.view addSubview:arwView];
    //
    //        view1 = [[UIView alloc]init];
    //
    //        view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    //        view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0]; 
    //
    //        //    view1.layer.borderWidth = 2.0;
    //        //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    //        [self.view addSubview:view1];
    //
    //
    //
    //
    //        UIView * backView = [[UIView alloc]init];
    //
    //        backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    //
    //        backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    //        backView.layer.cornerRadius = 7.0;
    //
    //        [view1 addSubview:backView];
    //
    //
    //        lblPrice1  = [[UILabel alloc]init];
    //        lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
    //        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    //        lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    //        lblPrice1.textColor = [UIColor lightGrayColor];
    //
    //        [backView addSubview:lblPrice1];
    //
    //
    //        UILabel *lblSubTotal  = [[UILabel alloc]init];
    //        lblSubTotal.frame = CGRectMake(self.view.frame.size.width-70, 30, 70, 25);
    //        lblSubTotal.text = @"SubTotal";
    //        lblSubTotal.textColor = [UIColor lightGrayColor];
    //        lblSubTotal.font = [UIFont systemFontOfSize:13];
    //        [backView addSubview:lblSubTotal];
    //
    //
    //
    //        tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    //
    //        tableView_Cart.dataSource=self;
    //        tableView_Cart.delegate=self;
    //        tableView_Cart.backgroundColor=[UIColor whiteColor];
    //        [view1 addSubview:tableView_Cart];
    //
    //        UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    //
    //        btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    //        btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    //        [btnCheckOut setTitle:@"CheckOut" forState:UIControlStateNormal];
    //        [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //        [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    //        [view1 addSubview:btnCheckOut];
    //
    //    }
    
}

-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}
-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)shareBtnPressed:(UIButton *)sender
{
   
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Are you want to leave the app?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
       
    NSLog(@"%ld",(long)sender.tag);
    if (sender.tag==0)
    {
        alert.tag =0;
        
    }else if (sender.tag==1)
    {
        alert.tag =1;
        
    }else if (sender.tag==2)
    {
        alert.tag =2;
        
    }else if (sender.tag==3)
    {
        alert.tag =3;
        
    }else if (sender.tag==4)
    {
        alert.tag =4;
        
    }
    [alert show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma Mark UIAleartViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1){
        if (alertView.tag==0) {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/buddha_pants"]];
        }
        else if (alertView.tag == 1){
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/wear.buddhapants"]];
        }
        else if (alertView.tag == 2){
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://buddhapants.tumblr.com"]];
        }
        else if (alertView.tag == 3){
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://instagram.com/buddha_pants"]];
        }
        else if (alertView.tag == 4){
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.pinterest.com/wearbuddhapants/"]];
        }
    }
}

@end
