//
//  About2ViewController.m
//  BuddhaPants
//
//  Created by brst on 23/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "About2ViewController.h"

@interface About2ViewController ()

@end

@implementation About2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    nameArr=[[NSMutableArray alloc]initWithObjects:@"RACHEL RAAB",@"CAROLINE MAE", @"CLAUDIO MILANO",nil];
    btnArr=[[NSMutableArray alloc]initWithObjects:@"Visit her website…",@"Visit her website…", @"Visit his website…",nil];
     imgArr=[[NSMutableArray alloc]initWithObjects:@"first.png",@"second.jpg",@"third.jpg",nil];
    dataArr=[[NSMutableArray alloc]initWithObjects:@"Raab, an artist and jack of all trades, holds a A.S. degree in Photographic Technology and a B.F.A in Photography with a minor in Graphic Arts. She has been a full time Graphic Designer and part time Printer and Photographer for over a decade now, having opened The Soda Shop, a one stop print and design firm in Savannah, Georgia. With a recent move to Miami, Raab was looking to get away from the industry. Her found passion for yoga and her love of Harem Pants presented a superb mix for a new business venture and beginning. Raab is now studying part time at FIU for Fashion and dedicating the rest of her days to designing and operating Buddha Pants.",@"Designer Caroline Mae Heidenreich graduated from Savannah College of Art and Design (SCAD) with a BFA in Fashion Design in 2009. She received the Jeffrey Fashion Cares New Talent Award after graduation and started creating custom couture looks for the elite in Atlanta, Los Angeles, Santa Barbara, Tallahassee, Havana, Miami, and NYC.\n\nCaroline Mae Heidenreich, friend and mentor of founder Rachel Raab, is a fashion Designer extraordinaire based out of New York City. Heidenreich and Raab graduated from SCAD together in 2009. Raab, always wearing a baggy pant, fell in love with a harem pant Caroline brought back from Paris in 2008. Raab commissioned her to make several pairs during her fashion studies at SCAD. Somewhere along the line they were nicknamed Buddha Pants. Raab has found many pairs over the last 5 years, scavenging everywhere from markets in Mexico to hippy stores in Charleston, SC. Never finding the perfect comfy pair. Raab and Heidenrich reunited in December 2012 after a few sips of bourbon the drawing pad came out and BuddhaPants.com was bought. Mae now resides in NYC and is busy with her brand and other endeavors.", @"Based in Miami, Claudio Milano is high end fashion brand and boutique, dedicated to elegance and class.\n\nOnce Raab and Frost had the prototype their plan was ready to be executed. Raab, studying fashion at FIU, started reaching out to international companies to begin production, a stressful and discouraging process. At a breaking point Raab sat down and decided to try to reach out to someone locally. After a chance phone call we were connected with a local fashion designer, Claudio Cohen of Claudio Milano. He welcomed us into is office and business with no hesitation, and the process of refining the original prototypes was undertaken. He is our mentor and inspiration. Claudio has helped us perfect all the little things that matter and manufactured the pants in his personal factory in Vietnam.",nil];

    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
   
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;

    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    // Do any additional setup after loading the view from its nib.
    
    
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:scrollView];
    
    
    UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(20, 10, scrollView.frame.size.width-40, 120)];
    lblHeader.text=@"Our mission is to create organic, fun, comfy, multi-functional garments just for you!";
    lblHeader.numberOfLines=5;
    lblHeader.font=[UIFont boldSystemFontOfSize:20.0];
    lblHeader.textAlignment=NSTextAlignmentCenter;
    lblHeader.backgroundColor=[UIColor whiteColor];
    lblHeader.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    [scrollView addSubview:lblHeader];
    
    
     lblText=[[UILabel alloc]initWithFrame:CGRectMake(10,lblHeader.frame.size.height+lblHeader.frame.origin.y, self.view.frame.size.width-20, 400)];
    lblText.text=@"Buddha Pants is a clothing company dedicated to creating multifunctional travel ready apparel, focusing on comfort & encouraging an active lifestyle. The harem pant that packs into it pocket was inspired by the Eno Hammock and designed and manufactured by Rachel Raab.I wanted to create a harem pant with pockets! Harem pants with pockets are not common, yet so functional, when I had the idea to combine the two it made so much sense. Something practical and comfy. Go figure. I hope you enjoy the pants just as much as I do.- Raab\n\nBuddha Pants was founded in December 2012 by Rachel Raab, Similar styles of Buddha Pants are called shalwar pants, harem pants, Gypsy pants. Buddha pants are all of the above and they fold themselves into their own pocket, making them a perfect travel-ready addition to any wardrobe! They give the illusion of a large triangular skirt but have flexible ankles that can be pulled up on the calf to create different looks.";
    //lblText.editable=NO;
    lblText.numberOfLines=80;
    lblText.font=[UIFont systemFontOfSize:13.0];
    lblText.textAlignment=NSTextAlignmentLeft;
    lblText.backgroundColor=[UIColor whiteColor];
    lblText.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
    [scrollView addSubview:lblText];

    CGFloat y=lblText.frame.size.height+lblText.frame.origin.y+10;
    
    for (int i=0; i<nameArr.count; i++)
    {
        UIImageView * imgView=[[UIImageView alloc]initWithFrame:CGRectMake(10, y,100,100)];
        imgView.image=[UIImage imageNamed:[imgArr objectAtIndex:i]];
        imgView.backgroundColor=[UIColor grayColor];
        [scrollView addSubview:imgView];
        
        
        UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(imgView.frame.size.width+imgView.frame.origin.x+20, y, 200, 20)];
        lblHeader.text=[nameArr objectAtIndex:i];
        lblHeader.font=[UIFont boldSystemFontOfSize:17.0];
        lblHeader.textAlignment=NSTextAlignmentLeft;
        lblHeader.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
        [scrollView addSubview:lblHeader];
        int h;
        if (i==0)
        {
              h=240;
        }else if (i==1)
        {
             h=430;
        }else if (i==2)
        {
             h=320;
        }
        lblText=[[UILabel alloc]initWithFrame:CGRectMake(10,imgView.frame.size.height+imgView.frame.origin.y+10, self.view.frame.size.width-20, h)];
        lblText.text= [dataArr objectAtIndex:i];
        //lblText.editable=NO;
        lblText.numberOfLines=73;
        lblText.font=[UIFont systemFontOfSize:13.0];
        lblText.textAlignment=NSTextAlignmentLeft;
       
        lblText.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        [scrollView addSubview:lblText];
        
        UIButton * btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(10, lblText.frame.size.height+lblText.frame.origin.y,200, 30);
        btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        
      
        
        
        NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:[btnArr objectAtIndex:i]];
        
        [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
        
        [btn setAttributedTitle:commentString forState:UIControlStateNormal];
          [btn setTitle:[btnArr objectAtIndex:i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
        btn.tag=i;
        [btn addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font=[UIFont systemFontOfSize:13];
        [scrollView addSubview:btn];
        
        
        
        
         y+=120+h+30;
        
    }
    
    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, y+100);

}
//-(void)cartbuttonPressed{
//    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//}

#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0) {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)btnPressed:(UIButton *)sender
{
    if (sender.tag==0)
    {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.raabstract.com/"]];
    }else if (sender.tag==1)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.maecouture.com/"]];
    }else if(sender.tag==2)
    {
          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.claudiomilano.com/"]];
    }
}


-(void)cartbuttonPressed{
    if (addtoCart) {
        
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else {
    [self coreDataFetch];
     
    
    
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
    arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
    [self.view addSubview:arwView];
    
    view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    
    //    view1.layer.borderWidth = 2.0;
    //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
    
    
    
    UIView * backView = [[UIView alloc]init];
    
    backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView];
    
    
    lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
    lblPrice1.textAlignment=NSTextAlignmentRight;
    lblPrice1.adjustsFontSizeToFitWidth=YES;
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
    
    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
        lblSubTotal.textAlignment=NSTextAlignmentRight;
    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [view1 addSubview:tableView_Cart];
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
    
    }
        addtoCart = false;
    }
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
        
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
    
    return cell;
}

-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}


-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
