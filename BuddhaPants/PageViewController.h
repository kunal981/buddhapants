//
//  PageViewController.h
//  BuddhaPants
//
//  Created by brst on 04/06/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIViewController

@property (assign, nonatomic) NSInteger indexNumber;
@end
