//
//  ForgetPasswordViewController.m
//  BuddhaPants
//
//  Created by brst on 25/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ForgetPasswordViewController.h"

@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden=NO;
     self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    self.view.backgroundColor=[UIColor whiteColor];
    
    UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 30)];
    lblHeader.text=@"Reset Password";
   lblHeader.font=[UIFont boldSystemFontOfSize:20.0];
    lblHeader.textAlignment=NSTextAlignmentCenter;
    lblHeader.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    [self.view addSubview:lblHeader];
    
    UILabel * lblText=[[UILabel alloc]initWithFrame:CGRectMake(10, lblHeader.frame.size.height+lblHeader.frame.origin.y+55,self.view.frame.size.width-20, 50)];
    lblText.text=@"We will send you an email to reset your password.";
    lblText.font=[UIFont systemFontOfSize:17.0];
    lblText.numberOfLines=2;
    lblText.textAlignment=NSTextAlignmentCenter;
    lblText.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
     [self.view addSubview:lblText];
    
    UILabel * lblEmail=[[UILabel alloc]initWithFrame:CGRectMake(20,lblText.frame.size.height+lblText.frame.origin.y+15, self.view.frame.size.width-40, 30)];
    lblEmail.text=@"Email";
    lblEmail.font=[UIFont systemFontOfSize:17.0];
    lblEmail.textAlignment=NSTextAlignmentLeft;
    lblEmail.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
    [self.view addSubview:lblEmail];
    
     txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(20,lblEmail.frame.size.height+lblEmail.frame.origin.y+5, self.view.frame.size.width-40, 35)];
     txtEmail.delegate=self;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtEmail.leftView = paddingView;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    txtEmail.keyboardType=UIKeyboardTypeEmailAddress;
    txtEmail.layer.borderColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0].CGColor;
    txtEmail.layer.borderWidth=1.0;
    [self.view addSubview:txtEmail];

    

    UIButton * btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(20,txtEmail.frame.size.height+txtEmail.frame.origin.y+20,100, 30);
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    
    [btn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
    btn.backgroundColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    
    [btn addTarget:self action:@selector(btnPressed) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.view addSubview:btn];
    
    UILabel * lblOr=[[UILabel alloc]initWithFrame:CGRectMake(btn.frame.size.width+btn.frame.origin.x+5,txtEmail.frame.size.height+txtEmail.frame.origin.y+20,25, 30)];
    lblOr.text=@"or";
    lblOr.font=[UIFont systemFontOfSize:15.0];
    lblOr.textAlignment=NSTextAlignmentLeft;
    lblOr.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
    [self.view addSubview:lblOr];

    UIButton * btnCancel=[UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame=CGRectMake(lblOr.frame.size.width+lblOr.frame.origin.x,txtEmail.frame.size.height+txtEmail.frame.origin.y+20,70, 30);
    btnCancel.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
         [btnCancel addTarget:self action:@selector(cancelBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    btnCancel.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.view addSubview:btnCancel];
    
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:@"Cancel"];
    
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    
    [btnCancel setAttributedTitle:commentString forState:UIControlStateNormal];
    //[salesBtn setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];



    // Do any additional setup after loading the view.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}
-(void)cancelBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)btnPressed
{
    
    if ([txtEmail.text isEqualToString:@""])
    {
        UIAlertView *  alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please fill your email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else
    {
        UIAlertView *  alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Password sent to your email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
