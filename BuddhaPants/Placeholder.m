//
//  Placeholder.m
//  TravelKit
//
//  Created by brst on 24/02/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "Placeholder.h"

@implementation Placeholder

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

    - (void) drawPlaceholderInRect:(CGRect)rect {
        UIColor *placeHolderTextColor;
        if (self.placeholder)
        {
            
            // color of placeholder text
            placeHolderTextColor = [UIColor colorWithRed:0.2667 green:0.7373 blue:0.6353 alpha:1.0];
            
           
            
            CGSize drawSize = [self.placeholder sizeWithAttributes:[NSDictionary dictionaryWithObject:self.font forKey:NSFontAttributeName]];
            CGRect drawRect = rect;
            
            // verticially align text
            drawRect.origin.y = (rect.size.height - drawSize.height) * 0.5;
            
            // set alignment
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.alignment = self.textAlignment;
            
            // dictionary of attributes, font, paragraphstyle, and color
            NSDictionary *drawAttributes = @{NSFontAttributeName: self.font,
                                             NSParagraphStyleAttributeName : paragraphStyle,
                                             NSForegroundColorAttributeName : placeHolderTextColor};
            
            
            // draw
            [self.placeholder drawInRect:drawRect withAttributes:drawAttributes];
            
        }
    }


@end
