//
//  RegisterViewController.m
//  BuddhaPants
//
//  Created by brst on 13/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController (){
    Api_Wrapper *obj;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtEmail.layer.borderColor = [UIColor whiteColor].CGColor;
    _txtEmail.layer.borderWidth = 0.30;
    _txtEmail.leftView = paddingView;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _firstName.leftView = paddingView1;
    _firstName.layer.borderColor = [UIColor whiteColor].CGColor;
    _firstName.layer.borderWidth = 0.30;
    _firstName.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtLastName.layer.borderColor = [UIColor whiteColor].CGColor;
    _txtLastName.layer.borderWidth = 0.30;
    _txtLastName.leftView = paddingView2;
    _txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPassword.leftView = paddingView3;
    _txtPassword.layer.borderColor = [UIColor whiteColor].CGColor;
    _txtPassword.layer.borderWidth = 0.30;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;

    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCreatePressed:(id)sender {
    NSString *rawString = [self.firstName text];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    
    NSString *rawString1 = [self.txtLastName text];
    NSCharacterSet *whitespace1 = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed1 = [rawString1 stringByTrimmingCharactersInSet:whitespace1];
    
    NSString *rawString2 = [self.txtEmail text];
    NSCharacterSet *whitespace2 = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed2 = [rawString2 stringByTrimmingCharactersInSet:whitespace2];
    
    NSString *rawString3 = [self.txtPassword text];
    NSCharacterSet *whitespace3 = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed3 = [rawString3 stringByTrimmingCharactersInSet:whitespace3];
    if ([trimmed length]==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter first name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    else if ([trimmed1 length]==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter last name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (self.txtPassword.text.length<5){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Password must be at least 5 characters " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if(![self NSStringIsValidEmail:self.txtEmail.text]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([trimmed3 length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if ([trimmed2 length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
     else {

        [self showLoadingView];
    //build an info object and convert to json
    BOOL flag = false;
    BOOL sendEmailFlag= true;
    NSDictionary *newDatasetInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.txtEmail.text, @"email", self.firstName.text, @"first_name", self.txtLastName.text,@"last_name",[NSNumber numberWithBool:flag],@"verified_email",[NSNumber numberWithBool:sendEmailFlag],@"send_email_invite",nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:newDatasetInfo,@"customer", nil];
    
    //convert object to data
    NSError *error =  nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&error];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://5fdc64a62f9a5e53c3c72411b8c2c461:0f36722dd80bfafbe3e956a006a31bc0@buddhapants-2.myshopify.com/admin/customers.json"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:jsonData];
    
    // print json:
    NSLog(@"JSON summary: %@", [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding]);
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
  

    }
    
}
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect frame=self.view.frame;
    frame.origin.y-=180;
    [self.view setFrame:frame];

}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect frame=self.view.frame;
    frame.origin.y=64;
    [self.view setFrame:frame];
    
}

-(NSString*)getJsonStringByDictionary:(NSDictionary*)dictionary{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    NSError *error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSArray *responseKeys = [result allKeys];
    NSLog(@"%@",result);
   /// NSArray *array = [result valueForKey:@"customers"];
    if ([responseKeys containsObject:@"customer"]) {
        LoginViewController *homeVC =[[LoginViewController alloc]init];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
    else{
        NSArray *errorArray = [result valueForKey:@"errors"];
        NSString *error = [[errorArray  valueForKey:@"email"]objectAtIndex:0] ;
        NSLog(@"%@",error);
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"Email %@",error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
 
  
    }
    [self hideLoadingView];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
    [self hideLoadingView];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
