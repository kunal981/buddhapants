//
//  RetailerViewController.h
//  BuddhaPants
//
//  Created by brst on 04/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RetailerViewController : UIViewController
{
    NSArray *retailer_Array;
    NSString *string;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextView *textView_retatiler;
@property (strong, nonatomic) IBOutlet UILabel *lblRetailers;

@end
