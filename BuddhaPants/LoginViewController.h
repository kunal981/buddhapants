//
//  LoginViewController.h
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgetPasswordViewController.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate>{
    
}
@property (strong, nonatomic) NSMutableArray *checkMail_array;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_bg;
@property (strong, nonatomic) IBOutlet UITextField *txtField_username;
@property (strong, nonatomic) IBOutlet UITextField *txtField_password;
@property (strong, nonatomic) IBOutlet UIButton *btn_login;
- (IBAction)btn_loginPressed:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *userNameY;
- (IBAction)btnRegisterPressed:(id)sender;

- (IBAction)forgetBtnPressed:(id)sender;


@end
