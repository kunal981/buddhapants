//
//  AboutViewController.m
//  BuddhaPants
//
//  Created by brst on 06/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController (){
    Api_Wrapper *obj;
}

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self aboutUsApi];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    UIBarButtonItem * rightBar=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart12"] style:UIBarButtonItemStylePlain target:self action:@selector(cartbuttonPressed)];
    self.navigationItem.rightBarButtonItem=rightBar;
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    // Do any additional setup after loading the view from its nib.
    
    
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:scrollView];
    
    
    UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width-20, 100)];
    lblHeader.text=@"Our mission is to create organic, fun, comfy, multi-functional garments just for you!";
    lblHeader.numberOfLines=3;
    lblHeader.textAlignment=NSTextAlignmentCenter;
    lblHeader.backgroundColor=[UIColor whiteColor];
    lblHeader.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    [scrollView addSubview:lblHeader];
    
}

-(void)cartbuttonPressed{
    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden =NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Api
-(void)aboutUsApi {
    
    [self showLoadingView];
    
    obj = [Api_Wrapper shared];
    
    
    [obj GET:nil string:aboutUS_String completion:^(NSDictionary *json, BOOL success) {
        NSLog(@"%@",json);
        
       NSArray *pages = [json valueForKey:@"page"];
        
        NSString *htmlString = [pages  valueForKey:@"body_html"];
         [self.wedView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://www.buddhapants.com/pages/about-buddha-pants"]];
       
        
        [self hideLoadingView];
        
    }];
}

-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


@end
