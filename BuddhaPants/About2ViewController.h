//
//  About2ViewController.h
//  BuddhaPants
//
//  Created by brst on 23/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface About2ViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
    UIScrollView * scrollView;
    UITextView * txtViewHeader;
    UIImageView * imgViewHeader;
    UILabel* lblText;
    
    NSMutableArray * imgArr,*nameArr,*dataArr,*btnArr;
    
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
    NSManagedObjectContext *managedObjectContext;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    UILabel* lblPrice1;
      BOOL addtoCart;
    
}
@end
