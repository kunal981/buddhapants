//
//  ProductViewController.h
//  BuddhaPants
//
//  Created by brst on 06/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ShippingAdressViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <MessageUI/MessageUI.h>

@import CoreData;

@class Cart;
@interface ProductViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate,MFMailComposeViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate>
{
    
    NSInteger selectedRow;
    
    UIPickerView *picker;
    NSMutableArray *sizeArray;
    NSMutableArray *colorArray;
    NSMutableArray *qtyArray;

    UITableView * tableView_Cart;
    UIView *cellView;
    UIImageView *profileImageView;
    UIImageView *slide_imageView,*slide_ArrowImageView1,*slide_ArrowImageView2 ;
    UILabel* lblProduct,*lblPrice;
    UIView *backGroundView;
    UIView * view1, * arwView;
    int noRow;
    int flag;
    AppDelegate *ad;
    NSArray *fetchedObjects;
    Cart *info;
    UILabel *lblSize;
    UILabel *lblBadage;
    UIButton *btn;
    UILabel *lblQty;
    
    NSMutableArray *table_Array;
    UILabel *lblPrice1;
    CGFloat width ;
    UIPageControl *pageControl;
    BOOL addtoCart;
    NSMutableArray *zoomImageArray;
}



@property (strong, nonatomic) NSFetchedResultsController *fetchedController;
@property (strong, nonatomic) IBOutlet UITextView *textView_descripation;
@property (strong, nonatomic) IBOutlet UILabel *lbl_productPrice;
@property (strong, nonatomic) IBOutlet UILabel *lbl_productName;
@property(strong,nonatomic) NSMutableArray *slideImageArray;
@property(strong,nonatomic) NSString *idForSelectedProduct;

@property(strong,nonatomic) NSString *str_lblName;
@property(strong,nonatomic) NSString *strHtml;

@property (strong,nonatomic)NSMutableArray *varientArray;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView1;
@property (strong, nonatomic) IBOutlet UITextField *txtSize;
@property (strong, nonatomic) IBOutlet UIScrollView *back_ScrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView_Size;
- (IBAction)btn_addToCartPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_SelectColor;
@property (strong, nonatomic) IBOutlet UITextField *txt_SelectQuanity;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UIImageView *colorArrow_image;

@property(strong,nonatomic)NSString * handleStr;
- (IBAction)facebookPressed:(id)sender;
- (IBAction)twitterPressed:(id)sender;
- (IBAction)emailPressed:(id)sender;
//- (IBAction)pinterestPressed:(id)sender;

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) IBOutlet UIButton *btnAddToCart;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
