//
//  RetailerViewController.m
//  BuddhaPants
//
//  Created by brst on 04/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "RetailerViewController.h"

@interface RetailerViewController ()
{
    Api_Wrapper *obj;
}

@end

@implementation RetailerViewController
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self retailersApi];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RetailersCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    UIBarButtonItem * rightBar=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart12"] style:UIBarButtonItemStylePlain target:self action:@selector(cartbuttonPressed)];
    self.navigationItem.rightBarButtonItem=rightBar;
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    // Do any additional setup after loading the view from its nib.
}



-(void)cartbuttonPressed
{
    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark Left bar button

-(void)barbuttonPressed
{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden =NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)retailersApi
{
    obj = [Api_Wrapper shared];
    
    
    [obj GET:nil string:retailers_String completion:^(NSDictionary *json, BOOL success) {
        NSLog(@"Reatailer = %@",json);
        
       
        retailer_Array = [json valueForKey:@"page"];
      
        
        NSString *htmlString = [retailer_Array  valueForKey:@"body_html"];
        
        
        
       // NSLog(@"HTML STRING = %@",htmlString);
        NSString *tittle = [retailer_Array valueForKey:@"title"];
        
       // NSLog(@"title for retailers = %@",tittle);
        
        
        
        
       // NSString *htmlString = @"CALIFORNIA/nMount Madonna Center";
        
       
//        NSString *str1 = @"Share\n Role \nPlay Photo via Facebook, or Twitter for free coins per photo.";
//        NSString *str2 = @"Like Role Play on facebook for 50 free coins.";
//        NSString *str3 = @"Check out 'What's Hot' on other ways to receive free coins";
//        
//        
//        NSString *htmlString = [NSString stringWithFormat:@"%@\n%@\n%@", str1, str2, str3];
//
        
        
//        string = @"CALIFORNIA\n\n Mount Madonna Center\n 445 Summit Road\n Watsonville, CA 95076\n mountmadonna.org\n (408) 846-4064\n\n In Heroes We Trust\n 300 Westminster Avenue\n Venice, CA 90291\n Inheroeswetrust.com\n (310) 310-8820\n\n\n CANADA\n\n Precious Earth\n 1213 15th Ave\n Regina, SK S4P 0Y8\n preciousearth.ca\n (306) 205-6959\n\n Downward Dog Yoga Centre\n 2nd floor, 735 Queen St W\n Toronto, ON M6J 1G1\n www.downwarddog.com\n (416) 703-8805\n\n Stoney Lake Wellness\n 3359 Lakefild Road\n Lakefield, Ontario K0L 2H0\n StoneylakeWellness.com\n\n Katies fitness\n 65 birchview cres\n Bolton, Ontario\n (416) 970-1484\n\n Woosahh\n 568 Ward street\n Nelson B.C. V1L1S9\n (778) 463-4220\n woosahh.com\n\n\n FLORIDA\n\n Studio 108\n 12644 Carlby Circle\n Tampa, FL 32751\n studio108tampa.com\n (813) 343-8887\n\n We Are Yoga\n 138 W. Granada Blvd\n Ormond Beach, FL 32174\n weareyoga.com\n (386) 677-9642\n\n Zen Den Yoga School\n 1740 Costa Del Sol\n Boca Raton, FL 33431\n zendenyogaschool.com\n (954) 461-4367\n\n Pulse 163\n 3447 NE 163rd Street\n North Miami Beach, FL 33160\n Pulse163.com\n (305) 904-6444\n\n AMBU Yoga\n 5400 South Seas Plantation Road\n Captiva, FL 33924\n AmbuYoga.com\n (239) 314-9642\n\n Tacos & Tequila Cantina\n 4834 Davis Blvd\n Naples, FL 34104\n Tacosandtequilanaples.com\n (239)732-8226\n\n\n GREECE\n\n Athens Yoga\n L. Riencourt 65-67\n Athens 11523\n Athensyoga.gr\n\n\n HAWAII\n\n Noelani Studios\n 66-437 Kamehameha Highway\n Haleiwa, HI 96712\n Noelanistudios.com\n (808) 389-3709\n\n\n\n INDIANA\n\n Love Your Body Yoga &\n Loungewear Boutique\n 1726 E. 86th Street\n Indianapolis, IN 46240\n (317) 698-1032\n\n\n MAINE\n\n Niraj Yoga\n 648 Congress Street\n Portland, Maine\n Nirajyoga.com\n (207) 318-1940\n\n\n NEW JERSEY\n\n Alluem Yoga\n 347 Lincoln Ave. East\n Cranford, NJ 07016\n Alluemyoga.com\n (908) 276-YOGA\n\n\n NEW YORK\n\n Satya Yoga Center\n 6400 Montgomery St.\n 3rd Floor\n Rhinebeck, NY 12572\n (845) 876-2528\n\n\n RHODE ISLAND\n\n Elevation Studio\n 74 W Side Road\n New Shoreham, RI 02807\n Elevationbi.com\n (401)466-9642\n\n\n PENNSYLVANIA\n\n Dye by Daquila\n 1710 Jancey St.\n Pittsburgh, PA 15206\n Daquilahair.com\n (412) 361-0900\n\n Imagine Your Yoga\n 36 N Beaver Street\n York, PA 17401\n (717) 900-8163\n imagineyouryoga.squarespace.com/\n\n\n SEATTLE\n\n Center for Yoga & Health\n 5340 Ballard Ave NW\n Seattle, Washington 98107\n KulaMovement.com\n (206) 972-2999\n\n\n WISCONSIN\n\n Yoga on the Lake\n 725B Woodlake Road\n Kohler, WI 53044\n (920) 453-2817\n\n\n UK\n\n Utopian Spirit\n Second Floor 79/81 High Street\n Godalmig, Surrey, GU 1AW\n +44(0)1483 200725\n www.UtopianSpirit.com\n\n\n For information on Wholesale\n Please write to\n Wholesale@BuddhaPants.com";
// 
// 
//        
//         self.textView_retatiler.text = string;
        
        
        
        
      
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        
        
        self.textView_retatiler.attributedText = attributedString;
        
        
        
        NSLog(@"%@",attributedString);
        
        
         self.textView_retatiler.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        self.textView_retatiler.tintColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        self.textView_retatiler.font=[UIFont boldSystemFontOfSize:15];
        
        self.lblRetailers.text = tittle;
        
        self.lblRetailers.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];

        self.lblRetailers.font=[UIFont boldSystemFontOfSize:20];

    }];
}

@end
