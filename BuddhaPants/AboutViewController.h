//
//  AboutViewController.h
//  BuddhaPants
//
//  Created by brst on 06/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController<UIWebViewDelegate>


{
    
    UIScrollView * scrollView;
    UITextView * txtViewHeader;
    UIImageView * imgViewHeader;
    
      BOOL addtoCart;
    
}

@property (strong, nonatomic) IBOutlet UIWebView *wedView;

@end
