//
//  Api_Wrapper.m
//  BuddhaPants
//
//  Created by brst on 14/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "Api_Wrapper.h"




#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#import "Reachability.h"


#define url @"https://buddhapants-2.myshopify.com/admin/"
#define server_url @"http://mfvs.cc/buddha/"


@implementation Api_Wrapper
static Api_Wrapper *shared = nil;
+ (Api_Wrapper *) shared
{
    @synchronized(self) {
        if (shared == nil){
            
            NSLog(@"dsadasdsafds");
            shared = [[Api_Wrapper alloc] init];
            
        }
    }
    
    return shared;
}

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}

- (void)hideProgressForView:(UIView*)view {
    if (_hud) {
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud = nil;
    }
}


-(void)POST:(id)dict string:(NSString*)urlString completion:(void(^)(NSDictionary *json,BOOL sucess))completion
{
     [AppDelegate showGlobalProgressHUDWithTitle:@":Loading"];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [AppDelegate dismissGlobalHUD];

        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
      
       // manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        NSString *newURl = [url stringByAppendingString:urlString];
        NSLog(@"%@",newURl);
        
        [manager POST:newURl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
            
            
            
            if (completion)
                completion(jsonDictionary, YES);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            completion(nil, NO);
        }];
    }
    
}



-(void)PostApi:(id)dictData string:(NSString*)serverUrl completion:(void(^)(NSDictionary *json,BOOL sucess))completion
{
    
    [AppDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [AppDelegate dismissGlobalHUD];
    }
    
    else
    {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        // manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        NSString *newURl = [server_url stringByAppendingString:serverUrl];
        NSLog(@"%@",newURl);
        
        [manager POST:newURl parameters:dictData success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
            
            
            
            if (completion)
                completion(jsonDictionary, YES);
            [AppDelegate dismissGlobalHUD];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            completion(nil, NO);
            [AppDelegate dismissGlobalHUD];
        }];
    }
    
}

-(void)PostApiVoting:(id)dictData string:(NSString*)serverUrl completion:(void(^)(NSDictionary *json,BOOL sucess))completion
{
    
   // [AppDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        //[AppDelegate dismissGlobalHUD];
    }
    
    else
    {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        // manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        NSString *newURl = [server_url stringByAppendingString:serverUrl];
        NSLog(@"%@",newURl);
        
        [manager POST:newURl parameters:dictData success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
            
            
            
            if (completion)
                completion(jsonDictionary, YES);
           // [AppDelegate dismissGlobalHUD];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            completion(nil, NO);
            //[AppDelegate dismissGlobalHUD];
        }];
    }
    
}


-(void)GET:(NSMutableDictionary*)dict string:(NSString*)urlString completion:(void(^)(NSDictionary *json ,BOOL success))completion
{
    
    [AppDelegate showGlobalProgressHUDWithTitle:@"Loading...."];

    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
         [AppDelegate dismissGlobalHUD];
    }
    
    else
    {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"5fdc64a62f9a5e53c3c72411b8c2c461" password:@"0f36722dd80bfafbe3e956a006a31bc0"];
        NSString *newURl = [url stringByAppendingString:urlString];
        NSLog(@"%@",newURl);
        
       [manager GET:newURl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
           NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
           
           
           
           if (completion)
               completion(jsonDictionary, YES);
           [AppDelegate dismissGlobalHUD];

       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           NSLog(@"%@",error);
           completion(nil, NO);
           [AppDelegate dismissGlobalHUD];

       }];
        
        
    }

}


-(void)GETStore:(NSMutableDictionary*)dict string:(NSString*)urlString completion:(void(^)(NSDictionary *json ,BOOL success))completion
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    
    else
    {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"5fdc64a62f9a5e53c3c72411b8c2c461" password:@"0f36722dd80bfafbe3e956a006a31bc0"];
        NSString *newURl = [url stringByAppendingString:urlString];
        NSLog(@"%@",newURl);
        
        [manager GET:newURl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
            
            
            
            if (completion)
                completion(jsonDictionary, YES);
           
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            completion(nil, NO);
            
            
        }];
        
        
    }
    
}


 
-(void)alertMSG:(NSString*)title message:(NSString*)msg
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

@end
