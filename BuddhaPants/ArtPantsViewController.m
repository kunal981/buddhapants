//
//  ArtPantsViewController.m
//  BuddhaPants
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ArtPantsViewController.h"

@interface ArtPantsViewController ()

@end

@implementation ArtPantsViewController


-(void)loadView
    {
        UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
        
        self.view = view;
    }

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    selectedBtnColor=[UIColor colorWithRed:0.7176 green:0.7176 blue:0.7176 alpha:1.0];
    notselectedBtnColor=[UIColor colorWithRed:0.7765 green:0.7765 blue:0.7765 alpha:1.0];

    
    
    UIImageView* imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -30, self.view.frame.size.width, self.view.frame.size.height-50)];
    imgView.image=[UIImage imageNamed:@"artpants.jpg"];
    imgView.contentMode=UIViewContentModeScaleAspectFill;
   // imgView.layer.masksToBounds=YES;
    [self.view addSubview:imgView];
    
//     selectedBtnColor=[UIColor colorWithRed:0.7176 green:0.7176 blue:0.7176 alpha:1.0];
//     notselectedBtnColor=[UIColor colorWithRed:0.7765 green:0.7765 blue:0.7765 alpha:1.0];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
    
    

    [self uppeViewArtPantsMethod];
    [self lowerButton];
    
         
}

#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"Select"]) {
        
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ButtonSelect"]) {
        voteButton.backgroundColor = notselectedBtnColor;
        uploadButton.backgroundColor=selectedBtnColor;
        [uploadButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
        [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else if (![[NSUserDefaults standardUserDefaults]boolForKey:@"ButtonSelect"]){
        voteButton.backgroundColor = selectedBtnColor;
        uploadButton.backgroundColor=notselectedBtnColor;
        [voteButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
        [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 
     }
    }
    
    addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0) {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }
    
    
}

-(void)uppeViewArtPantsMethod
{
//    UILabel * lblArtPants=[[UILabel alloc]initWithFrame:CGRectMake(10,100, self.view.frame.size.width-20, 40)];
//    lblArtPants.text=@"A R T  P A N T S";
//    lblArtPants.font=[UIFont fontWithName:@"HelveticaNeue" size:33];
//    lblArtPants.textColor=[UIColor whiteColor];
//    lblArtPants.adjustsFontSizeToFitWidth=YES;
//    lblArtPants.textAlignment=NSTextAlignmentCenter;
//    [self.view addSubview:lblArtPants];
//    
//    UIView *whiteLine=[[UIView alloc]initWithFrame:CGRectMake( (self.view.frame.size.width-200)/2, lblArtPants.frame.size.height+lblArtPants.frame.origin.y+15,150, 1.5)];
//    whiteLine.backgroundColor=[UIColor whiteColor];
//    [self.view addSubview:whiteLine];
//    
//    UILabel *    lblsecond=[[UILabel alloc]initWithFrame:CGRectMake(50,whiteLine.frame.size.height+whiteLine.frame.origin.y+50, self.view.frame.size.width-100, 45)];
//    lblsecond.text=@"Want your Artwork on Buddha Pants?";
//    lblsecond.font=[UIFont fontWithName:@"HelveticaNeue" size:18];
//    lblsecond.textColor=[UIColor whiteColor];
//    lblsecond.numberOfLines=2;
//    lblsecond.adjustsFontSizeToFitWidth=YES;
//    lblsecond.textAlignment=NSTextAlignmentCenter;
//    [self.view addSubview:lblsecond];
//    
//  UILabel *  lblthirdSubmit=[[UILabel alloc]initWithFrame:CGRectMake(50,lblsecond.frame.size.height+lblsecond.frame.origin.y+20, self.view.frame.size.width-100, 45)];
//    lblthirdSubmit.text=@"Submit a design and lets make it happen!";
//    lblthirdSubmit.font=[UIFont fontWithName:@"HelveticaNeue" size:18];
//    lblthirdSubmit.textColor=[UIColor whiteColor];
//    lblthirdSubmit.numberOfLines=2;
//    lblthirdSubmit.adjustsFontSizeToFitWidth=YES;
//    lblthirdSubmit.textAlignment=NSTextAlignmentCenter;
//    [self.view addSubview:lblthirdSubmit];
//    
//    UIImageView* arwimagview=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-130 ,lblthirdSubmit.frame.size.height+lblthirdSubmit.frame.origin.y+20,50, 100)];
//    arwimagview.image=[UIImage imageNamed:@"arrow.png"];
//    [self.view addSubview:arwimagview];



    
}

#pragma mark Right bar button

-(void)cartbuttonPressed{
    if (addtoCart) {
        
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else {
        subTotal= 0;
        [self coreDataFetch];
        
        
        
        backGroundView = [[UIView alloc]init];
        backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        backGroundView.backgroundColor = [UIColor blackColor];
        backGroundView.alpha =  0.50;
        
        [self.view addSubview:backGroundView];
        
        
        arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
        
        arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
        
        [self.view addSubview:arwView];
        
        view1 = [[UIView alloc]init];
        
        view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
        view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
        
        //    view1.layer.borderWidth = 2.0;
        //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
        [self.view addSubview:view1];
        
        
        
        
        UIView * backView = [[UIView alloc]init];
        
        backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
        
        backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
        backView.layer.cornerRadius = 7.0;
        
        [view1 addSubview:backView];
        
        
        lblPrice1  = [[UILabel alloc]init];
        lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        lblPrice1.textAlignment=NSTextAlignmentRight;
        lblPrice1.adjustsFontSizeToFitWidth=YES;
        lblPrice1.font = [UIFont boldSystemFontOfSize:17];
        lblPrice1.textColor = [UIColor lightGrayColor];
        
        [backView addSubview:lblPrice1];
        
        
        UILabel *lblSubTotal  = [[UILabel alloc]init];
        lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
        lblSubTotal.text = @"SubTotal";
        lblSubTotal.textAlignment=NSTextAlignmentRight;
        
        lblSubTotal.textColor = [UIColor lightGrayColor];
        lblSubTotal.font = [UIFont systemFontOfSize:13];
        [backView addSubview:lblSubTotal];
        
        
        
        tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
        
        tableView_Cart.dataSource=self;
        tableView_Cart.delegate=self;
        
        tableView_Cart.backgroundColor=[UIColor whiteColor];
        [view1 addSubview:tableView_Cart];
        
        UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeSystem];
        
        btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
        btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
        [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
        [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnCheckOut];
        
    }
        addtoCart = false;
    }
    
}

#pragma mark Lower button
-(void)lowerButton
{
    
    
    CGFloat width=[UIScreen mainScreen].bounds.size.width;
    
    UIView * buttonLowerView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-84, width, 40)];
    buttonLowerView.backgroundColor= [UIColor whiteColor];
    [self.view addSubview:buttonLowerView];
    
    NSLog(@"%@",NSStringFromCGRect(buttonLowerView.frame));
    
    voteButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    voteButton.frame = CGRectMake(0, 0, (self.view.frame.size.width)/2-1, 40);
    voteButton.backgroundColor = notselectedBtnColor;
    [voteButton setTitle:@"VOTE" forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    voteButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [voteButton addTarget:self action:@selector(voteButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:voteButton];
    
    uploadButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    uploadButton.frame = CGRectMake(voteButton.frame.size.width+voteButton.frame.origin.x+2                                                                                                                                                                                                                                       , 0, (self.view.frame.size.width)/2-1, 40);
    uploadButton.backgroundColor = notselectedBtnColor;
    [uploadButton setTitle:@"UPLOAD" forState:UIControlStateNormal];
     uploadButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [uploadButton addTarget:self action:@selector(uploadButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:uploadButton];


}

-(void)voteButtonPresssed
{
     [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"ButtonSelect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    voteButton.backgroundColor = selectedBtnColor;
    uploadButton.backgroundColor=notselectedBtnColor;
     [voteButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
     [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonSelected = NO;
    VoteViewController * voteVC=[[VoteViewController alloc]init];
    
    [self.navigationController pushViewController:voteVC animated:YES];

}
-(void)uploadButtonPresssed
{
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"ButtonSelect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    buttonSelected = YES;
    voteButton.backgroundColor = notselectedBtnColor;
    uploadButton.backgroundColor=selectedBtnColor;
     [uploadButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
     [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    
    UploadViewController * voteVC=[[UploadViewController alloc]init];
    
    [self.navigationController pushViewController:voteVC animated:YES];
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,70,70)];
        
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
    
    return cell;
}


-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}


-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
