//
//  MYCell.h
//  BuddhaPants
//
//  Created by brst on 03/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_product;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Tittle;

@end
