//
//  ProductViewController.m
//  BuddhaPants
//
//  Created by brst on 06/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ProductViewController.h"
#import "PageViewController.h"
#import "ZomeImageViewController.h"

#define IMAGE_TAG 1
@interface ProductViewController ()
{
    NSManagedObjectContext *managedObjectContext;
    CGFloat kScrollObjWidth	 ;
    UIView *viewForPeriodPicker,*backView;
}

@end
static float subTotal;
@implementation ProductViewController
@synthesize scrollView1;
@synthesize managedObjectContext = _managedObjectContext;
const CGFloat kScrollObjHeight	= 210.0;

const NSUInteger kNumImages		= 5;

- (void)layoutScrollImages
{
    UIImageView *view = nil;
    NSArray *subviews = [scrollView1 subviews];
    
    // reposition all image subviews in a horizontal serial fashion
    CGFloat curXLoc = 0;
    for (view in subviews)
    {
        if ([view isKindOfClass:[UIImageView class]] && view.tag > 0)
        {
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 0);
            view.frame = frame;
            
            curXLoc += (kScrollObjWidth);
        }
    }
    
    NSLog(@"%f",kScrollObjWidth);
    
    // set the content size so it can be scrollable
    [scrollView1 setContentSize:CGSizeMake((self.slideImageArray.count * kScrollObjWidth), [scrollView1 bounds].size.height)];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.back_ScrollView.delegate = self;
    
    
    self.activityIndicator.hidden = YES;
    
    zoomImageArray = [[NSMutableArray alloc]init];
    self.btnAddToCart.backgroundColor= [UIColor colorWithRed:0.3765 green:0.8863 blue:0.8196 alpha:1.0];
    scrollView1.delegate=self;
    int height=[UIScreen mainScreen].bounds.size.height;
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
        width =  320;

    }
    else if ([UIScreen mainScreen].bounds.size.height==667){
        width = 375;
    }
   
    
    
    if (height<=568)
    {
        kScrollObjWidth=320.0;
    }else if (height>568 && height<960)
    {
        kScrollObjWidth=375;

    }else if (height==960)
    {
        kScrollObjWidth=540;
    }
   
    
    [self showLoadingView];
    
    qtyArray=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
    table_Array = [[NSMutableArray alloc]init];
    sizeArray = [[NSMutableArray alloc]init];
    colorArray = [[NSMutableArray alloc]init];
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
     managedObjectContext = [ad managedObjectContext];
    NSLog(@"%@",self.varientArray);
    
   
    
    noRow = 4;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtSize.leftView = paddingView;
    _txtSize.leftViewMode = UITextFieldViewModeAlways;
    
    for (int i =0; i<self.varientArray.count; i++) {
        NSString *si = [[self.varientArray objectAtIndex:i]valueForKey:@"option1"];
        [sizeArray addObject:si];
    }
    
    for (int i =0; i<self.varientArray.count; i++) {
        NSString *si = [[self.varientArray objectAtIndex:i]valueForKey:@"option2"];
        if (![si isKindOfClass:[NSNull class]]) {
              [colorArray addObject:si];
        }
      
    }
    
    if (colorArray.count == 0) {
        NSLog(@"ddd");
       self.txt_SelectQuanity.frame = CGRectMake(self.txt_SelectQuanity.frame.origin.x, self.txtSize.frame.origin.y+self.txtSize.frame.size.height+15, self.txtSize.frame.size.width, self.txtSize.frame.size.height);
        
        [self.txt_SelectColor setHidden:YES];
        [self.colorArrow_image setHidden:YES];
    }
    else{
        [self.txt_SelectColor setHidden:NO];
        [self.colorArrow_image setHidden:NO];
    }
   
    
    
    self.txtSize.layer.borderColor = [UIColor colorWithRed:0.3176 green:0.7412 blue:0.6314 alpha:1.0].CGColor;
    self.txtSize.layer.borderWidth = 1.0;
    
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txt_SelectColor.leftView = paddingView1;
    _txt_SelectColor.leftViewMode = UITextFieldViewModeAlways;
    
  
    
    self.txt_SelectColor.layer.borderColor = [UIColor colorWithRed:0.3176 green:0.7412 blue:0.6314 alpha:1.0].CGColor;
    self.txt_SelectColor.layer.borderWidth = 1.0;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txt_SelectQuanity.leftView = paddingView2;
    _txt_SelectQuanity.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    self.txt_SelectQuanity.layer.borderColor = [UIColor colorWithRed:0.3176 green:0.7412 blue:0.6314 alpha:1.0].CGColor;
    self.txt_SelectQuanity.layer.borderWidth = 1.0;
    
    
    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
  
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
   
    
   
    // 1. setup the scrollview for multiple images and add it to the view controller
    //
    // note: the following can be done in Interface Builder, but we show this in code for clarity
   // [scrollView1 setBackgroundColor:[UIColor redColor]];
    [scrollView1 setCanCancelContentTouches:NO];
    scrollView1.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    scrollView1.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
    scrollView1.scrollEnabled = YES;
    scrollView1.delegate=self;
    [self createPageController];
    
    // pagingEnabled property default is NO, if set the scroller will stop or snap at each photo
    // if you want free-flowing scroll, don't set this property.
    scrollView1.pagingEnabled = YES;
    
    // load all the images from our bundle and add them to the scroll view
    NSUInteger i;
  
    NSLog(@"%@",self.slideImageArray);
    
    
    
    
     //int width=[UIScreen mainScreen].bounds.size.width;
    
    CGFloat x=0;
    for (i = 0; i < self.slideImageArray.count; i++)
    {
       
        
        
      slide_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, width, 170)];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnImage:)];
        tap.numberOfTapsRequired = 1 ;
         slide_imageView.tag = i;	// tag our images for later use when we place them in serial fashion
        slide_imageView.userInteractionEnabled = YES;
        [slide_imageView addGestureRecognizer:tap];
        
       
        slide_imageView.contentMode=UIViewContentModeScaleAspectFit;
        
        //slide_imageView.backgroundColor=[UIColor redColor];
        slide_ArrowImageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, slide_imageView.frame.size.height/2-15, 30, 40)];
        
        //slide_ArrowImageView1.image = [UIImage imageNamed:@"move_left.png"];
        
        
        slide_ArrowImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(slide_imageView.frame.size.width-30, slide_imageView.frame.size.height/2-15, 30, 30)];
        //slide_ArrowImageView2.image = [UIImage imageNamed:@"move_right.png"];
        
        if (i==0 ) {
            slide_ArrowImageView1.hidden = YES;
        }
        else{
           slide_ArrowImageView1.hidden = NO;
        }
        if (i == self.slideImageArray.count-1) {
            slide_ArrowImageView2.hidden = YES;

        }
        else{
            slide_ArrowImageView2.hidden = NO;
 
        }
        
        
        [slide_imageView addSubview:slide_ArrowImageView1];

        [slide_imageView addSubview:slide_ArrowImageView2];
        
        
        
            [slide_imageView sd_setImageWithURL:[NSURL URLWithString:[[self.slideImageArray objectAtIndex:i]valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
        
        
        
//        [slide_imageView sd_setImageWithURL:[NSURL URLWithString:[[self.slideImageArray objectAtIndex:i]valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            [zoomImageArray addObject:image];
//
//         }];
        
              [scrollView1 addSubview:slide_imageView];
        
        x+=width;
        
    }
    
    if (self.slideImageArray == nil|| self.slideImageArray==(id)[NSNull null] || self.slideImageArray.count==0)
    {
        NSLog(@"null image");
        
         slide_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, width, 170)];
        slide_imageView.contentMode = UIViewContentModeScaleAspectFit;
        slide_imageView.image = [UIImage imageNamed:@"ic_no_image.jpg"];
        [scrollView1 addSubview:slide_imageView];
    }
    
    _lbl_productName.text = [NSString stringWithFormat:@"%@",self.str_lblName];
    
    self.lbl_productPrice.text =[NSString stringWithFormat:@"$%@",[[self.varientArray objectAtIndex:0] valueForKey:@"price"]];
    
   [scrollView1 setContentSize:CGSizeMake((self.slideImageArray.count * width), 180)];
    
    //[self layoutScrollImages];	//
    
    
    
    // Do any additional setup after loading the view from its nib.
    
    // [self pagecontrollerMethod];
}
-(void)createPageController
{
    scrollView1.pagingEnabled=YES;
   pageControl = [[UIPageControl alloc] init];
   pageControl.backgroundColor=[UIColor colorWithRed:0.3765 green:0.8863 blue:0.8196 alpha:1.0];
    pageControl.frame = CGRectMake(0,170,scrollView1.frame.size.width,20);
    pageControl.numberOfPages = self.slideImageArray.count;
    pageControl.currentPage = 0;
    [self.back_ScrollView addSubview:pageControl];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView1.frame.size.width;
    float fractionalPage = scrollView1.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    pageControl.currentPage = page;
}


-(void)pagecontrollerMethod
{
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    slide_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 320, 210)];
    

    PageViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];

}



#pragma  - UIPageViewController Methods
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger index = [(PageViewController *)viewController indexNumber];
    
    if (index == 0)
    {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
    
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    NSUInteger index = [(PageViewController *)viewController indexNumber];
    
    
    index++;
    
    if (index == 3)
    {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (PageViewController *)viewControllerAtIndex:(NSUInteger)index
{
    
    PageViewController *childViewController = [[PageViewController alloc] initWithNibName:@"PageViewController" bundle:nil];
    childViewController.indexNumber = index;
    
    return childViewController;
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return 3;
}



- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;

}



-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
   
    
    
    self.back_ScrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+self.textView_descripation.frame.size.height);
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    //self.activityIndicator.hidden = YES;
    
    [self hideLoadingView];
    
    addtoCart = true;
 
    if (_strHtml == nil|| _strHtml==(id)[NSNull null])
    {
        NSLog(@"null string");
    }
    else
    {
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[_strHtml dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        self.textView_descripation.attributedText = attributedString;
        self.textView_descripation.font=[UIFont systemFontOfSize:13];
        [self.textView_descripation sizeToFit];

    }
    
      [self coreDataFetch];
    if (fetchedObjects.count==0)
    {
        [backGroundView removeFromSuperview];
        [arwView removeFromSuperview];
        [view1 removeFromSuperview];
        

    }
    else
    {
       // [tableView_Cart setHidden:NO];
        
    }
    subTotal= 0;
    if (fetchedObjects.count==0)
    {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }

    [tableView_Cart reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITextField

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    if (textField == self.txtSize) {
        flag = 1;
        [_txtSize resignFirstResponder];
        [viewForPeriodPicker removeFromSuperview];
        NSLog(@"%f",self.view.frame.size.width);
        viewForPeriodPicker = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-210 ,self.view.frame.size.width,210)];
        viewForPeriodPicker.backgroundColor=[UIColor clearColor];
        
        UIToolbar *toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        
        [viewForPeriodPicker addSubview:toolBarForPeriodPicker];
        
        
        
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width , 180)];
        picker.showsSelectionIndicator = YES;
        picker.backgroundColor=[UIColor whiteColor];
        picker.hidden = NO;
        picker.delegate = self;
        picker.dataSource=self;
        
        [viewForPeriodPicker addSubview:picker];
        
        [self.view addSubview:viewForPeriodPicker];
        
        CGRect frame1=self.back_ScrollView.frame;
        frame1.origin.y=-46;
        
        [self.back_ScrollView setFrame:frame1];

    }else if (textField == self.txt_SelectColor){
        flag = 2;
        [_txt_SelectColor resignFirstResponder];
         [viewForPeriodPicker removeFromSuperview];
        viewForPeriodPicker = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-210 ,self.view.frame.size.width,210)];
        viewForPeriodPicker.backgroundColor=[UIColor clearColor];
        
        UIToolbar *toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        
        [viewForPeriodPicker addSubview:toolBarForPeriodPicker];
        
        
        
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width , 180)];
        picker.showsSelectionIndicator = YES;
        picker.backgroundColor=[UIColor whiteColor];
        picker.hidden = NO;
        picker.delegate = self;
        picker.dataSource=self;
        
        [viewForPeriodPicker addSubview:picker];
        
        [self.view addSubview:viewForPeriodPicker];

        
        CGRect frame1=self.back_ScrollView.frame;
        frame1.origin.y=-76;
        
        [self.back_ScrollView setFrame:frame1];

    }
    if (textField == self.txt_SelectQuanity) {
        
       


        flag=3;
        [viewForPeriodPicker removeFromSuperview];
        viewForPeriodPicker = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-210 ,self.view.frame.size.width,210)];
        viewForPeriodPicker.backgroundColor=[UIColor clearColor];
        
        UIToolbar *toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        
        [viewForPeriodPicker addSubview:toolBarForPeriodPicker];
        
        
        
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width , 180)];
        picker.showsSelectionIndicator = YES;
        picker.backgroundColor=[UIColor whiteColor];
        picker.hidden = NO;
        picker.delegate = self;
        picker.dataSource=self;
        
        [viewForPeriodPicker addSubview:picker];
        
        [self.view addSubview:viewForPeriodPicker];
        [self.txt_SelectQuanity resignFirstResponder];
        CGRect frame1=self.back_ScrollView.frame;
        frame1.origin.y=-120;
        
        [self.back_ScrollView setFrame:frame1];
        
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    CGRect frame1=self.view.frame;
//    frame1.origin.y=100;
//  [self.back_ScrollView setFrame:frame1];

}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}



-(void)cancelBtnPressedPeriod
{
    CGRect frame1=self.back_ScrollView.frame;
    frame1.origin.y=0;
    
    [self.back_ScrollView setFrame:frame1];
    [picker removeFromSuperview];
    [viewForPeriodPicker removeFromSuperview];
}


-(void)doneBtnPressedPeriod
{
   
    CGRect frame1=self.back_ScrollView.frame;
    frame1.origin.y=0;
    
    [self.back_ScrollView setFrame:frame1];
    
    
    //[tableViewAdding reloadData];
    [picker removeFromSuperview];
    [viewForPeriodPicker removeFromSuperview];
    
}


#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}



- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
     if (flag==1){
       return [sizeArray count];
    }
    else if(flag==2)
        return colorArray.count;
    else
        return qtyArray.count;
}



- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if (flag==1) {
        return [sizeArray objectAtIndex:row];

    }
    else if(flag==2)
        
        return [colorArray objectAtIndex:row];
    else
            return [qtyArray objectAtIndex:row];
    }



#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    if (flag==1)
    {
        
        selectedRow=row;
    NSString *resultString = [[NSString alloc] initWithFormat:
                              @"%@",[sizeArray objectAtIndex:row]];
    _txtSize.text = resultString;
        NSLog(@"%@",[[self.varientArray objectAtIndex:row] valueForKey:@"price"]);
        self.lbl_productPrice.text =[NSString stringWithFormat:@"%@", [[self.varientArray objectAtIndex:row] valueForKey:@"price"]];
        
    _txtSize.textColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];

   // [picker removeFromSuperview];
    }
    else if (flag==2)
    {
        NSString *resultString = [[NSString alloc] initWithFormat:
                                  @"%@",[colorArray objectAtIndex:row]];
        _txt_SelectColor.text = resultString;
        _txt_SelectColor.textColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
        
       // [picker removeFromSuperview];
 
    }
    else
    {
        NSString *resultString = [[NSString alloc] initWithFormat:
                                  @"%@",[qtyArray objectAtIndex:row]];
        self.txt_SelectQuanity.text = resultString;
        self.txt_SelectQuanity.textColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
       
        //[picker removeFromSuperview];

    }
   
}

#pragma mark ADD to Cart

-(void)cartbuttonPressed
{
    
    if (addtoCart)
    {
        
    
    
    subTotal=0;
    
    [self viewWillAppear:YES];
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    //[lblBadage setHidden:YES];
    
   
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
     arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
        [self.view addSubview:arwView];
    
     view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
 
//    view1.layer.borderWidth = 2.0;
//    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
   

    
    UIView * backView1 = [[UIView alloc]init];
    
    backView1.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView1.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView1.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView1];

    
     lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
    //lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.textAlignment=NSTextAlignmentRight;
    lblPrice1.adjustsFontSizeToFitWidth=YES;
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
   

    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
    lblSubTotal.textAlignment=NSTextAlignmentRight
    ;
    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [self subTotal];
    [view1 addSubview:tableView_Cart];
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
        
        addtoCart = false;
    }
  
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
       if (fetchedObjects.count==0) {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];

        return 0;
           }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
  return  fetchedObjects.count;
    }
  
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
        profileImageView.tag = IMAGE_TAG;
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
       
      
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
         lblPrice.layer.borderColor = [UIColor redColor].CGColor;
       
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
         lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
       
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        
        
        [cellView addSubview:lblQty];

        
    }
    Cart *obj = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj.image];
    
    
    lblProduct.text =obj.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj.size];
    lblQty.text = obj.qty;
    
   
   
        
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}



-(void)subTotal
{
    for (int i  =0 ; i<fetchedObjects.count; i++)
    {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        float result= [obj.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        

    }
}

-(void)subttotalDelete:(float)result
{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
        su =    su - result;
        
        NSLog(@"toatal final%f",su);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
      if (editingStyle == UITableViewCellEditingStyleDelete) {
         
          [tableView_Cart beginUpdates];
          
          AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
          NSManagedObjectContext *context = [delegate managedObjectContext];
          Cart *obj = [fetchedObjects objectAtIndex:indexPath.row];
          NSLog(@"%@",fetchedObjects);
          NSLog(@"%@",obj.totalprice);
          float result= [obj.totalprice floatValue];
          
          
          // subTotal =  subTotal - result;
          NSLog(@"total :%f",result);
          
          [context deleteObject:obj];
          [context save:nil];
          
          
          
          
        
          [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
          
         [self viewWillAppear:YES];
         [self subttotalDelete:result];

         
          [tableView_Cart endUpdates];
      }
}

-(void)checkOutPresssed
{
    
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}



- (IBAction)btn_addToCartPressed:(id)sender
{
  
    if (self.txtSize.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please select size" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if (self.txt_SelectQuanity.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please select quantity" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        
        
      
        
        
        [self coreDataInsert:selectedRow];
        [self cartbuttonPressed];
    
    
        }
    
}
//#pragma mark Database Check
//-(void)checkProductExistence
//{
//    
//    
//    
//    managedObjectContext = [ad managedObjectContext];
//    NSError *error;
//
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
//                                              inManagedObjectContext:managedObjectContext];
//    [fetchRequest setEntity:entity];
//   
//     NSPredicate* predicateID=   [NSPredicate predicateWithFormat:@"varientIDStr = %@",  str];
//    
//    [fetchRequest setPredicate:predicateID];
//
//    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
//    
//    
//    
//    if (fetchedObjects != nil)
//    {
//        [self coreDataInsert:selectedRow];
//        [self cartbuttonPressed];
//    }
//    
//    NSLog(@"%@",fetchedObjects);
//    
//    
//     }
//
//

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}



#pragma mark CoreData
-(void)coreDataInsert:(NSInteger)index
{
    
    NSString* str=[NSString stringWithFormat:@"%@",[[self.varientArray objectAtIndex:selectedRow]valueForKey:@"id"]];

    managedObjectContext = [self managedObjectContext];
    
    NSError * error;
  
    NSLog(@"%@ ",[[self.varientArray objectAtIndex:index] valueForKey:@"id"]);
  
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate* predicateID=   [NSPredicate predicateWithFormat:@"varientIDStr = %@",  str];
    
    [fetchRequest setPredicate:predicateID];
    
    
    NSArray * myArr = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"%@",myArr);
    Cart *customer;
    if (myArr.count==0)
    {
        NSLog(@"nothing");
    }else
    {
        
        customer = [myArr objectAtIndex:0];
        
    
        NSString * idVar=[NSString stringWithFormat:@"%@",[myArr valueForKey:@"varientIDStr"]];
        
        NSLog(@"%@",idVar);

    }
    
   
    
    if (customer )
    {
       
        
        NSString *strPrice = self.lbl_productPrice.text;
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSNumber *myPrice = [f numberFromString:strPrice];
        
        NSLog(@"%@",myPrice);
        
        customer.price = myPrice;
         customer.qty=self.txt_SelectQuanity.text;
        
        NSNumberFormatter *fQty = [[NSNumberFormatter alloc]init];
        fQty.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSNumber *myQty = [ fQty numberFromString:customer.qty];
        
        float Price = [myPrice floatValue];
        int  qty = [myQty intValue];
        float sum = (Price * qty);
        
        
        NSNumber *totalPrice  = [NSNumber numberWithFloat:sum];
        
        customer.totalprice = totalPrice;
        
        
    }
    
    else
    {
        
        Cart *addData = [NSEntityDescription insertNewObjectForEntityForName:@"Cart" inManagedObjectContext:managedObjectContext];
        
        
        addData.varientIDStr=[NSString stringWithFormat:@"%@",[[self.varientArray objectAtIndex:index] valueForKey:@"id"] ];
        
        
        
        addData.varId=[NSString stringWithFormat:@"%@",[[self.varientArray objectAtIndex:0] valueForKey:@"product_id"] ];
        
        addData.image = UIImageJPEGRepresentation(slide_imageView.image, 1);
        addData.name = self.str_lblName;
        
        NSString *strPrice = self.lbl_productPrice.text;
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSNumber *myPrice = [f numberFromString:strPrice];
        
        NSLog(@"%@",myPrice);
        
        addData.price = myPrice;
        addData.size = self.txtSize.text;
        addData.color  = self.txt_SelectColor.text;
        addData.qty = self.txt_SelectQuanity.text;
        
        NSNumberFormatter *fQty = [[NSNumberFormatter alloc]init];
        fQty.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSNumber *myQty = [ fQty numberFromString:addData.qty];
        
        float Price = [myPrice floatValue];
        int  qty = [myQty intValue];
        float sum = (Price * qty);
        
        NSLog(@"%f",sum);
        NSNumber *totalPrice  = [NSNumber numberWithFloat:sum];
        
        addData.totalprice = totalPrice;
        [managedObjectContext save:&error];

    }
    
}

-(void)coreDataFetch{
    
    
    NSError *error;
    
    managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
    NSLog(@"%@",fetchedObjects);
    
    
    
}

#pragma mark UITapGesture 
-(void)tapOnImage:(UITapGestureRecognizer*)sender
{
   
    NSLog(@"%@",zoomImageArray);
    
    UIView *view = sender.view;

    NSLog(@"%ld",(long)view.tag);
    
    //[AppDelegate showGlobalProgressHUDWithTitle:@"Loading...."];
    
    
    //self.activityIndicator.hidden = NO;
    
    ZomeImageViewController *zoomVC = [[ZomeImageViewController alloc]init];
    
    //[self showLoadingView];

    
    zoomVC.imgUrl = [[_slideImageArray objectAtIndex:view.tag] valueForKey:@"src"];
    zoomVC.imgIndex = view.tag;
    zoomVC.slideZoomArray = _slideImageArray;
    [self presentViewController:zoomVC animated:YES completion:nil];
    
}


//-(void)deleteCoreData{
//    managedObjectContext = [ad managedObjectContext];
//    
//    NSFetchRequest *fetchAllObjects = [[NSFetchRequest alloc] init];
//    
//    [fetchAllObjects setEntity:[NSEntityDescription entityForName:@"Cart" inManagedObjectContext:managedObjectContext]];
//    [fetchAllObjects setIncludesPropertyValues:NO]; //only fetch the managedObjectID
//    NSLog(@"%@",fetchAllObjects);
//    NSError *error = nil;
//    NSArray *allObjects = [managedObjectContext executeFetchRequest:fetchAllObjects error:&error];
//    // uncomment next line if you're NOT using ARC
//    // [allObjects release];
//    
//    for (NSManagedObject *object in allObjects) {
//        [managedObjectContext deleteObject:object];
//    }
//}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    
    //[self showLoadingView];
    
    
    //self.activityIndicator.hidden = NO;
    
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}



-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}



-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



- (IBAction)facebookPressed:(id)sender
{
    
    [self showLoadingView];
    
    __weak __typeof(self) weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        __strong __typeof(self) strongLocalSelf = weakSelf;
    SLComposeViewController *controller = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
        
    SLComposeViewControllerCompletionHandler myBlock =
    ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled)
        {
            NSLog(@"Cancelled");
            [self hideLoadingView];
        }
        else
        {
            NSLog(@"Done");
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Successfully posted!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
        }
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    controller.completionHandler =myBlock;
    //Adding the Text to the facebook post value from iOS
    [controller setInitialText:self.textView_descripation.text];
    //Adding the URL to the facebook post value from iOS
    [controller setTitle:self.lbl_productName.text];
   
      NSString * url=@"http://www.buddhapants.com/products/";
    url=[url stringByAppendingString:self.handleStr];
    
    [controller addURL:[NSURL URLWithString:url]];
    //Adding the Text to the facebook post value from iOS
    NSLog(@"%lu",(unsigned long)self.slideImageArray.count);
    if(!self.slideImageArray || self.slideImageArray.count!=0 ){
        NSString * imageStr=[[self.slideImageArray objectAtIndex:0]valueForKey:@"src"];
        UIImage * image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageStr]]];
        
        
        [controller addImage:image];
 
    }
    else
    {
       
    }
    
    
    [self presentViewController:controller animated:YES completion:nil];
        
        });

}
                   


- (IBAction)twitterPressed:(id)sender
{
    
    [self showLoadingView];
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
    SLComposeViewControllerCompletionHandler myBlock =
    ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled)
        {
            NSLog(@"Cancelled");
            [self hideLoadingView];
        }
        else
        {
            NSLog(@"Done");
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Successfully posted!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
        }
        [tweetSheet dismissViewControllerAnimated:YES completion:nil];
    };
 tweetSheet.completionHandler =myBlock;
   [tweetSheet setInitialText:@"CHECKOUT"];
   // [tweetSheet setTitle:@"CHECKOUT"];
    NSString * url=@"http://www.buddhapants.com/products/";
    url=[url stringByAppendingString:self.handleStr];

    [tweetSheet addURL:[NSURL URLWithString:url]];
//    NSString * imageStr=[[self.slideImageArray objectAtIndex:0]valueForKey:@"src"];
//    UIImage * image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageStr]]];
//    [tweetSheet addImage:image];
    //[self presentModalViewController:tweetSheet animated:YES];
    [self presentViewController:tweetSheet animated:YES completion:nil];
}

- (IBAction)emailPressed:(id)sender
{
    
    MFMailComposeViewController*mailComposer;
    if ([MFMailComposeViewController canSendMail])
    {
        
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        NSString * stringWith=[NSString stringWithFormat:@"Check out %@ on Buddha Pants :",self.lbl_productName.text];
        
        [mailComposer setSubject:stringWith];
        NSString * url=@"http://www.buddhapants.com/products/";
        url=[url stringByAppendingString:self.handleStr];
        
        //[mailComposer setMessageBody:[NSString stringWithFormat:@"Check out %@ on Buddha Pants :",self.lbl_productName.text] isHTML:YES];
        
 
        [mailComposer setMessageBody:url isHTML:YES];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }
    else
    {
        NSLog(@"This device cannot send email");
    }

}

- (IBAction)pinterestPressed:(id)sender
{
    
}



- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"%u",result);
    
    if (error)
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Oops !" message:@"Successfully posted!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    if (result)
    {
        NSLog(@"%u",result);
    }
        [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

@end
