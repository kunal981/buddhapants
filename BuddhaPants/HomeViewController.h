//
//  HomeViewController.h
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>
{
      AppDelegate *ad;
    UIView *backGroundView;
    UIView *view1,*arwView;
    float subTotal;
    UITableView *tableView_Cart;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    UIImageView *slide_imageView ;
    UILabel* lblProduct,*lblPrice;
    UILabel *lblSize;
    UILabel * lblQty,*lblPrice1;
    
    NSMutableArray *src_Array;
    NSMutableArray *image_array;
    NSMutableArray *varrient_array;
    NSMutableArray *bodyHtml_Array;
    NSMutableArray *handleArray;
    UILabel* lblBadage;
     BOOL addtoCart;
    
    
    
    UICollectionView *collectionView;
    
}

@property (weak, nonatomic) IBOutlet UILabel *collection_label;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonHeight;
@property (strong, nonatomic) IBOutlet UITextView *textVIew_discripation;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *verticalLayoutConstraint;
@property (strong, nonatomic) IBOutlet UIImageView *collectionImgView;
@property (strong, nonatomic) IBOutlet UITextView *textDescriptionLower;

@end
