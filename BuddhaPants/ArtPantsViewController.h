//
//  ArtPantsViewController.h
//  BuddhaPants
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteViewController.h"
#import "UploadViewController.h"

@interface ArtPantsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    
    
    UIButton * voteButton,*uploadButton;
    UIColor * selectedBtnColor,*notselectedBtnColor;
    
    BOOL buttonSelected;
    
    UILabel* lblPrice1;
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
    NSManagedObjectContext *managedObjectContext;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    
      BOOL addtoCart;

}

@end
