//
//  HomeViewController.m
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "HomeViewController.h"
#import "UINavigationItem+TitleView.h"
#import <QuartzCore/QuartzCore.h>

@interface HomeViewController ()
{
    Api_Wrapper *obj;
    NSManagedObjectContext *managedObjectContext;
    NSMutableArray *product_Array;
    NSMutableArray *title_Array;
    NSMutableArray *title_Array2;
}

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    _scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
    
    //_collectionView.delegate = self;
    
    CGSize size =  CGSizeMake(320, 1200.0);
    [self.scrollView setContentSize:size];
    
    NSLog(@"scroll view frame = %f",self.scrollView.frame.size.height);
    
   // self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    

//    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
//    
//   
//    [self.collectionView setCollectionViewLayout:flowLayout];
    
    
//    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
//    [_collectionView setBackgroundColor:[UIColor redColor]];
//    
   // [self.view addSubview:_collectionView];
    

//    self.collectionView.frame = CGRectMake(0, 560, self.view.frame.size.width, self.collectionView.frame.size.height + 300);
    
    
    
   self.collectionView.backgroundColor =  [UIColor blackColor];
  //  self.scrollView.backgroundColor = [UIColor redColor];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleWidth;
    
    product_Array = [[NSMutableArray alloc]init];
      title_Array = [[NSMutableArray alloc]init];
    src_Array = [[NSMutableArray alloc]init];
    handleArray=[[NSMutableArray alloc]init];
    
    varrient_array = [[NSMutableArray alloc]init];
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    managedObjectContext = [ad managedObjectContext];

    [self homeApi];
    
    self.collectionImgView.image=[UIImage imageNamed:@"collection"];
    self.collectionImgView.contentMode=UIViewContentModeScaleAspectFit;
    self.navigationItem.hidesBackButton = YES;
    
  //  self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"3.png"]];
    
//    SWRevealViewController *revealController = [self revealViewController];
//    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigation_icon.png"]
//                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
//    
//    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
   
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
   
    if ([UIScreen mainScreen].bounds.size.height==568)
    {
        
        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
        
        
        // self.collection_label.frame = CGRectMake(10, self.collection_label.frame.origin.y, self.collection_label.frame.size.width, self.collection_label.frame.size.height);
        
//        CGSize size =  CGSizeMake(320, 1200.0);
//        [self.scrollView setContentSize:size];
    }
    else if ([UIScreen mainScreen].bounds.size.height==480)
    {
        
        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
    
    else if ([UIScreen mainScreen].bounds.size.height==667)
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell~6" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
    
    else
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell~6+" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }

    NSLog(@"frame is %lu",141*(product_Array.count/2) );
    collectionView.frame = CGRectMake(0, collectionView.frame.origin.y, collectionView.frame.size.width, 141*(product_Array.count/2));
    
    // Do any additional setup after loading the view from its nib.
}

//-(void)cartbuttonPressed{
//    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     addtoCart = true;
    self.navigationController.navigationItem.hidesBackButton =  YES;
    
    
    //collectionView.frame = CGRectMake(60, 547, 200, 21);
    
    
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    if (fetchedObjects.count==0)
    {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }
    

    
    
    
//    if ([UIScreen mainScreen].bounds.size.height==568)
//    {
//        
//        
//        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
//        
//        //self.collection_label.frame = CGRectMake(10, self.collection_label.frame.origin.y, self.collection_label.frame.size.width, self.collection_label.frame.size.height);
//    }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Left bar button

-(void)barbuttonPressed
{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}



#pragma mark Api 
-(void)homeApi
{
    
    [self storeApi];
    
      self.lblHeader.font=[UIFont fontWithName:@"AmericanTypewriter" size:16];
    NSString * stringT=[NSString stringWithFormat:@"%@",@"Buddha Pants is a clothing company dedicated to creating multifunctional travel ready apparel, focusing on comfort & encouraging an active lifestyle. We make the only yoga pant that packs into its pocket! The yoga harem pant that packs into it pocket was inspired by the Eno Hammock, designed and manufactured by Buddha Pants.\n\nI wanted to create a harem pant with pockets! Harem pants with pockets are not common, yet so functional, when I had the idea to combine the two it made so much sense. Something practical and comfy. Go figure. I hope you enjoy the pants just as much as I do.\n    - Rachel Raab (Founder + Creative Director)"];
    
    self.textVIew_discripation.text=[NSString stringWithFormat:@"%@",stringT];
    self.textVIew_discripation.font=[UIFont fontWithName:@"Arial-ItalicMT" size:13];

    
//    [self showLoadingView];
//    
//    obj = [Api_Wrapper shared];
//        
//    
//    [obj GET:nil string:page_String completion:^(NSDictionary *json, BOOL success) {
//        //NSLog(@"%@",json);
//        
//        NSArray *pages = [json valueForKey:@"page"];
//        
//        NSString *String = [pages  valueForKey:@"body_html"];
//        
//        [self htmlParser:String];
//       
//        [self hideLoadingView];
//        [self storeApi];
//        
//   }];
}

-(void)htmlParser:(NSString*)html
{
    NSError *error = nil;
    
    HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&error];
    
    if (error)
    {
        NSLog(@"Error: %@", error);
        return;
    }

    HTMLNode *bodyNode = [parser body];
    
    NSArray *inputNodes = [bodyNode findChildTags:@"h1"];
    
    for (HTMLNode *inputNode in inputNodes) {
        NSArray *spanNodes = [inputNode findChildTags:@"span"];
        if (spanNodes.count==1) {
            
        }
        for (HTMLNode *spanNode in spanNodes) {
           
            NSLog(@"%@", [spanNode contents] ); //Answer to first question
            NSString *strHeader = [spanNode contents];
            NSLog(@"%@",strHeader);
            
            //self.lblHeader.text =strHeader;
            
           // NSLog(@"%@",self.lblHeader.text);
            self.lblHeader.font=[UIFont fontWithName:@"AmericanTypewriter" size:16];

            
            
            
        }
    }
    
    NSArray *inputNodes2 = [bodyNode findChildTags:@"p"];

    NSMutableString *strDescripation = [[NSMutableString alloc]init];
    
    for (HTMLNode *inputNode in inputNodes2) {
        NSArray *spanNodes = [inputNode findChildTags:@"span"];
        for (HTMLNode *spanNode in spanNodes) {
            NSArray *emNodes = [spanNode findChildTags:@"em"];
            NSLog(@"%@",emNodes);
            
            if(emNodes.count==0)
            {
                NSString*   str1 = [spanNode contents];
                
                              [strDescripation appendString:str1];
                
                NSLog(@"%@",str1);
                
            }  else {
               for (HTMLNode *emNode in emNodes) {
                    NSString*   str2 = [emNode contents];
                 
                   NSLog(@"%@",str2);

                   [strDescripation appendString:[NSString stringWithFormat:@"\n\n%@",str2]];
                   
                   
                }
            }
          
            //NSLog(@"%@",strDescripation);
            
          
            //Answer to first question
        }
    }
    
    NSString * stringT=[NSString stringWithFormat:@"%@",@"Buddha Pants is a clothing company dedicated to creating multifunctional travel ready apparel, focusing on comfort & encouraging an active lifestyle. We make the only yoga pant that packs into its pocket! The yoga harem pant that packs into it pocket was inspired by the Eno Hammock, designed and manufactured by Buddha Pants.\n\nI wanted to create a harem pant with pockets! Harem pants with pockets are not common, yet so functional, when I had the idea to combine the two it made so much sense. Something practical and comfy. Go figure. I hope you enjoy the pants just as much as I do.\n    - Rachel Raab (Founder + Creative Director)"];
    
    self.textVIew_discripation.text=[NSString stringWithFormat:@"%@",stringT];
    self.textVIew_discripation.font=[UIFont fontWithName:@"Arial-ItalicMT" size:13];
    
    
//
    
}

-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


#pragma Add to Cart

-(void)cartbuttonPressed
{
    
    if (addtoCart)
    {
        
    
    
    if(fetchedObjects.count==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
      subTotal= 0;
    
      [self coreDataFetch];
   
    
      NSLog(@"%lu",(unsigned long)fetchedObjects.count);
    
    
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
    arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
    [self.view addSubview:arwView];
    
    view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    
    //    view1.layer.borderWidth = 2.0;
    //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
    
    
    
    UIView * backView = [[UIView alloc]init];
    
    backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView];
    
    
    lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
        lblPrice1.textAlignment=NSTextAlignmentRight;
        lblPrice1.adjustsFontSizeToFitWidth=YES;
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
    
    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
        lblSubTotal.textAlignment=NSTextAlignmentRight;
       
    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [view1 addSubview:tableView_Cart];
        
        
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
    }
        addtoCart = false;
    }
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  fetchedObjects.count;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
       
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
    return cell;
}

-(void)subTotal
{
    for (int i  =0 ; i<fetchedObjects.count; i++)
    {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}





-(void)subttotalDelete:(float)result
{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
       
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}



- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}




-(void)coreDataFetch
{
    
    NSError *error;
    
    managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (int i =0; i<fetchedObjects.count; i++)
    {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        
        float result= [obj1.totalprice floatValue];
        
        subTotal =  subTotal + result;
        
    }
    
    
    
    
    
}

-(void)storeApi
{
    obj = [Api_Wrapper
           shared];
    
   [self showLoadingView];
    
    [obj GETStore:nil string:product_String_Home completion:^(NSDictionary *json, BOOL success) {
       
        
        product_Array = [[json valueForKey:@"products"]valueForKey:@"image"];
        
        NSLog(@"product array =%@",product_Array);
        title_Array  =[[json valueForKey:@"products"]valueForKey:@"title"];
        
      
//        title_Array2 = [[NSMutableArray alloc] initWithObjects:
//                                                @{@"title" : @"Beige Aztec"},
//                                                @{@"title" : @"Beige Tribal"},
//                                                @{@"title" : @"Black Aztec"},
//                                                @{@"title" : @"Black Savannah Flair"},
//                                                @{@"title" : @"Black Sunshine"},
//                                                @{@"title" : @"Blue Stripes"},
//                                                @{@"title" : @"Blue Sunshine"},
//                                                @{@"title" : @"Blue Zags"},
//                                                @{@"title" : @"Blue Zebra"},
//                                                @{@"title" : @"Blue Salt Flats"},
//                                                @{@"title" : @"Congo"},
//                                                @{@"title" : @"Digital Rainbow"},
//                                                @{@"title" : @"Full Moon"},
//                                                @{@"title" : @"Green Zags"},
//                                                @{@"title" : @"Green Zebra"},
//                                                @{@"title" : @"Grey Zags"},
//                                                @{@"title" : @"Kali Pink"},
//                                                @{@"title" : @"Kali Blue"},
//                                                @{@"title" : @"Kali Red"},nil];

        
        
        
        
        
        title_Array2 = [[NSMutableArray alloc] initWithObjects:
                        @{@"product_id" : @"2625251715"},
                        @{@"product_id" : @"306340031"},nil];
//                        @{@"product_id" : @"Black Aztec"},
//                        @{@"product_id" : @"Black Savannah Flair"},
//                        @{@"product_id" : @"Black Sunshine"},
//                        @{@"product_id" : @"Blue Stripes"},
//                        @{@"product_id" : @"Blue Sunshine"},
//                        @{@"product_id" : @"Blue Zags"},
//                        @{@"product_id" : @"Blue Zebra"},
//                        @{@"product_id" : @"Blue Salt Flats"},
//                        @{@"product_id" : @"Congo"},
//                        @{@"product_id" : @"Digital Rainbow"},
//                        @{@"product_id" : @"Full Moon"},
//                        @{@"product_id" : @"Green Zags"},
//                        @{@"product_id" : @"Green Zebra"},
//                        @{@"product_id" : @"Grey Zags"},
//                        @{@"product_id" : @"Kali Pink"},
//                        @{@"product_id" : @"Kali Blue"},
//                        @{@"product_id" : @"Kali Red"},nil];

        
        
        
        
        
        
        title_Array2 = [NSMutableArray new];
        [title_Array2 addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"product_id",@"2625251715",
                                 @"product_id",@"306340031",nil]];
//                                 @"product_id",@"Black Aztec",
//                                 @"product_id",@"Black Savannah Flair",
//                                 @"product_id",@"Black Sunshine",
//                                 @"product_id",@"Blue Zags",
//                                 @"product_id",@"Blue Zebra",
//                                 @"product_id",@"Blue Salt Flats",
//                                 @"product_id",@"Congo",
//                                 @"product_id",@"Digital Rainbow",
//                                 @"product_id",@"Full Moon",
//                                @"product_id",@"Green Zags",
//                                 @"product_id",@"Green Zebra",
//                                 @"product_id",@"Grey Zags",
//                                 @"product_id",@"Kali Pink",
//                                @"product_id",@"Kali Blue",
//                                 @"product_id",@"Kali Red",nil]];
        
//        NSLog(@"title array 2 =%@",title_Array2);
//        
//        NSMutableArray *DummyArray = [NSMutableArray new];
//        
//        for (int i = 0; i < [title_Array2 count]; i++)
//        {
//            
//            // NSDictionary *dict=[title_Array2 objectAtIndex:i];
//            
//            for (int j = 0; j < [product_Array count]; j++)
//            {
//                if ([[[title_Array2 objectAtIndex:i] valueForKey:@"product_id"]  isEqualToString:[product_Array objectAtIndex:j]])
//                {
//                    [DummyArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:[product_Array objectAtIndex:j], nil]];
//                    
//                }
//            }
//        }
//        NSLog(@"DummyArray = %@",DummyArray);
        
    
        
        
        src_Array = [[json valueForKey:@"products"]valueForKey:@"images"];
        
        
        
        NSLog(@"title array = %@",title_Array);
        
        varrient_array = [[json valueForKey:@"products"]valueForKey:@"variants"];
        bodyHtml_Array = [[json valueForKey:@"products"]valueForKey:@"body_html"];
        handleArray=[[json valueForKey:@"products"]valueForKey:@"handle"];
        
//        [self.collectionView reloadData];
        [self LoadImageScroller];
        [self hideLoadingView];

    }];
    
    
}



-(void)LoadImageScroller
{
//    for(UIView *subview in _scrollView.subviews)
//    {
//        [subview removeFromSuperview];
//    }
    
    float width = self.view.frame.size.width/2;
    float height = self.view.frame.size.width/2;
    int x = 0;
    int y = 578;
    
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, 578+((height+25)*[product_Array count]/2))];
    
    for(int i = 1; i<=[product_Array count]; i++)
    {
        UIImageView *image = [UIImageView new];
        image.frame = CGRectMake(x, y, width, height);
        
        
        [image sd_setImageWithURL:[NSURL URLWithString:[[product_Array objectAtIndex:i-1]valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
        
        image.contentMode = UIViewContentModeScaleAspectFit;
        image.backgroundColor = [UIColor whiteColor];
        image.layer.borderWidth=1.0;
        image.layer.borderColor=[[UIColor lightGrayColor] CGColor];
        [_scrollView addSubview:image];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(x, y, width, height+25);
        
        // NSLog(@"button frame = %d",y);
        
        button.tag = (i-1) + 999;
        [_scrollView addSubview:button];
        
        
        
        
        UILabel *lbl = [UILabel new];
        lbl.frame = CGRectMake(x, y+height, width, 25);
        
        
        //NSLog(@"label frame = %f",y+height);
        
        //lbl.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
        lbl.backgroundColor = [UIColor lightGrayColor];
        

        lbl.text = [title_Array objectAtIndex:i-1];
        lbl.font = [UIFont boldSystemFontOfSize:14.0f];
        lbl.textAlignment = NSTextAlignmentCenter;
        //lbl.textColor = [UIColor darkGrayColor];
        
        lbl.textColor = [UIColor colorWithRed:139.0/255.0
                        green:19.0/255.0
                         blue:26.0/255.0
                        alpha:1.0];
        
        [_scrollView addSubview:lbl];
        
        if(i % 2 == 0)
        {
            x = 0;
            y = y + height + 25;
        }
        else
        {
            x = x + width;
        }
    }
}




-(IBAction)myAction:(id)sender
{
    int index = (int)[sender tag]-999;
    
    image_array = [[NSMutableArray alloc]init];
    
     ProductViewController *productVC =  [[ProductViewController alloc]init];
    
    NSLog(@"button clicked");
    
    for(int i = 0; i<=index; i++)
    {
        NSString *htmlString = [bodyHtml_Array objectAtIndex:i];
        
        NSMutableArray *var_array = [[NSMutableArray alloc]init];
        
        
        image_array = [src_Array objectAtIndex:i];
        var_array = [varrient_array objectAtIndex:i];
        
        productVC.slideImageArray = image_array;
        productVC.str_lblName = [title_Array objectAtIndex:i];
        productVC.varientArray = var_array;
        productVC.strHtml = htmlString;
        productVC.handleStr=[handleArray objectAtIndex:i];
        
    }
    
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:productVC animated:YES];
}


#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
//    if(product_Array.count!=0)
//        return 20;
//    else
//        return 0;
    
    return product_Array.count;

}





//- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
//{
//    if(product_Array.count!=0)
//        return 20;
//    else
//        return 0;
//    
//   // return product_Array.count;
//}






// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (MYCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    MYCell *cell = [collectionView1 dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    
    NSString *src = [[product_Array objectAtIndex:indexPath.row]valueForKey:@"src"];
    
    if (src == nil|| src==(id)[NSNull null])
    {
        NSLog(@"null image");
        cell.imgView_product.image = [UIImage imageNamed:@"ic_no_image.jpg"];
    }
    else
    {
        
        // NSLog(@"src=%@",idOfproductsArray);
        
        cell.imgView_product.contentMode=UIViewContentModeScaleAspectFit;
        
        
        [cell.imgView_product sd_setImageWithURL:[NSURL URLWithString:[[product_Array objectAtIndex:indexPath.row]valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
        
        
    }
    
    
    cell.lbl_Tittle.text =[title_Array objectAtIndex:indexPath.row];
    
    
    
    
   
//    customCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//    
//    if (cell == nil)
//    {
//        NSLog(@"null image");
//        cell.imgView_product.image = [UIImage imageNamed:@"ic_no_image.jpg"];
//    }
//    cell.albumSong.text = [strOfSongArray[indexPath.row] valueForKey:@"m_title"];
//    
//    strNavigationTitle = Cell.albumSong.text;
//    
//    songImg = [strOfSongArray[indexPath.row] valueForKey:@"m_file_image"];
//    
//    NSURL *url = [NSURL URLWithString:songImg];
//    
//    NSLog(@"url is %@",url);
    
    
    
    
      // displayImage = YES;
    
    return cell;

}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    MYCell *cell = (MYCell*)[collectionView cellForItemAtIndexPath:indexPath];
    //    NSArray *views = [cell.contentView subviews];
    //    UILabel *label = [views objectAtIndex:0];
    //    NSLog(@"Select %@",label.text);
    
    image_array = [[NSMutableArray alloc]init];
    
    NSString *htmlString = [bodyHtml_Array objectAtIndex:indexPath.row];
    
    
    
    NSMutableArray *var_array = [[NSMutableArray alloc]init];
    
    ProductViewController *productVC =  [[ProductViewController alloc]init];
    
    image_array = [src_Array objectAtIndex:indexPath.row];
    var_array = [varrient_array objectAtIndex:indexPath.row];
    
    productVC.slideImageArray = image_array;
    productVC.str_lblName = [title_Array objectAtIndex:indexPath.row];
    productVC.varientArray = var_array;
    productVC.strHtml = htmlString;
     productVC.handleStr=[handleArray objectAtIndex:indexPath.row];
    
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:productVC animated:YES];
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIScreen mainScreen].bounds.size.height==568)
    {
        //return CGSizeMake(156, 141);
        
        return CGSizeMake(self.view.frame.size.width/2, 141);
        
        //return CGSizeMake(CGRectGetWidth(self.collectionView.frame) * 0.5f, 130);
    
    }
    else if ([UIScreen mainScreen].bounds.size.height==667)
    {
        return CGSizeMake(self.view.frame.size.width/2, 182);
    }
    else if ([UIScreen mainScreen].bounds.size.height==480)
    {
       return CGSizeMake(self.view.frame.size.width/2, 160);
    }
    else
        return CGSizeMake(self.view.frame.size.width/2, 184);
}


//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([UIScreen mainScreen].bounds.size.height==568) {
//        return CGSizeMake(self.view.frame.size.width/2, 160);
//    }
//    else if ([UIScreen mainScreen].bounds.size.height==667){
//        return CGSizeMake(self.view.frame.size.width/2, 190);
//    }else
//        return CGSizeMake(self.view.frame.size.width/2, 200);
//}






//- (CGSize)collectionViewContentSize
//{
//    NSInteger rowCount = [self.collectionView numberOfSections] / self.numberOfColumns;
//    // make sure we count another row if one is only partially filled
//    if ([self.collectionView numberOfSections] % self.numberOfColumns) rowCount++;
//    
//    CGFloat height = self.itemInsets.top +
//    rowCount * self.itemSize.height + (rowCount - 1) * self.interItemSpacingY +
//    self.itemInsets.bottom;
//    
//    return CGSizeMake(self.collectionView.bounds.size.width, height);
//}



-(void)viewDidLayoutSubviews
{
    
    NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
    
    [super viewDidLayoutSubviews];
    
    self.verticalLayoutConstraint.constant =800;

   if ([UIScreen mainScreen].bounds.size.height==568)
   {
       
       self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, 141*(product_Array.count/2));
       
       //self.scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 80 + (141*(product_Array.count/2)));
       
       //[cell.imgView_product sd_setImageWithURL:[NSURL URLWithString:[[product_Array objectAtIndex:indexPath.row]valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
       
       
       NSLog(@"Scroll view frame = %@",NSStringFromCGSize(self.scrollView.contentSize));
       
       NSLog(@"Collection view frame = %f",self.collectionView.frame.size.height);
       
    }
   else if ([UIScreen mainScreen].bounds.size.height==480)
   {
       
       self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, 141*(product_Array.count/2));
       
       //self.scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+ 80 + (141*(product_Array.count/2)));
   }
    else if ([UIScreen mainScreen].bounds.size.height==667)
    {
        
        self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, 141*(product_Array.count/2));

         //self.scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+ 80 + (141*(product_Array.count/2)));
   }
    else
    {
        self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, 141*(product_Array.count/2));
        
        //self.scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+ 80 + (141*(product_Array.count/2)));
    }
}

@end
