//
//  UploadViewController.h
//  BuddhaPants
//
//  Created by brst on 5/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
        NSMutableDictionary *dictData;
    UIButton * voteButton,*uploadButton;
    UIColor * selectedBtnColor,*notselectedBtnColor;
    
    UITextField * txtField,*txtFieldDesc;
    
    NSURLConnection* connection;
    
    UILabel* lblPrice1;
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
    NSManagedObjectContext *managedObjectContext;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    
    UIImagePickerController * pickerimage;
    UIImage * uploadImage;
    UIImageView * uploadImageView;
    NSString *base64String;
    UIScrollView *scrollView;
    BOOL addtoCart;
}

@end
