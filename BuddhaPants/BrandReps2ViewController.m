//
//  BrandReps2ViewController.m
//  BuddhaPants
//
//  Created by brst on 23/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "BrandReps2ViewController.h"

@interface BrandReps2ViewController ()
{
    Api_Wrapper *objApi;
    
    NSString *name;
    NSString *company;
    NSString *adress;
    NSString *email;
    NSString *instagram;
    NSString *message;
    
    UITextField * txtField;
}

@end

@implementation BrandReps2ViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0)
    {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dictData=[[NSMutableDictionary alloc]init];
    
    tittleArr=[[NSMutableArray alloc]initWithObjects:@"Name",@"Company", @"Mailing Address",@"Email",@"Instagram Account",@"Message",nil];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
   
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
   
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;

    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:scrollView];
    
    UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(0, 5, scrollView.frame.size.width, 30)];
    lblHeader.text=@"Become a Buddha Ambassador!";
    lblHeader.font=[UIFont boldSystemFontOfSize:20.0];
    lblHeader.textAlignment=NSTextAlignmentCenter;
    lblHeader.backgroundColor=[UIColor whiteColor];
    lblHeader.textColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    [scrollView addSubview:lblHeader];
    
    
    UIImageView * imgView;
             imgView=[[UIImageView alloc]initWithFrame:CGRectMake((scrollView.frame.size.width-100)/2,lblHeader.frame.size.height+lblHeader.frame.origin.y+5, 100, 130)];
        imgView.backgroundColor=[UIColor lightGrayColor];
        imgView.image=[UIImage imageNamed:@"getInvolved.png"];
    [scrollView addSubview:imgView];
    
        
  
    
    UILabel * lblHeader1=[[UILabel alloc]initWithFrame:CGRectMake(10, imgView.frame.size.height+imgView.frame.origin.y+10,scrollView.frame.size.width-20 ,15)];
    
    lblHeader1.text=@"Interested in becoming a brand Ambassador? ";
    
    lblHeader1.font=[UIFont systemFontOfSize:13.0];
    lblHeader1.textAlignment=NSTextAlignmentCenter;
    lblHeader1.backgroundColor=[UIColor whiteColor];
    lblHeader1.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
    [scrollView addSubview:lblHeader1];
    
    
    UILabel * btn1=[[UILabel alloc]init];
    btn1.frame=CGRectMake(10,lblHeader1.frame.size.height+lblHeader1.frame.origin.y+10,scrollView.frame.size.width-20, 35);
  
     btn1.text=@"Email Ambassadors@BuddhaPants.com\nOr fill out the form below.";
    btn1.textColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
    btn1.numberOfLines=2;
    btn1.textAlignment=NSTextAlignmentCenter;
    
         btn1.font=[UIFont systemFontOfSize:13];
    [scrollView addSubview:btn1];
    
     
    
    
    CGFloat y=btn1.frame.size.height+btn1.frame.origin.y+20;
    for (int i=0; i<tittleArr.count; i++)
    {
        
        UILabel * lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(20, y, scrollView.frame.size.width-40, 20)];
        if (i==0 || i==3)
        {
            UILabel * lblStar=[[UILabel alloc]initWithFrame:CGRectMake(60, y, 50, 20)];
            lblStar.text = @"*";
            lblStar.textColor = [UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
            [scrollView addSubview:lblStar];
        }
        lblHeader.text= [tittleArr objectAtIndex:i];
        lblHeader.font=[UIFont systemFontOfSize:15.0];
        lblHeader.textAlignment=NSTextAlignmentLeft;
        lblHeader.textColor=[UIColor  colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0];
        [scrollView addSubview:lblHeader];
        int height;
        if (i==5)
        {
            height=80;
        }
        else
        {
            height=35;
        }
        
        txtField = [[UITextField alloc]initWithFrame:CGRectMake(20, y+20, scrollView.frame.size.width-40, height)];
        
        txtField.backgroundColor=[UIColor whiteColor];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        txtField.leftView = paddingView;
        txtField.leftViewMode = UITextFieldViewModeAlways;
        txtField.layer.borderColor=[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0].CGColor;
        txtField.layer.borderWidth=1.0;
        txtField.tag=i+20;
        txtField.delegate=self;
        txtField.placeholder=@" ";
        y+=55+25;
        [scrollView addSubview:txtField];
        
    }
    
    
    UIButton * btnSend=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSend.frame=CGRectMake(20, y+50,100, 30);
    btnSend.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    
    [btnSend setTitle:@"SEND" forState:UIControlStateNormal];
    [btnSend setTitleColor:[UIColor colorWithRed:0.5529 green:0.1373 blue:0.1490 alpha:1.0] forState:UIControlStateNormal];
    btnSend.backgroundColor=[UIColor colorWithRed:0.4118 green:0.8000 blue:0.7451 alpha:1.0];
    
    [btnSend addTarget:self action:@selector(btnPressedSent) forControlEvents:UIControlEventTouchUpInside];
    btnSend.titleLabel.font=[UIFont systemFontOfSize:15];
    [scrollView addSubview:btnSend];
    
    
    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, y+350);
    
}


-(void)btnPressedSent
{
    name = [(UITextField*)[scrollView viewWithTag:20]text];
    company = [(UITextField*)[scrollView viewWithTag:21]text];
    
    adress=[(UITextField*)[scrollView viewWithTag:22]text];
    
    email=[(UITextField*)[scrollView viewWithTag:23]text];
    instagram=[(UITextField*)[scrollView viewWithTag:24]text];
    message=[(UITextField*)[scrollView viewWithTag:25]text];
    
    if (name.length==0)
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    else if (email.length==0)
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter Email Address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }else
    {
        [dictData setValue:name forKey:@"name"];
        [dictData setValue:company forKey:@"company"];
        [dictData setValue:adress forKey:@"mail_address"];
        [dictData setValue:email forKey:@"email"];
        [dictData setValue:instagram forKey:@"insaccount"];
        [dictData setValue:message forKey:@"msg"];
        [dictData setValue:@"getinvolved" forKey:@"uniq_string"];
        
        [self apihit];
    }
    

}
-(void)apihit
{
    objApi = [Api_Wrapper shared];
    
   // [self showLoadingView];
    
    
    [objApi PostApi:dictData string:about_String
         completion:^(NSDictionary *json, BOOL sucess) {
             NSLog(@"%@",json);
             
             if ([[json valueForKey:@"status"] isEqualToString:@"true"])
             {
                 
                 UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:@"Thanks for getting in contact!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 
                 [alert show];
                 
                 
                 
                 
                 
                 
                 for(int i=0; i<tittleArr.count;i++)
                 {
                     txtField=(UITextField *)[scrollView viewWithTag:i+20];
                     [txtField setText:@""];
                 }
                 
                 
                 //[self hideLoadingView];
                 
             }
             
             
            // [self hideLoadingView];
             
             
         }];
    
    
    
    
    
    
    
}
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)btn1Pressed
{
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.buddhapantsmiami.com/pages/get-involved"]];
    
}
//-(void)cartbuttonPressed{
//    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//}

-(void)cartbuttonPressed{
    if (addtoCart) {
        
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else {
    [self coreDataFetch];
   
    
    
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
    arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
    [self.view addSubview:arwView];
    
    view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    
    //    view1.layer.borderWidth = 2.0;
    //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
    
    
    
    UIView * backView = [[UIView alloc]init];
    
    backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView];
    
    
    lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
        lblPrice1.textAlignment=NSTextAlignmentRight;
        lblPrice1.adjustsFontSizeToFitWidth=YES;

    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
    
    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
    lblSubTotal.textAlignment=NSTextAlignmentRight;
    lblSubTotal.adjustsFontSizeToFitWidth=YES;

    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [view1 addSubview:tableView_Cart];
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
    
    }
        addtoCart = false;
    }
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
        
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
       return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}


-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}

-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}


#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag<24)
    {
        CGRect frame=self.view.frame;
        frame.origin.y=-110;
        self.view.frame=frame;
    }else
    {
        CGRect frame=self.view.frame;
        frame.origin.y=-150;
        self.view.frame=frame;
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect frame=self.view.frame;
    frame.origin.y=64;
    self.view.frame=frame;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
