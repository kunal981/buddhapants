//
//  LoginViewController.m
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "LoginViewController.h"
#import "AFHTTPRequestOperationManager.h"

@interface LoginViewController (){
 Api_Wrapper *obj;
    NSString*uid;
}
@end

@implementation LoginViewController
@synthesize txtField_username,txtField_password,btn_login;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.checkMail_array = [[NSMutableArray alloc]init];
    
    txtField_username.layer.borderColor = [UIColor whiteColor].CGColor;
    txtField_username.layer.borderWidth = 0.30;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtField_username.leftView = paddingView;
    txtField_username.leftViewMode = UITextFieldViewModeAlways;
    
    txtField_password.layer.borderColor = [UIColor whiteColor].CGColor;
    txtField_password.layer.borderWidth = 0.30;
   // txtField_password.text=@"123";
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtField_password.leftView = paddingView1;
    txtField_password.leftViewMode = UITextFieldViewModeAlways;
    btn_login.layer.borderColor = [UIColor whiteColor].CGColor;
    btn_login.layer.borderWidth = 0.60;
    
   
    

    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillChange:)
//                                                 name:UIKeyboardDidShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardDidHide:)
//                                                 name:UIKeyboardDidHideNotification
//                                               object:nil];
    
   // self.view.backgroundColor  = [UIColor greenColor];
       // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    
    
  //self.txtField_username.text = @"deepak.thakur@brihaspatitech.com";
   // self.navigationController.navigationBar.hidden = YES;
    
//    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"email"] isEqualToString:self.txtField_username.text]) {
//        HomeViewController *homeVC = [[HomeViewController alloc]init];
//        [self.navigationController pushViewController:homeVC animated:YES];
//    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)keyboardWillChange:(NSNotification *)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    NSLog(@"%@",NSStringFromCGRect(keyboardFrameBeginRect));
    
}


#pragma mark - UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _userNameY.constant = 290;
   
   
       txtField_username.frame = CGRectMake(txtField_username.frame.origin.x, _userNameY.constant, txtField_username.frame.size.width, txtField_username.frame.size.height);
   
     //txtField_password.frame = CGRectMake(txtField_password.frame.origin.x, _userNameY.constant, txtField_password.frame.size.width, txtField_password.frame.size.height);
//    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    _userNameY.constant = 210;
    txtField_username.frame = CGRectMake(txtField_username.frame.origin.x, _userNameY.constant, txtField_username.frame.size.width, txtField_username.frame.size.height);
    
    

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark UIButtonAction

- (IBAction)btn_loginPressed:(id)sender {
    
    if(self.txtField_username.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtField_password.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
    [self loginApi];
    }
  
    
}

//-(BOOL)prefersStatusBarHidden{
//    return YES;
//}


- (IBAction)btnRegisterPressed:(id)sender {
    RegisterViewController *registerVC = [[RegisterViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (IBAction)forgetBtnPressed:(id)sender {
    
    ForgetPasswordViewController * forgetVC=[[ForgetPasswordViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:forgetVC animated:YES];
}
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
}


#pragma mark API
-(void)loginApi{
   // [self showLoadingView];
    
    obj = [Api_Wrapper shared];
    NSMutableDictionary *dict  = [[NSMutableDictionary alloc]init];
    [dict setObject:self.txtField_username.text forKey:@"query"];
    
    [obj GET:dict string:login_String completion:^(NSDictionary *json, BOOL success) {
        NSLog(@"%@",json);
        NSArray *result = [json valueForKey:@"customers"];
        if (result.count!=0) {
            for (int i =0 ; i<result.count; i++)
            {
                NSString *email = [[result objectAtIndex:i]valueForKey:@"email"];
                
                NSString*firstName= [[result objectAtIndex:i]valueForKey:@"first_name"];
                  NSString*lastname= [[result objectAtIndex:i]valueForKey:@"last_name"];
                 uid= [[result objectAtIndex:i]valueForKey:@"id"];
                
                  [[NSUserDefaults standardUserDefaults]setValue:email forKey:@"email"];
                [[NSUserDefaults standardUserDefaults]setValue:uid forKey:@"uid"];
                [[NSUserDefaults standardUserDefaults]setValue:firstName forKey:@"firstName"];

                [[NSUserDefaults standardUserDefaults]setValue:lastname forKey:@"lastName"];

                
                [self.checkMail_array addObject:email];
            }
            NSLog(@"%@",self.checkMail_array);
            
            if ([self.checkMail_array containsObject:[NSString stringWithFormat:@"%@",self.txtField_username.text]]) {
                
                
                [self loginNotificationApi];
               
            }
            else{
              
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"invalid  credential" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"invalid  credential" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
        }
        
        
        
        
        
               
        //[self hideLoadingView];
        
    }];
    
    
}

-(void)loginNotificationApi{
    //[self showLoadingView];
    NSLog(@"sdsda");
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"usertoken"];
    
    NSMutableDictionary *dict  = [[NSMutableDictionary alloc]init];
   // [dict setObject:deviceToken forKey:@"DeviceToken"];//device
    
    [dict setObject:@"6565656" forKey:@"DeviceToken"];// simulator
    [dict setObject:uid forKey:@"shopifyid"];
    [dict setObject:@"iphone" forKey:@"deviceName"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
   
    
    [manager POST:@"http://mfvs.cc/buddha/PushNotification.php" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        NSString *success = [responseObject valueForKey:@"status"];
        if ([success isEqualToString:@"true"]) {
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"ReLogin"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            HomeViewController *homeVC = [[HomeViewController alloc]init];
            [self.navigationController pushViewController:homeVC animated:YES];
        }
        else{
            [self performSelectorOnMainThread:@selector(errorAlert) withObject:nil waitUntilDone:NO];
        }
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];

}
-(void)errorAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}
@end
