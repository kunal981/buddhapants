//
//  EventsViewController.m
//  BuddhaPants
//
//  Created by brst on 03/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "EventsViewController.h"

@interface EventsViewController ()
{
    Api_Wrapper *obj;
}

@end

@implementation EventsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self eventsApi];
    
    image_array = [[NSMutableArray alloc]init];
     self.lbl_header.text = @"EVENTS";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];

    self.webView.delegate = self;
    
    //self.webView.scrollView.delegate = self;
    //self.webView.scrollView.showsHorizontalScrollIndicator = NO;
    
    
   [[self.webView scrollView] setContentSize: CGSizeMake(self.webView.frame.size.width, self.webView.frame.size.height)];
    
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
   
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;

    
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    // Do any additional setup after loading the view from its nib.
}

//-(void)cartbuttonPressed{
//    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//}






- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    NSLog(@"uiwebview width = %f",self.webView.frame.size.width);
    
    CGSize contentSize = self.webView.scrollView.contentSize;
    
    NSLog(@"content size of uiwebview = %f",self.webView.scrollView.contentSize);
    
    CGSize viewSize = self.webView.bounds.size;
    
    NSLog(@"view Size of uiwebview = %f",self.webView.bounds.size);
    
      float rw = viewSize.width / contentSize.width;
    
    NSLog(@"rw is = %f",rw);
    
    NSLog(@"view size width is = %f", viewSize.width);
    NSLog(@"content width is = %f",contentSize.width);
    
      self.webView.scrollView.minimumZoomScale = rw;
      self.webView.scrollView.maximumZoomScale = rw;
      self.webView.scrollView.zoomScale = rw;
   
    
    
    [[self.webView scrollView] setContentSize: CGSizeMake(viewSize.width, self.webView.frame.size.height)];
    
    
    self.webView.scrollView.showsHorizontalScrollIndicator = NO;
    
//    self.webView.scrollView.alwaysBounceHorizontal = NO;
//    self.webView.scrollView.directionalLockEnabled = YES;
 
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    
}

#pragma mark Left bar button

-(void)barbuttonPressed
{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0)
    {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
    }
    

}

-(void)cartbuttonPressed
{
    
    if (addtoCart)
    {
        
    
    if(fetchedObjects.count==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
    subTotal= 0;
    [self coreDataFetch];
       
    
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
    arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
    [self.view addSubview:arwView];
    
    view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    
    //    view1.layer.borderWidth = 2.0;
    //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
    
    
    
    UIView * backView = [[UIView alloc]init];
    
    backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView];
    
    
    lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.textAlignment=NSTextAlignmentRight;
    lblPrice1.adjustsFontSizeToFitWidth=YES;
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
    
    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
    lblSubTotal.textAlignment=NSTextAlignmentRight;
    
    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [view1 addSubview:tableView_Cart];
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
        
    }
        addtoCart  = false;
    }
    
    
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
        
        profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
    
    return cell;
}

-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
      
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}


-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++)
    {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




-(void)eventsApi
{
    obj = [Api_Wrapper shared];
    
    
    [obj GET:nil string:event_String completion:^(NSDictionary *json, BOOL success) {
        NSLog(@"Reatailer = %@",json);
        
        
        events_Array = [json valueForKey:@"page"];
        
       
        NSString *body = [events_Array  valueForKey:@"body_html"];
        NSLog(@"%@",body);
       // NSString *tittle = [events_Array  valueForKey:@"title"];
      //  self.lbl_header.text = tittle;
       
        self.webView.clipsToBounds = YES;
        NSString *htmlString = [NSString stringWithFormat:
                            @"<html><head>"
                            "<style type=\"text/css\">"
                                "a {color: #811518;}"
                            "body{"
                            "font-family: Helvetica;"
                            "font-size: 17px;"
                            "},</style>"
                            "</head><body bgcolor=\"#FFFFFF\" text=\"#811518\"face=\"Bookman Old Style, Book Antiqua, Garamond\" size=\"5\">%@</body></html>", body];
        
       // NSString *htmlString = [NSString stringWithFormat:@"<html><body bgcolor=\"#FFFFFF\" text=\"#811518\" face=\"Bookman Old Style, Book Antiqua, Garamond\" size=\"5\">%@</body></html>", body];
        
       // [NSString stringWithFormat:@"<html><body bgcolor=\"#000000\" text=\"#FFFFFF\" face=\"Bookman Old Style, Book Antiqua, Garamond\" size=\"5\">%@</body></html>", [item objectForKey:@"description"]] baseURL: nil];
        
        
        [self.webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://www.buddhapants.com/pages/events"]];
        self.webView.tintColor = [UIColor redColor];
        //[self.webView stringByEvaluatingJavaScriptFromString:htmlString];
        
        
        
    }];
}

-(void)htmlParser:(NSString*)html
{
    NSError *error = nil;
    
    HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&error];
    
    if (error) {
        NSLog(@"Error: %@", error);
        return;
    }
    
    HTMLNode *bodyNode = [parser body];
    
    NSArray *inputNodes = [bodyNode findChildTags:@"img"];
    
    NSLog(@"%@",inputNodes);
    
    for (HTMLNode *inputNode in inputNodes) {
        
        
           NSString  *str =  [inputNode getAttributeNamed:@"src"];
        str = [str stringByReplacingOccurrencesOfString:@"//" withString:@""];
        
       
        [image_array addObject:str];
        //Answer to first question
        
    }
    
    NSArray *hrfNode = [bodyNode findChildTags:@"a"];
    
    NSLog(@"%@",hrfNode);
    
    for (HTMLNode *inputNode in hrfNode) {
        
        
        NSString  *str =  [inputNode getAttributeNamed:@"href"];
        NSLog(@"%@",str);
        
        
}
    
    NSArray *addressNode = [bodyNode findChildTags:@"i"];
    
    NSLog(@"%@",addressNode);
    
    for (HTMLNode *inputNode in addressNode) {
        
        
        
      NSString*   str2 = [inputNode contents];
        
        NSLog(@"%@",str2);
        
        
        
        
        //Answer to first question
        
    }

    
    
    
    
    //self.textVIew_discripation.text=[NSString stringWithFormat:@"%@",strDescripation];
    
    
}

-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //[self showLoadingView];

}



//- (void) webViewDidFinishLoad: (UIWebView *) webView
//{
////        [webView stringByEvaluatingJavaScriptFromString: @"(function($){for(var i=0;i<$.length;++i){$[i].removeAttribute('target')}})(document.querySelectorAll('a[target=_blank]'))"];
//    }



-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Server Error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
@end
