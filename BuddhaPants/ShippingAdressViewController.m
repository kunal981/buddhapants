//
//  ShippingAdressViewController.m
//  BuddhaPants
//
//  Created by brst on 24/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ShippingAdressViewController.h"
#define kPayPalEnvironment PayPalEnvironmentNoNetwork

@interface ShippingAdressViewController ()
{
    NSManagedObjectContext *managedObjectContext;
     Api_Wrapper *objApi;

}


@property(nonatomic, strong, readwrite) IBOutlet UIButton *payNowButton;
@property(nonatomic, strong, readwrite) IBOutlet UIButton *payFutureButton;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;

@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

@end

@implementation ShippingAdressViewController
@synthesize scrollView;
@synthesize managedObjectContext = _managedObjectContext;


- (void)viewDidLoad
{
    [super viewDidLoad];
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    managedObjectContext = [ad managedObjectContext];

    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+70);
      self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    self.lineView.backgroundColor = [UIColor whiteColor];
    self.lineView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.lineView.layer.borderWidth = 1.0;
    self.lineView.layer.cornerRadius = 7.0;
    countryArr = [[NSMutableArray alloc]initWithObjects:@"UK",@"USA",@"Canada", nil];

    // Do any additional setup after loading the view.
}


#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    
        return countryArr.count;
}


- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return [countryArr objectAtIndex:row];
}

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSString *resultString = [[NSString alloc] initWithFormat:
                              @"%@",[countryArr objectAtIndex:row]];
    self.txtCountry.text = resultString;
    
   
    
    
   // [self.txtState becomeFirstResponder];
    
        [picker removeFromSuperview];
}





#pragma mark - PayPal

- (void)setPayPalEnvironment:(NSString *)environment
{
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}



- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Payment"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Payment"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    [self showSuccess];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self orderApi];
}



#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment
{
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}



#pragma mark - Authorize Future Payments

- (IBAction)getUserAuthorizationForFuturePayments:(id)sender
{
    
    PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self presentViewController:futurePaymentViewController animated:YES completion:nil];
}



#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization
{
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    [self showSuccess];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController
{
    NSLog(@"PayPal Future Payment Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization
{
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Authorize Profile Sharing

- (IBAction)getUserAuthorizationForProfileSharing:(id)sender
{
    
    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
    [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization
{
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    [self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
{
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization
{
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}


#pragma mark - Helpers

- (void)showSuccess
{
    self.successView.hidden = NO;
    self.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    self.successView.alpha = 0.0f;
    [UIView commitAnimations];
}



- (IBAction)btnContinuePressed:(id)sender
{
    
    if (self.txtFirstName.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter first name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtLastName.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter last name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtComapny.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter company" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtAddress.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
//    else if (self.txtApt.text.length==0) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter apt,site" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
    
    else if (self.txtCity.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter city" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtCountry.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter country" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (self.txtState.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter state" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (self.txtZipCode.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter postal code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (self.txtPhone.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter phone" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{


        
    
        self.resultText = nil;
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        
        
        
        PayPalItem *item1 = [PayPalItem itemWithName:@"Old jeans with holes"
                                        withQuantity:2
                                           withPrice:[NSDecimalNumber decimalNumberWithString:@"84.99"]
                                        withCurrency:@"USD"
                                             withSku:@"Hip-00037"];
        PayPalItem *item2 = [PayPalItem itemWithName:@"Free rainbow patch"
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:@"0.00"]
                                        withCurrency:@"USD"
                                             withSku:@"Hip-00066"];
        PayPalItem *item3 = [PayPalItem itemWithName:@"Long-sleeve plaid shirt (mustache not included)"
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:@"37.99"]
                                        withCurrency:@"USD"
                                             withSku:@"Hip-00291"];
        
        NSArray *items = @[item1, item2, item3];
        
        
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        NSLog(@"%@",total);
        float price = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
        NSString *p =[NSString stringWithFormat:@"%.2f",price];
        NSDecimalNumber *subTotal = [[NSDecimalNumber alloc] initWithString:p];
        
        NSLog(@"%@",subTotal);
        
        NSDecimalNumber *total_Price = [[subTotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];

        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subTotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        NSLog(@"%@", [PayPalMobile libraryVersion]);
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        
        payment.amount = total_Price;
        NSLog(@"%@",payment.amount);
        payment.currencyCode = @"USD";
        payment.shortDescription = @"Total Price";
       // payment.items = items;  // if not including multiple items, then leave payment.items as nil
       // payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
        
        if (!payment.processable)
        {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        
        // Update payPalConfig re accepting credit cards.
        self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        
       // paymentViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentViewController:paymentViewController animated:YES completion:nil];
    }


}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.tag==0)
    {
        [self.txtLastName becomeFirstResponder];
    }else if (textField.tag==1)
    {
        [self.txtComapny becomeFirstResponder];
    }else if (textField.tag==2)
    {
        [self.txtAddress becomeFirstResponder];
        
        CGRect frame=self.view.frame;
        frame.origin.y=-20;
        self.view.frame=frame;

    }else if (textField.tag==3)
    {
         [self.txtApt becomeFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=-40;
        self.view.frame=frame;

    }else if (textField.tag==4)
    {
        [self.txtCity becomeFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=-20;
        self.view.frame=frame;
        [textField resignFirstResponder];

    }else if (textField.tag==5)
    {
        [textField resignFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=64;
        self.view.frame=frame;
        
        
        

    }else if (textField.tag==6)
    {
        [self.txtState becomeFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=-80;
        self.view.frame=frame;

    }else if (textField.tag==7)
    {
         [self.txtZipCode becomeFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=-90;
        self.view.frame=frame;
    }
    else if (textField.tag==8)
    {
        [self.txtPhone becomeFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=-160;
        self.view.frame=frame;

        
    }
     else if (textField.tag==9)
    {
        [textField resignFirstResponder];
        CGRect frame=self.view.frame;
        frame.origin.y=64;
        self.view.frame=frame;
  
    }
    
       return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtFirstName resignFirstResponder];
     [self.txtLastName resignFirstResponder];
     [self.txtComapny resignFirstResponder];
     [self.txtCity resignFirstResponder];
     [self.txtAddress resignFirstResponder];
     [self.txtApt resignFirstResponder];
     [self.txtCountry resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    if (textField == self.txtCountry) {
        [self.txtCountry resignFirstResponder];
        picker = [[UIPickerView alloc] init];
        picker.frame = CGRectMake(0, self.txtCountry.frame.origin.y-230, self.view.frame.size.width, 230);
        
        picker.delegate = self;
        picker.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:picker];

    }else if(textField.tag==7)
    {
        CGRect frame=self.view.frame;
        frame.origin.y=-80;
        self.view.frame=frame;

    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.txtCity) {
        [self.txtCity resignFirstResponder];
    }
}
# pragma mark -  Order Api
-(void)orderApi
{
    lineArry=[[NSMutableArray alloc]init];
    mainDict=[[NSMutableDictionary alloc]init];
    finalDict=[[NSMutableDictionary alloc]init];
    [self coreDataFetch];
    
    for (int i=0; i<fetchedObjects.count; i++)
    {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        
        
        NSLog(@"%@,%@",obj.varId,obj.qty);
        
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:obj.varId forKey:@"variant_id"];
         [dict setValue:obj.qty forKey:@"quantity"];
        [dict setValue:@"barcode" forKey:@"title"];
        //[dict setObject:@"1001" forKey:@"name"];
        [dict setObject:obj.price forKey:@"price"];

        [lineArry addObject:dict];
        

    }
    //[[NSUserDefaults standardUserDefaults]setValue:email forKey:@"email"];
    NSString *userEmail = [[NSUserDefaults standardUserDefaults]valueForKey:@"email"];
    [mainDict setObject:userEmail forKey:@"email"];
     [mainDict setObject:@"fulfilled" forKey:@"fulfillment_status"];
     [mainDict setObject:[NSNumber numberWithBool:true] forKey:@"send_receipt"];
      //[mainDict setObject:[NSNumber numberWithBool:true] forKey:@"send_fulfillment_receipt"];
    [mainDict setObject:lineArry forKey:@"line_items"];
    
    [finalDict setObject:mainDict forKey:@"order"];
    
    

    NSLog(@"%@",finalDict);
    [self deleteCoreData];
    
   
    [self dataForApi];
    
    
//    
//        UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Thank you for your purchase.  Your order has been placed successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];

}


-(void)dataForApi
{
//    NSDictionary *newDatasetInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.txtEmail.text, @"email", self.firstName.text, @"first_name", self.txtLastName.text,@"last_name",[NSNumber numberWithBool:flag],@"verified_email",[NSNumber numberWithBool:sendEmailFlag],@"send_email_invite",nil];
    [self showLoadingView];
    
    NSDictionary *dictApi = [NSDictionary dictionaryWithObjectsAndKeys:mainDict,@"order", nil];
    
    //convert object to data
    NSError *error =  nil;
    NSLog(@"%@",dictApi);
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictApi options:kNilOptions error:&error];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://5fdc64a62f9a5e53c3c72411b8c2c461:0f36722dd80bfafbe3e956a006a31bc0@buddhapants-2.myshopify.com/admin/orders.json"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:jsonData];
    
    // print json:
    NSLog(@"JSON summary: %@", [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding]);
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
    
    


}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
   
    
   // NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    NSError *error = nil;
    
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    id order_idArray = [[result valueForKey:@"order"] valueForKey:@"customer"];
    
    NSString *order_id = [order_idArray valueForKey:@"last_order_id"];
 
    [self hideLoadingView];

    SuccessViewController *successVC = [[SuccessViewController alloc]init];
    successVC.orderID = order_id;
    [self.navigationController pushViewController:successVC animated:YES];
    /// NSArray *array = [result valueForKey:@"customers"];
    }
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
}

# pragma mark - Core Data Fetch
-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"%@",fetchedObjects);
    
}

-(void)deleteCoreData{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *order in fetchedObjects) {
        [managedObjectContext deleteObject:order];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];

}
# pragma mark - Loading Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
