//
//  StoreViewController.m
//  BuddhaPants
//
//  Created by brst on 03/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "StoreViewController.h"

@interface StoreViewController ()
{
    Api_Wrapper *obj;
}

@end

@implementation StoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    managedObjectContext = [ad managedObjectContext];

    
    self.view.backgroundColor=[UIColor whiteColor];
    product_Array = [[NSMutableArray alloc]init];
    title_Array = [[NSMutableArray alloc]init];
    src_Array = [[NSMutableArray alloc]init];
    handleArray=[[NSMutableArray alloc]init];
    idOfproductsArray=[[NSMutableArray alloc]init];
   
    
    varrient_array = [[NSMutableArray alloc]init];
    [self storeApi];
  //  NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
    
    if ([UIScreen mainScreen].bounds.size.height==568)
    {
         [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
    else  if ([UIScreen mainScreen].bounds.size.height==480)
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }

    
    else if ([UIScreen mainScreen].bounds.size.height==667)
    {
         [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell~6" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
    
    else
    {
        [self.collectionView registerNib:[UINib nibWithNibName:@"MYCell~6+" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    }
   
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
     self.navigationItem.titleView = self.navigationItem.defaultTitleView;

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
   
//    UIBarButtonItem * rightBar=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart12"] style:UIBarButtonItemStylePlain target:self action:@selector(cartbuttonPressed)];
//    self.navigationItem.rightBarButtonItem=rightBar;
    // Do any additional setup after loading the view from its nib.
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    
    // Allocate UIButton
  UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    //Add NKNumberBadgeView as a subview on UIButton
    
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;

  
}


#pragma mark Left bar button

-(void)barbuttonPressed
{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    addtoCart = true;

    
    [self coreDataFetch];
    subTotal= 0;
   
    [tableView_Cart reloadData];
    
    self.navigationController.navigationBar.hidden =NO;
    if (fetchedObjects.count==0)
    {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return product_Array.count;
}




// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (MYCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    MYCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    
    NSString *src = [[product_Array objectAtIndex:indexPath.row]valueForKey:@"src"];
    
    if (src == nil|| src==(id)[NSNull null])
    {
        NSLog(@"null image");
        cell.imgView_product.image = [UIImage imageNamed:@"ic_no_image.jpg"];
    }
    else
    {
    
   // NSLog(@"src=%@",idOfproductsArray);
    
     cell.imgView_product.contentMode=UIViewContentModeScaleAspectFit;
     [cell.imgView_product sd_setImageWithURL:[NSURL URLWithString:[[product_Array objectAtIndex:indexPath.row]valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
    }
    
    
    cell.lbl_Tittle.text =[title_Array objectAtIndex:indexPath.row];
    
   
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    image_array = [[NSMutableArray alloc]init];
    
    NSString *htmlString = [bodyHtml_Array objectAtIndex:indexPath.row];
    
   
    
    NSMutableArray *var_array = [[NSMutableArray alloc]init];
    
    ProductViewController *productVC =  [[ProductViewController alloc]init];
    
    image_array = [src_Array objectAtIndex:indexPath.row];
    NSLog(@"%@",image_array);
    
    var_array = [varrient_array objectAtIndex:indexPath.row];
    
    productVC.slideImageArray = image_array;
    productVC.str_lblName = [title_Array objectAtIndex:indexPath.row];
    productVC.varientArray = var_array;
    productVC.strHtml = htmlString;
    productVC.handleStr=[handleArray objectAtIndex:indexPath.row];
    productVC.idForSelectedProduct=[idOfproductsArray objectAtIndex:indexPath.row];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:productVC animated:YES];
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIScreen mainScreen].bounds.size.height==568)
    {
        return CGSizeMake(self.view.frame.size.width/2, 142);
    }
    else if ([UIScreen mainScreen].bounds.size.height==667){
        return CGSizeMake(self.view.frame.size.width/2, 188);
    }
    else if ([UIScreen mainScreen].bounds.size.height==480){
        return CGSizeMake(self.view.frame.size.width/2, 160);
    }

    else
        return CGSizeMake(self.view.frame.size.width/2, 192);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)storeApi
{
    obj = [Api_Wrapper shared];
    
    //[self showLoadingView];
    
    [obj GET:nil string:product_String completion:^(NSDictionary *json, BOOL success) {
        NSLog(@"%@",json);
              
        product_Array = [[json valueForKey:@"products"]valueForKey:@"image"];
        title_Array  =[[json valueForKey:@"products"]valueForKey:@"title"];
        src_Array = [[json valueForKey:@"products"]valueForKey:@"images"];
        
        varrient_array = [[json valueForKey:@"products"]valueForKey:@"variants"];
        bodyHtml_Array = [[json valueForKey:@"products"]valueForKey:@"body_html"];
        handleArray=[[json valueForKey:@"products"]valueForKey:@"handle"];
        idOfproductsArray=[[json valueForKey:@"products"]valueForKey:@"id"];
        
        [self.collectionView reloadData];
    
       // [self hideLoadingView];

      
    }];
    
        
}

-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)cartbuttonPressed
{
    if (addtoCart)
    {
        
    
    subTotal=0;
    [self viewWillAppear:YES];
    
    if(fetchedObjects.count==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
    
    
    backGroundView = [[UIView alloc]init];
    backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    backGroundView.backgroundColor = [UIColor blackColor];
    backGroundView.alpha =  0.50;
    
    [self.view addSubview:backGroundView];
    
    
    arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
    
    arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
    
    [self.view addSubview:arwView];
    
    view1 = [[UIView alloc]init];
    
    view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
    view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    
    //    view1.layer.borderWidth = 2.0;
    //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
    [self.view addSubview:view1];
    
    
    
    
    UIView * backView = [[UIView alloc]init];
    
    backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
    
    backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
    backView.layer.cornerRadius = 7.0;
    
    [view1 addSubview:backView];
    
    
    lblPrice1  = [[UILabel alloc]init];
    lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
    //lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
    lblPrice1.textAlignment=NSTextAlignmentRight;
    lblPrice1.adjustsFontSizeToFitWidth=YES;
    lblPrice1.font = [UIFont boldSystemFontOfSize:17];
    lblPrice1.textColor = [UIColor lightGrayColor];
    
    [backView addSubview:lblPrice1];
    
    
    
    UILabel *lblSubTotal  = [[UILabel alloc]init];
    lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
    lblSubTotal.text = @"SubTotal";
    lblSubTotal.textAlignment=NSTextAlignmentRight;
    lblSubTotal.textColor = [UIColor lightGrayColor];
    lblSubTotal.font = [UIFont systemFontOfSize:13];
    [backView addSubview:lblSubTotal];
    
    
    
    tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
    
    tableView_Cart.dataSource=self;
    tableView_Cart.delegate=self;
    tableView_Cart.backgroundColor=[UIColor whiteColor];
    [view1 addSubview:tableView_Cart];
    
    UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeCustom];
    
    btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
    btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
    [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
    [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:btnCheckOut];
        addtoCart = false;
    }
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  fetchedObjects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
        cellView.backgroundColor=[UIColor whiteColor];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
               profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        profileImageView.layer.borderWidth=0.70;
        
        
        
        [cellView addSubview:profileImageView];
        
        lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
        lblProduct.font = [UIFont boldSystemFontOfSize:18];
        lblProduct.backgroundColor=[UIColor clearColor];
        lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        lblProduct.adjustsFontSizeToFitWidth=YES;
        //lblProduct.text=@"Savannah Winter";
        lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblProduct];
        
        lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
        lblPrice.font = [UIFont systemFontOfSize:12];
        lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        
        lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblPrice];
        
        lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
        lblSize.font = [UIFont systemFontOfSize:12];
        lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblSize.font = [UIFont systemFontOfSize:15];
        lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:lblSize];
        
        lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
        lblQty.font = [UIFont systemFontOfSize:12];
        lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
        
        lblQty.font = [UIFont systemFontOfSize:15];
        lblQty.textAlignment = NSTextAlignmentCenter;
        lblQty.layer.borderWidth = .5;
        lblPrice.layer.borderColor = [UIColor redColor].CGColor;
        
        [cellView addSubview:lblQty];
        
        
    }
    Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
    profileImageView.image=[UIImage imageWithData:obj1.image];
    
    
    lblProduct.text =obj1.name;
    lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
    lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
    lblQty.text = obj1.qty;
    
    
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 81;
}

-(void)subTotal
{
    
    for (int i  =0 ; i<fetchedObjects.count; i++)
    {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        float result= [obj.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}

-(void)checkOutPresssed
{
    
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}

-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
   }
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}


@end
