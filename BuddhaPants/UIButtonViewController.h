//
//  UIButtonViewController.h
//  BuddhaPants
//
//  Created by brst on 5/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteViewController.h"

@interface UIButtonViewController : NSObject

{
    UIColor *selectedBtnColor,*notselectedBtnColor;
    
    UIButton*voteButton,*uploadButton;
}

-(UIView *)lowerButton:(CGRect)frame;
@end
