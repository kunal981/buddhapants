//
//  BrandRepsViewController.m
//  BuddhaPants
//
//  Created by brst on 06/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "BrandRepsViewController.h"

@interface BrandRepsViewController (){
    Api_Wrapper *obj;
}

@end

@implementation BrandRepsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self brands_repsApi];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    UIBarButtonItem * rightBar=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart12"] style:UIBarButtonItemStylePlain target:self action:@selector(cartbuttonPressed)];
    self.navigationItem.rightBarButtonItem=rightBar;    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
    self.txtNmae.layer.borderColor = [UIColor colorWithRed:0.4471 green:0.0157 blue:0.0863 alpha:1.0].CGColor;
    self.txtNmae.layer.borderWidth = 1.0;
    
    self.txtCompany.layer.borderColor = [UIColor colorWithRed:0.4471 green:0.0157 blue:0.0863 alpha:1.0].CGColor;
    self.txtCompany.layer.borderWidth = 1.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    
    
    self.txtNmae.leftView = paddingView;
    self.txtNmae.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCompany.leftView = paddingView1;
    self.txtCompany.leftViewMode = UITextFieldViewModeAlways;

   
    // Do any additional setup after loading the view from its nib.
}

-(void)cartbuttonPressed{
    UIAlertView *alert =   [[UIAlertView alloc]initWithTitle:nil message:@"Your Cart is Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden =NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)brands_repsApi {
    obj = [Api_Wrapper shared];
    
    
    [obj GET:nil string:brandReps_String completion:^(NSDictionary *json, BOOL success) {
        
        
        
        NSArray* contactArray = [json valueForKey:@"page"];
        
        
        NSString *htmlString = [contactArray  valueForKey:@"body_html"];
        
        
        NSLog(@"%@",htmlString);
        [self.webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://www.buddhapants.com/pages/get-involved"]];
        
    }];
}


@end
