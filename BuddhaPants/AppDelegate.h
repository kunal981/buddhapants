//
//  AppDelegate.h
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBProgressHUD;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
    NSString* tokenString;
}

+(MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title ;
+ (void)dismissGlobalHUD;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@property (strong, nonatomic) UINavigationItem *navItem;


@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

