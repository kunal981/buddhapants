//
//  VoteViewController.m
//  BuddhaPants
//
//  Created by brst on 5/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "VoteViewController.h"

@interface VoteViewController ()
{
    Api_Wrapper *objApi;

}

@end
static NSString* cellidentifier1=@"ArtpantsCell";
@implementation VoteViewController

-(void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    view.backgroundColor=[UIColor whiteColor];
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
    dictData=[[NSMutableDictionary alloc]init];
    dataArray=[[NSMutableArray alloc]init];
    sendDataDict=[[NSMutableDictionary alloc]init];
    
    selectedBtnColor=[UIColor colorWithRed:0.7176 green:0.7176 blue:0.7176 alpha:1.0];
    notselectedBtnColor=[UIColor colorWithRed:0.7765 green:0.7765 blue:0.7765 alpha:1.0];
    self.navigationItem.titleView = self.navigationItem.defaultTitleView;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barbuttonPressed)];
    
    
    //****** Right UIBarButton With Label ******//
    
    lblBadage = [[UILabel alloc] init];
    lblBadage.frame = CGRectMake(30, 0, 16,16);
    lblBadage.backgroundColor = [UIColor whiteColor];
    lblBadage.layer.cornerRadius = 8;
    lblBadage.layer.masksToBounds = YES;
    
    lblBadage.font = [UIFont systemFontOfSize:11];
    lblBadage.textAlignment = NSTextAlignmentCenter;
    lblBadage.textColor = [UIColor colorWithRed:1.0000 green:0.4000 blue:0.2000 alpha:1.0];
    
    
    // Allocate UIButton
    UIButton*  btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setBackgroundImage:[UIImage imageNamed:@"cart12"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:lblBadage];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
    
    tableViewVote=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-84)];
     [tableViewVote registerClass:[ArtpantsCell class] forCellReuseIdentifier:cellidentifier1];
    tableViewVote.dataSource=self;
    tableViewVote.delegate=self;
    tableViewVote.separatorColor=[UIColor clearColor];
    tableViewVote.backgroundColor=[UIColor whiteColor];
    tableViewVote.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:tableViewVote];

    
    
    [self lowerButton];
    
    [self getAllImagesApi];

}

#pragma mark Lower button
-(void)lowerButton
{
    
    
    CGFloat width=[UIScreen mainScreen].bounds.size.width;
    
    UIView * buttonLowerView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-84, width, 40)];
    buttonLowerView.backgroundColor=notselectedBtnColor;
    [self.view addSubview:buttonLowerView];
    
    NSLog(@"%@",NSStringFromCGRect(buttonLowerView.frame));
    
    voteButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    voteButton.frame = CGRectMake(0, 0, (self.view.frame.size.width)/2, 40);
    voteButton.backgroundColor = selectedBtnColor;
    [voteButton setTitle:@"VOTE" forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    voteButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [voteButton addTarget:self action:@selector(voteButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:voteButton];
    
    uploadButton = [UIButton  buttonWithType:UIButtonTypeSystem];
    uploadButton.frame = CGRectMake(voteButton.frame.size.width+voteButton.frame.origin.x, 0, (self.view.frame.size.width)/2, 40);
    uploadButton.backgroundColor = notselectedBtnColor;
    [uploadButton setTitle:@"UPLOAD" forState:UIControlStateNormal];
    uploadButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
    [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [uploadButton addTarget:self action:@selector(uploadButtonPresssed) forControlEvents:UIControlEventTouchUpInside];
    [buttonLowerView addSubview:uploadButton];
    
    
}

-(void)voteButtonPresssed
{
//    voteButton.backgroundColor = selectedBtnColor;
//    uploadButton.backgroundColor=notselectedBtnColor;
//    [voteButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
//    [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}
-(void)uploadButtonPresssed
{
    voteButton.backgroundColor = notselectedBtnColor;
    uploadButton.backgroundColor=selectedBtnColor;
    [uploadButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [voteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UploadViewController * voteVC=[[UploadViewController alloc]init];
    
    [self.navigationController pushViewController:voteVC animated:YES];

    
}

#pragma mark Left bar button

-(void)barbuttonPressed{
    SlideNewViewController *slideVC = [[SlideNewViewController alloc]init];
    
    [self.navigationController pushViewController:slideVC animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    addtoCart = true;
    [self coreDataFetch];
    subTotal= 0;
    self.navigationController.navigationBar.hidden =NO;
    
    if (fetchedObjects.count==0) {
        lblBadage.text=[NSString stringWithFormat:@"%@",@"0"];
        
        
    }
    else
    {
        lblBadage.text=[NSString stringWithFormat:@"%lu",(unsigned long)fetchedObjects.count];
        
    }
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor lightGrayColor];
    [self.refreshControl addTarget:self
                            action:@selector(getAllImagesApi)
                  forControlEvents:UIControlEventValueChanged];
    [tableViewVote addSubview:self.refreshControl];

    
    
}

#pragma mark Api Hit
-(void)getAllImagesApi
{
    
    [sendDataDict setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"uid"] forKey:@"uid"];
    
    
    objApi = [Api_Wrapper shared];
    
   // [self showLoadingView];
    
    
    [objApi PostApi:sendDataDict string:imageGet_String
         completion:^(NSDictionary *json, BOOL sucess) {
             NSLog(@"%@",json);
             
             if ([[json valueForKey:@"status"] isEqualToString:@"true"])
             {
                 
                 
                 dataArray=[json valueForKey:@"pants"];
                 
                 NSLog(@"%@",dataArray);
                
                 if (dataArray.count==0)
                 {
                     UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Oops !" message:@"This post is no longer available." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     
                     [alert show];
                     
                   
                     
                  
                     //[self hideLoadingView];
                   
                     return ;
                 }
                 
                 [self.refreshControl endRefreshing];

                [tableViewVote reloadData];
                // [self hideLoadingView];
                 
                 
             }
             
             
            // [self hideLoadingView];
             
             
         }];
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    ArtPantsViewController *view = [[ArtPantsViewController alloc]init];
    [self.navigationController pushViewController:view animated:NO];
}


//-(void)showLoadingView
//{
//    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.userInteractionEnabled = YES;
//    hud.labelText = @"Loading..";
//    hud.dimBackground = YES;
//    
//}
//-(void)hideLoadingView
//{
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//}


#pragma mark Right bar button

-(void)cartbuttonPressed{
    if (addtoCart) {
        
    
    if(fetchedObjects.count==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your cart is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else {
        subTotal= 0;
        [self coreDataFetch];
        
        
        
        backGroundView = [[UIView alloc]init];
        backGroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        backGroundView.backgroundColor = [UIColor blackColor];
        backGroundView.alpha =  0.50;
        
        [self.view addSubview:backGroundView];
        
        
        arwView=[[UIView alloc]initWithFrame:CGRectMake(80,0,self.view.frame.size.width,320)];
        
        arwView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"box"]];
        
        [self.view addSubview:arwView];
        
        view1 = [[UIView alloc]init];
        
        view1.frame = CGRectMake(0, 15, self.view.frame.size.width, 320);
        view1.backgroundColor=[UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
        
        //    view1.layer.borderWidth = 2.0;
        //    view1.layer.borderColor =  [UIColor darkGrayColor].CGColor;
        [self.view addSubview:view1];
        
        
        
        
        UIView * backView = [[UIView alloc]init];
        
        backView.frame = CGRectMake(0,0, view1.frame.size.width, 60);
        
        backView.backgroundColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0];
        backView.layer.cornerRadius = 7.0;
        
        [view1 addSubview:backView];
        
        
        lblPrice1  = [[UILabel alloc]init];
        lblPrice1.frame = CGRectMake(self.view.frame.size.width-80, 10, 70, 25);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        lblPrice1.textAlignment=NSTextAlignmentRight;
        lblPrice1.adjustsFontSizeToFitWidth=YES;
        lblPrice1.font = [UIFont boldSystemFontOfSize:17];
        lblPrice1.textColor = [UIColor lightGrayColor];
        
        [backView addSubview:lblPrice1];
        
        
        UILabel *lblSubTotal  = [[UILabel alloc]init];
        lblSubTotal.frame = CGRectMake(self.view.frame.size.width-80, 30, 70, 25);
        lblSubTotal.text = @"SubTotal";
        lblSubTotal.textAlignment=NSTextAlignmentRight;
        
        lblSubTotal.textColor = [UIColor lightGrayColor];
        lblSubTotal.font = [UIFont systemFontOfSize:13];
        [backView addSubview:lblSubTotal];
        
        
        
        tableView_Cart=[[UITableView alloc]initWithFrame:CGRectMake(0, backView.frame.size.height+backView.frame.origin.y, view1.frame.size.width, view1.frame.size.height-120)];
        
        tableView_Cart.dataSource=self;
        tableView_Cart.delegate=self;
        tableView_Cart.backgroundColor=[UIColor whiteColor];
        [view1 addSubview:tableView_Cart];
        
        UIButton *btnCheckOut = [UIButton  buttonWithType:UIButtonTypeSystem];
        
        btnCheckOut.frame = CGRectMake(10, tableView_Cart.frame.size.height+tableView_Cart.frame.origin.y+10, view1.frame.size.width-20, 35);
        btnCheckOut.backgroundColor = [UIColor colorWithRed:0.4157 green:0.8000 blue:0.7451 alpha:1.0];
        [btnCheckOut setTitle:@"CHECKOUT" forState:UIControlStateNormal];
        [btnCheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCheckOut addTarget:self action:@selector(checkOutPresssed) forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnCheckOut];
        
    }
        addtoCart = false;
    }
    
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width,size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


- (UIImage * )scaleImage:(UIImage *)image maxWidth:(int) maxWidth maxHeight:(int) maxHeight
{
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    if (width <= maxWidth && height <= maxHeight)
    {
        return image;
    }
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    if (width > maxWidth || height > maxHeight)
    {
        CGFloat ratio = width/height;
        
        if (ratio > 1)
        {
            bounds.size.width = maxWidth;
            bounds.size.height = bounds.size.width / ratio;
        }
        else
        {
            bounds.size.height = maxHeight;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(context, scaleRatio, -scaleRatio);
    CGContextTranslateCTM(context, 0, -height);
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
    
}
- (NSMutableAttributedString*)colorWord:(NSString *)sender {
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:sender];
    
    NSArray *words=[sender componentsSeparatedByString:@" "];
    
    for (NSString *word in words) {
        if ([word hasPrefix:@"by"]) {
            NSRange range=[sender rangeOfString:word];
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:range];
        }
    }
    return string;
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tableViewVote)
    {
        NSLog(@"%lu",(unsigned long)dataArray.count);
        
                return dataArray.count;
        
    }
    else
    {
         return  fetchedObjects.count;
    }
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableView==tableViewVote)
    {
           ArtpantsCell * artCell = [tableView dequeueReusableCellWithIdentifier:cellidentifier1];
        artCell.backgroundColor=[UIColor whiteColor];

      
        
//       artCell.uploadImageView.contentMode=UIViewContentModeScaleAspectFit;
        
//        NSData * data=[[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row]valueForKey:@"image"]]];
//        
//        UIImage * image=[UIImage imageWithData:data];
////        image= [self scaleImage:image maxWidth:artCell.uploadImageView.frame.size.width maxHeight:artCell.uploadImageView.frame.size.height];
////        
//        
//        image=[self imageWithImage:image convertToSize:CGSizeMake(artCell.uploadImageView.frame.size.width, artCell.uploadImageView.frame.size.height)];
//        artCell.uploadImageView.image=image;
    
        [artCell.uploadImageView sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row]valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"images.jpeg"] options:SDWebImageRefreshCached];

        
        CGFloat x=0;
        for (int i=0; i<5; i++)
        {
            NSInteger u_rating=[[[dataArray objectAtIndex:indexPath.row]valueForKey:@"u_rating"]integerValue];

            voteBtn=[UIButton buttonWithType:UIButtonTypeSystem];
            voteBtn.frame=CGRectMake(x,artCell.uploadImageView.frame.size.height+artCell.uploadImageView.frame.origin.y, (self.view.frame.size.width-5)/5, 35) ;
            voteBtn.tag=i+1;
            
            if (voteBtn.tag==u_rating)
            {
                
                voteBtn.backgroundColor=[UIColor colorWithRed:0.6431 green:1.0000 blue:0.9608 alpha:1.0];
            }else
            {
                
                 voteBtn.backgroundColor=[UIColor colorWithRed:0.3059 green:0.8863 blue:0.8275 alpha:1.0];
            }

            voteBtn.contentHorizontalAlignment=UIControlContentVerticalAlignmentCenter;
            if (i==0) {
                
            
          
                //[voteBtn setImage:[UIImage imageNamed:@"star1.png"] forState:UIControlStateNormal];
                //voteBtn.tintColor = [UIColor orangeColor];
                
                [voteBtn setBackgroundImage:[UIImage imageNamed:@"1star"] forState:UIControlStateNormal];
                
            }
            
            if (i==1) {
                
                
                [voteBtn setBackgroundImage:[UIImage imageNamed:@"star2"] forState:UIControlStateNormal];
                
            }
            if (i==2) {
                
                
                
               [voteBtn setBackgroundImage:[UIImage imageNamed:@"star3"] forState:UIControlStateNormal];
            }
            if (i==3) {
                
                
                
               [voteBtn setBackgroundImage:[UIImage imageNamed:@"star4"] forState:UIControlStateNormal];
                
            }
            if (i==4) {
                
                
                
               [voteBtn setBackgroundImage:[UIImage imageNamed:@"star5"] forState:UIControlStateNormal];
                
            }
            [voteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [voteBtn addTarget:self action:@selector(votebuttonPressed:withevent:) forControlEvents:UIControlEventTouchUpInside];
           
            
            [artCell.contentView addSubview:voteBtn];
            
            
            
            NSLog(@"%@",voteBtn.titleLabel.text);
            
            x+=voteBtn.frame.size.width+1;
            
        }

        
        artCell.lblVoteValue1.text=[NSString stringWithFormat:@"%@ votes",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"one_rating"]];
        artCell.lblVoteValue2.text=[NSString stringWithFormat:@"%@ votes",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"two_rating"]];
        artCell.lblVoteValue3.text=[NSString stringWithFormat:@"%@ votes",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"three_rating"]];
        artCell.lblVoteValue4.text=[NSString stringWithFormat:@"%@ votes",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"four_rating"]];
        artCell.lblVoteValue5.text=[NSString stringWithFormat:@"%@ votes",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"five_rating"]];
        
        
        artCell.lblTittle.text=[[dataArray objectAtIndex:indexPath.row]valueForKey:@"title"];
        
        NSString * userName= [NSString stringWithFormat:@"by %@",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"user_name"]];
        NSMutableAttributedString * string=[self colorWord:userName];
        
        [artCell.lblUsername setAttributedText:string];
        
       

        artCell.description.text=[[dataArray objectAtIndex:indexPath.row]valueForKey:@"description"];

      
        
        artCell.selectionStyle=UITableViewCellSelectionStyleNone;
        return artCell;
    }else
    { static NSString* cellidentifier=nil;
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (cell==nil) {
            
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
            cell.backgroundColor=[UIColor clearColor];
            
            cellView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width,80)];
            cellView.backgroundColor=[UIColor whiteColor];
            cellView.tag=indexPath.row;
            [cell.contentView addSubview:cellView];
            
            profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10,60,60)];
            
            profileImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
            profileImageView.layer.borderWidth=0.70;
            
            
            
            [cellView addSubview:profileImageView];
            
            lblProduct = [[UILabel alloc] initWithFrame:CGRectMake(70,6,270,22)];
            lblProduct.font = [UIFont boldSystemFontOfSize:18];
            lblProduct.backgroundColor=[UIColor clearColor];
            lblProduct.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
            lblProduct.adjustsFontSizeToFitWidth=YES;
            //lblProduct.text=@"Savannah Winter";
            lblProduct.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
            [cellView addSubview:lblProduct];
            
            lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(70,20,170,30)];
            lblPrice.font = [UIFont systemFontOfSize:12];
            lblPrice.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
            
            
            lblPrice.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
            [cellView addSubview:lblPrice];
            
            lblSize = [[UILabel alloc] initWithFrame:CGRectMake(70,38,170,30)];
            lblSize.font = [UIFont systemFontOfSize:12];
            lblSize.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
            
            lblSize.font = [UIFont systemFontOfSize:15];
            lblSize.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
            [cellView addSubview:lblSize];
            
            lblQty = [[UILabel alloc] initWithFrame:CGRectMake(cellView.frame.size.width-60,32,20,20)];
            lblQty.font = [UIFont systemFontOfSize:12];
            lblQty.textColor = [UIColor colorWithRed:0.4275 green:0.8000 blue:0.7333 alpha:1.0];
            
            lblQty.font = [UIFont systemFontOfSize:15];
            lblQty.textAlignment = NSTextAlignmentCenter;
            lblQty.layer.borderWidth = .5;
            lblPrice.layer.borderColor = [UIColor redColor].CGColor;
            
            [cellView addSubview:lblQty];
            
            
        }
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        profileImageView.image=[UIImage imageWithData:obj1.image];
        
        
        lblProduct.text =obj1.name;
        lblPrice.text = [NSString stringWithFormat:@"$%@",obj1.price];
        lblSize.text = [NSString stringWithFormat:@"(%@)",obj1.size];
        lblQty.text = obj1.qty;
        
        
        
        
        
        return cell;

        
    }
    
    
    
    }
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
}
-(void)subTotal{
    for (int i  =0 ; i<fetchedObjects.count; i++) {
        Cart *obj1 = [fetchedObjects objectAtIndex:i];
        float result= [obj1.totalprice floatValue];
        
        NSLog(@"%f",result);
        subTotal =  subTotal + result;
        
        NSLog(@"toatal final%f",subTotal);
        lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", subTotal];
        
        
        [[NSUserDefaults standardUserDefaults]setFloat:subTotal forKey:@"SUBTOTAL"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
}

-(void)subttotalDelete:(float)result{
    float su  = [[NSUserDefaults standardUserDefaults]floatForKey:@"SUBTOTAL"];
    su =    su - result;
    
    NSLog(@"toatal final%f",su);
    lblPrice1.text = [NSString stringWithFormat:@"$%0.2f", su];
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:su forKey:@"SUBTOTAL"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [tableView_Cart beginUpdates];
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        Cart *obj1 = [fetchedObjects objectAtIndex:indexPath.row];
        
        float result= [obj1.totalprice floatValue];
        
        
        // subTotal =  subTotal - result;
        NSLog(@"total :%f",result);
        
        [context deleteObject:obj1];
        [context save:nil];
        
        
        
        
        
        [tableView_Cart deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self viewWillAppear:YES];
        [self subttotalDelete:result];
        
        
        [tableView_Cart endUpdates];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (tableView_Cart.editing) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (tableView==tableViewVote)
    {
          return 460;
    }else
    {
          return 81;
    }
  
}


-(void)coreDataFetch{
    
    ad =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error;
    
    managedObjectContext = [ad managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (info in fetchedObjects) {
    //        NSLog(@"name: %@", info.name);
    //
    //        NSLog(@"price: %@", info.price);
    //        NSLog(@"size: %@", info.size);
    //        NSLog(@"qty: %@", info.qty);
    //        NSLog(@"Total:%@",info.totalprice);
    //
    //              }
    
    for (int i =0; i<fetchedObjects.count; i++) {
        Cart *obj = [fetchedObjects objectAtIndex:i];
        
        float result= [obj.totalprice floatValue];
        
        
        subTotal =  subTotal + result;
        
    }
}


-(void)checkOutPresssed
{
    [arwView removeFromSuperview];
    [backGroundView removeFromSuperview];
    [view1 removeFromSuperview];
    
    ShippingAdressViewController * shippingAdress=[[ShippingAdressViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:shippingAdress animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    addtoCart = true;
    [backGroundView removeFromSuperview];
    [arwView removeFromSuperview];
    [view1 removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)votebuttonPressed:(UIButton * )sender withevent:(UIEvent *)event
{
    NSLog(@"%ld",(long)sender.tag);
    UITouch * touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:tableViewVote];
    NSIndexPath * indexPath = [tableViewVote indexPathForRowAtPoint: location];
    
    
    NSString * ratingValue=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    NSLog(@"%@",[[dataArray objectAtIndex:indexPath.row]valueForKey:@"u_image"]);
    
    [sendDataDict setValue:[[dataArray objectAtIndex:indexPath.row]valueForKey:@"u_image"] forKey:@"u_image"];
    [sendDataDict setValue:ratingValue forKey:@"rating"];
    [sendDataDict setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"uid"]  forKey:@"uid"];
    
    NSLog(@"%@",sendDataDict);
    
    [self giveRatingApi];
    
    NSLog(@"%ld",(long)indexPath.row);
}

-(void)giveRatingApi
{
    objApi = [Api_Wrapper shared];
    
 
 
    [objApi PostApiVoting:sendDataDict string:rating_String completion:^(NSDictionary *json, BOOL sucess) {
        if ([[json valueForKey:@"status"] isEqualToString:@"true"])
                         {
            
                             UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Thank you for submission!      It is with the admin for approval" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
                             [alert show];
            
            
                             [self getAllImagesApi];
                             
                            
                             
                       }
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
