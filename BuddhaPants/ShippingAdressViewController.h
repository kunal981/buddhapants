//
//  ShippingAdressViewController.h
//  BuddhaPants
//
//  Created by brst on 24/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"

@interface ShippingAdressViewController : UIViewController<UITextFieldDelegate,PayPalPaymentDelegate,PayPalFuturePaymentDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,PayPalProfileSharingDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    
    UIScrollView * scrollView;
    NSMutableArray * tittleArr,*adreesArr;
    UILabel * lblTxtField,*lblAccount;
    UITableView * tableView1;
     UIView *cellView;
      AppDelegate *ad;
    
    UIPickerView *picker;
    int flag;
    NSMutableArray * countryArr,*stateArr,*lineArry;
    NSArray *fetchedObjects;
    NSMutableDictionary * dict,*mainDict,*finalDict;
    
 
}
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property (strong, nonatomic) IBOutlet UIView *lineView;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtComapny;
@property (strong, nonatomic) IBOutlet UITextField *txtAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtApt;
@property (strong, nonatomic) IBOutlet UITextField *txtCity;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;
@property (strong, nonatomic) IBOutlet UITextField *txtState;
@property (strong, nonatomic) IBOutlet UITextField *txtZipCode;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
- (IBAction)btnContinuePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

 @end
