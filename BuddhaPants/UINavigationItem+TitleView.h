//
//  UINavigationItem+TitleView.h
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (TitleView)

- (UIImageView*) defaultTitleView ;
@end
