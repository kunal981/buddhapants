//
//  UINavigationItem+TitleView.m
//  BuddhaPants
//
//  Created by brst on 02/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "UINavigationItem+TitleView.h"
#import <objc/runtime.h>
#import <objc/message.h>

const char* DEFAULT_TITTLE_VIEW_KEY = "DEFAULT_TITLE_VIEW";

@implementation UINavigationItem (TitleView)

static UIImage *defaultTitleImage;

- (UIImageView*) defaultTitleView {
    UIImageView* view = objc_getAssociatedObject(self, DEFAULT_TITTLE_VIEW_KEY);
    
    if ( !view ) {
        if( !defaultTitleImage ) defaultTitleImage = [UIImage imageNamed:@"3"];
        
        view = [[UIImageView alloc] initWithImage:defaultTitleImage];
        objc_setAssociatedObject(self, DEFAULT_TITTLE_VIEW_KEY, view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return view;
}



@end
