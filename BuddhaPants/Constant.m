//
//  Constant.m
//  CarDealer
//
//  Created by brst on 27/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "Constant.h"

@implementation Constant

 NSString* const register_String= @"customers.json";
 NSString* const page_String= @"pages/17734007.json";
 NSString* const retailers_String= @"pages/18131879.json";
 NSString* const event_String= @"pages/20270275.json";
 NSString* const product_String= @"products.json";
 NSString* const aboutUS_String= @"pages/20867691.json";
 NSString* const contactUS_String= @"pages/18131831.json";
 NSString* const brandReps_String= @"pages/22109183.json";
 NSString* const wholeSales_String= @"pages/22133879.json";
 NSString* const login_String= @"customers/search.json";
NSString* const order_String= @"orders.json";
NSString* const about_String= @"AboutForm.php";
NSString* const upload_String= @"ImageUpload.php";

NSString* const imageGet_String = @"DataGet.php";
NSString* const rating_String = @"ImageRating.php";
NSString* const custom_Collection = @"custom_collections.json";


NSString* const product_String_Home = @"products.json?ids=2625251715,6814845315,2172557251,457077051,471416363,1203664003,45707751,1203875779,2354036035,6643741507,2245637251,504389891,2245637251,1981538115,306360455,306359643,418586319,1981437891,2306186051,1981471171,1981426435,306340031,1981499203,504388611,388520943,419062891,306361031,306329599,306365483,1203758851,306340031,306362491,457436543,306361543,306364159";



@end
