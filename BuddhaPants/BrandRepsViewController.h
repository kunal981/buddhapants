//
//  BrandRepsViewController.h
//  BuddhaPants
//
//  Created by brst on 06/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandRepsViewController : UIViewController<UITextFieldDelegate,UIWebViewDelegate>{
    
}
@property (strong, nonatomic) IBOutlet UITextField *txtNmae;
@property (strong, nonatomic) IBOutlet UITextField *txtCompany;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
