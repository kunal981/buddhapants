//
//  BrandReps2ViewController.h
//  BuddhaPants
//
//  Created by brst on 23/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandReps2ViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
     UILabel* lblPrice1;
      NSMutableDictionary * dictData;
    
    
    UIScrollView * scrollView;
    NSMutableArray * tittleArr;
    
    float subTotal;
    UILabel* lblBadage,*lblProduct,*lblPrice,*lblSize,*lblQty;
    UIView *backGroundView;
    UIView *arwView,*view1;
    UITableView *tableView_Cart;
    AppDelegate *ad;
    NSManagedObjectContext *managedObjectContext;
    NSArray *fetchedObjects;
    UIView *cellView;
    UIImageView *profileImageView;
    BOOL addtoCart;
}

@end
