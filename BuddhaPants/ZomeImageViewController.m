//
//  ZomeImageViewController.m
//  BuddhaPants
//
//  Created by brst on 14/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ZomeImageViewController.h"


@interface ZomeImageViewController ()
{
    double width;
    double height;
    double x;
    double y;
}

@end


@implementation ZomeImageViewController
//@synthesize galleryImages = galleryImages_;
@synthesize imageHostScrollView = imageHostScrollView_;
@synthesize currentIndex = currentIndex_;

@synthesize prevImage;
@synthesize nxtImage;

@synthesize counterTitle;

@synthesize prevImgView;
@synthesize centerImgView;
@synthesize nextImgView;


#define safeModulo(x,y) ((y + x % y) % y)

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //[AppDelegate dismissGlobalHUD];
    
    
    self.imageHostScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.imageHostScrollView.frame)*self.slideZoomArray.count, CGRectGetHeight(self.imageHostScrollView.frame));
    
    self.imageHostScrollView.delegate = self;
    
    CGRect rect = CGRectZero;
    
    rect.size = CGSizeMake(CGRectGetWidth(self.imageHostScrollView.frame), CGRectGetHeight(self.imageHostScrollView.frame));
    
    // add prevView as first in line
    UIImageView *prevView = [[UIImageView alloc] initWithFrame:rect];
    self.prevImgView = prevView;
    
    UIScrollView *scrView = [[UIScrollView alloc] initWithFrame:rect];
    [self.imageHostScrollView addSubview:scrView];
    
    scrView.delegate = self;
    [scrView addSubview:self.prevImgView];
    scrView.minimumZoomScale = 0.5;
    scrView.maximumZoomScale = 2.5;
    self.prevImgView.frame = scrView.bounds;
    
    
    // add currentView in the middle (center)
    rect.origin.x += CGRectGetWidth(self.imageHostScrollView.frame);
    UIImageView *currentView = [[UIImageView alloc] initWithFrame:rect];
    self.centerImgView = currentView;
    //    [self.imageHostScrollView addSubview:self.centerImgView];
    
    
    scrView = [[UIScrollView alloc] initWithFrame:rect];
    scrView.delegate = self;
    scrView.minimumZoomScale = 0.5;
    scrView.maximumZoomScale = 2.5;
    [self.imageHostScrollView addSubview:scrView];
    
    [scrView addSubview:self.centerImgView];
    self.centerImgView.frame = scrView.bounds;
    
    // add nextView as third view
    rect.origin.x += CGRectGetWidth(self.imageHostScrollView.frame);
    UIImageView *nextView = [[UIImageView alloc] initWithFrame:rect];
    self.nextImgView = nextView;
    //    [self.imageHostScrollView addSubview:self.nextImgView];
    
    scrView = [[UIScrollView alloc] initWithFrame:rect];
    [self.imageHostScrollView addSubview:scrView];
    scrView.delegate = self;
    scrView.minimumZoomScale = 1.0;
    scrView.maximumZoomScale = 2.5;
    
    [scrView addSubview:self.nextImgView];
    self.nextImgView.frame = scrView.bounds;
    
    
    
    // center the scrollview to show the middle view only
    [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
    self.imageHostScrollView.userInteractionEnabled=YES;
    self.imageHostScrollView.pagingEnabled = YES;
    self.imageHostScrollView.delegate = self;
    
    self.prevImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.centerImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.nextImgView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    //some data for testing
   // self.galleryImages = [[NSMutableArray alloc] init];
//    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"1" ofType:@"png"] atIndex:0];
//    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"2" ofType:@"png"] atIndex:1];
//    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"3" ofType:@"png"] atIndex:2];
//    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"4" ofType:@"png"] atIndex:3];
    
    self.currentIndex = 0;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.numberOfTapsRequired = 2;
    recognizer.delegate = self;
    [self.imageHostScrollView addGestureRecognizer:recognizer];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnImage)];
        tap.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:tap];
    
    
//    _scrollView.scrollEnabled = YES;
//    _scrollView.minimumZoomScale=1.0;
//    _scrollView.maximumZoomScale=4.0;
//        _scrollView.delegate=self;
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnImage)];
//    tap.numberOfTapsRequired = 1;
//    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    NSLog(@"%@",NSStringFromCGSize(self.imgView.size));
//    if (self.imgView.size.height > self.imgView.size.width) {
//        
//       
//        height = self.view.frame.size.height;
//        width = (self.imgView.size.width/_imgView.size.height) * self.view.frame.size.height;
//        
//    }
//    
//    
//    if (_imgView.size.width > _imgView.size.height) {
//        width  = self.view.frame.size.width;
//        height = (_imgView.size.height/_imgView.size.width) * self.view.frame.size.width;
//        // [_scrollView setContentSize:CGSizeMake(width, 1700)];
//    }
//    
//    if (_imgView.size.width == _imgView.size.height) {
//        
//        width = self.view.frame.size.width;
//        height =  self.view.frame.size.width;
//        //[_scrollView setContentSize:CGSizeMake(width, 1700)];
//
//    }
//    
//    x = (self.view.frame.size.width/2) - (width/2);
//    y = (self.view.frame.size.height/2) - (height/2);
//    
//    NSLog(@"%f %f",x,y);
//    
//    for (int i =0; i<self.slideZoomArray.count; i++) {
//        self.zoomImageView.frame = CGRectMake(x, y, width, height);
//       
//        
//        
//        
//        [self.zoomImageView sd_setImageWithURL:[NSURL URLWithString:[[self.slideZoomArray objectAtIndex:i] valueForKey:@"src"]] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
//        x=x+self.view.frame.size.width;
//         CGSize scrollableSize = CGSizeMake(1500,height);
//        [self.scrollView setContentSize:scrollableSize];
//        NSLog(@"%f",x);
//    }
    
    
    
    
    
    
    
//    imageHostScrollView_.contentOffset = CGPointMake(self.imgIndex*self.view.frame.size.width, 0);
//    
//    NSLog(@"%f",imageHostScrollView_.contentOffset.x);
    
  
    
}



//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//    return self.zoomImageView;
//}



//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollV withView:(UIView *)view atScale:(float)scale
//{
//    [_scrollView setContentSize:CGSizeMake(scale*320, scale*1700)];
//}



-(void)tapOnImage
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;
{
    //incase we are zooming the center image view parent
    if (self.centerImgView.superview == scrollView)
    {
        return self.centerImgView;
    }
    
    return nil;
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    //    CGFloat pageWidth = sender.frame.size.width;
    //    pageNumber_ = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
{
    CGFloat pageWidth = scrollView.frame.size.width;
    previousPage_ = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    //incase we are still in same page, ignore the swipe action
    if(previousPage_ == page) return;
    
    if(sender.contentOffset.x >= sender.frame.size.width)
    {
        //swipe left, go to next image
        [self setRelativeIndex:1];
        
        // center the scrollview to the center UIImageView
        [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
    }
    else if(sender.contentOffset.x < sender.frame.size.width)
    {
        //swipe right, go to previous image
        [self setRelativeIndex:-1];
        
        // center the scrollview to the center UIImageView
        [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
    }
    
    UIScrollView *scrollView = (UIScrollView *)self.centerImgView.superview;
    scrollView.zoomScale = 1.0;
}



- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    UIScrollView *scrollView = (UIScrollView*)self.centerImgView.superview;
    float scale = scrollView.zoomScale;
    scale += 1.0;
    if(scale > 2.0) scale = 1.0;
    [scrollView setZoomScale:scale animated:YES];
}



#pragma mark - image loading-

-(UIImage *)imageAtIndex:(NSInteger)inImageIndex
{
    // limit the input to the current number of images, using modulo math
    
    
   // inImageIndex = safeModulo(inImageIndex, [self totalImages]);
    
    inImageIndex = _imgIndex;
    
    NSString *filePath = [[self.slideZoomArray objectAtIndex:inImageIndex] valueForKey:@"src"];
    
    NSLog(@"filePath for image = %@",filePath);
    
     __block UIImage *image = nil;
    
    //Otherwise load from the file path
    
    if (nil == image)
    {
        NSString *imagePath = filePath;
        
        if(imagePath)
        {
            if([imagePath isAbsolutePath])
            {
                image = [UIImage imageWithContentsOfFile:imagePath];
            }
            else
            {
                image = [UIImage imageNamed:imagePath];
        
            }
            
           // if(nil == image){
            
            
            
            
            
        
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//               // NSString *imgURL = @"imagUrl";
//                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]];
//                
//                //set your image on main thread.
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    [centerImgView setImage:[UIImage imageWithData:data]];
//                    
//                   // image = [UIImage imageWithData:data];
//                });    
//            });
            
            
            
               [self.prevImgView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"loader.png"] options:SDWebImageRefreshCached];
            

            
            
                   //NSString *imgURL = filePath;
                //NSData *data1 = [NSData dataWithContentsOfURL:[NSURL URLWithString:filePath]];
                     
//                     //set your image on main thread.
//                     dispatch_async(dispatch_get_main_queue(), ^{
//                         //[nextImgView setImage:[UIImage imageWithData:data1]];
//                        
//                             image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]];
//                         
//                         
//                         self.centerImgView.image = image;
//                         
//                        // self.zoomImageView.image = image;
//                         
//                         
//                     });

            

          
       }
    }
    
    
    //return image;
    
   
    return prevImgView.image;
    
}


#pragma mark -

- (NSInteger)totalImages
{
    return [self.slideZoomArray count];
}

- (NSInteger)currentIndex
{
    
    return safeModulo(currentIndex_, [self totalImages]);
}

- (void)setCurrentIndex:(NSInteger)inIndex
{
    currentIndex_ = inIndex;
    
    
   // NSString *title = [NSString stringWithFormat:@"%d of %d",self.currentIndex+1,[self.slideZoomArray count]];
   // self.counterTitle.text = title;
    
    if([self.slideZoomArray count] > 0)
    {
        self.prevImgView.image   = [self imageAtIndex:[self relativeIndex:-1]];
        self.centerImgView.image = [self imageAtIndex:[self relativeIndex: 0]];
        self.nextImgView.image   = [self imageAtIndex:[self relativeIndex: 1]];
    }
}



- (NSInteger)relativeIndex:(NSInteger)inIndex
{
    return safeModulo(([self currentIndex] + inIndex), [self totalImages]);
}



- (void)setRelativeIndex:(NSInteger)inIndex
{
    [self setCurrentIndex:self.currentIndex + inIndex];
}



@end
