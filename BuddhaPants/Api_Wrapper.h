//
//  Api_Wrapper.h
//  BuddhaPants
//
//  Created by brst on 14/04/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MBProgressHUD;
@interface Api_Wrapper : NSObject
+(Api_Wrapper*)shared;

-(void)POST:(id)dict string:(NSString*)urlString completion:(void(^)(NSDictionary *json,BOOL sucess))completion;
-(void)PostApi:(id)dictData string:(NSString*)serverUrl completion:(void(^)(NSDictionary *json,BOOL sucess))completion;



-(void)alertMSG:(NSString*)title message:(NSString*)msg;
-(void)GET:(NSMutableDictionary*)dict string:(NSString*)urlString completion:(void(^)(NSDictionary *json ,BOOL success))completion;
@property (nonatomic, strong) NSDictionary *loginDict;

-(void)GETStore:(NSMutableDictionary*)dict string:(NSString*)urlString completion:(void(^)(NSDictionary *json ,BOOL success))completion;

-(void)PostApiVoting:(id)dictData string:(NSString*)serverUrl completion:(void(^)(NSDictionary *json,BOOL sucess))completion;
@property (nonatomic, strong) MBProgressHUD *hud;
@end
