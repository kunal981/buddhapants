//
//  LeftTableViewCell.h
//  VIDIT
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtpantsCell : UITableViewCell
{
    UIImageView *uploadImageView;
   
    UILabel *lblTittle,*lblUsername,*lblVoteValue1,*lblVoteValue2,*lblVoteValue3,*lblVoteValue4,*lblVoteValue5,*lblOneVote;
    
    UIButton * voteBtn;
    
//    UIButton * voteBtn1,* voteBtn2,* voteBtn3,* voteBtn4,* voteBtn5;
    
    UITextView * description;
}
@property(nonatomic,strong)UIImageView *uploadImageView;
@property(nonatomic,strong)UIButton *voteBtn ;
@property(nonatomic,strong)UITextView *description;

@property(nonatomic,strong)UILabel *lblTittle,*lblUsername,*lblVoteValue1,*lblVoteValue2,*lblVoteValue3,*lblVoteValue4,*lblVoteValue5;
@end
