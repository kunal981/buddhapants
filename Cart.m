//
//  Cart.m
//  BuddhaPants
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "Cart.h"


@implementation Cart

@dynamic color;
@dynamic deleteid;
@dynamic image;
@dynamic name;
@dynamic price;
@dynamic qty;
@dynamic size;
@dynamic totalprice;
@dynamic varId;
@dynamic sizeOfStr;
@dynamic varientIDStr;

@end
