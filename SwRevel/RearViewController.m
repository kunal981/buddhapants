
/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 Original code:
 Copyright (c) 2011, Philip Kluz (Philip.Kluz@zuui.org)
 
*/

#import "RearViewController.h"

#import "SWRevealViewController.h"


@interface RearViewController()
{
    NSInteger _presentedRow;
}

@end

@implementation RearViewController

@synthesize rearTableView = _rearTableView;


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
	
   // self.title = NSLocalizedString(@"Menu", nil);
    
    self.navigationController.navigationBar.hidden =YES;
    
     self.rearTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
    
    

    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor =  [UIColor whiteColor];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
    }
	
    NSString *text = nil;
    if (row == 0)
    {
        text = @"Home";
        cell.imageView.image = [UIImage imageNamed:@"Home"];
        
    }
    else if (row == 1)
    {
        text = @"Profile";
        cell.imageView.image = [UIImage imageNamed:@"Contact"];
    }
    else if (row == 2)
    {
          text = @"Support";
          cell.imageView.image = [UIImage imageNamed:@"Cog"];
    }
    else if (row==3){
          text = @"Sign out";
        
          cell.imageView.image = [UIImage imageNamed:@"Power"];
    }
    

    cell.textLabel.text = NSLocalizedString( text,nil );
	
    return cell;
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
//    SWRevealViewController *revealController = self.revealViewController;
//    
//    // We know the frontViewController is a NavigationController
//    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
//    NSInteger row = indexPath.row;
//    NSLog(@"%ld",(long)row);
//    // Here you'd implement some of your own logic... I simply take for granted that the first row (=0) corresponds to the "FrontViewController".
//    if (row == 1)
//    {
//        // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
//        if ( ![frontNavigationController.topViewController isKindOfClass:[ProfileViewController class]] )
//        {
//            ProfileViewController *firstVC = [[ProfileViewController alloc] init];
//            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:firstVC];
//            [revealController pushFrontViewController:navigationController animated:YES];
//        }
//        // Seems the user attempts to 'switch' to exactly the same controller he came from!
//        else
//        {
//            [revealController revealToggle:self];
//        }
//    }
//    else {
//        
//        
//            if (row == 0)
//            {
//                if ([UIScreen mainScreen].bounds.size.height == 568) {
//                    if ( ![frontNavigationController.topViewController isKindOfClass:[CategoriesViewController class]] )
//                    {
//                        CategoriesViewController *searchVC = [[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController" bundle:nil];
//                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
//                        [revealController pushFrontViewController:navigationController animated:YES];
//                    }
//                    // Seems the user attempts to 'switch' to exactly the same controller he came from!
//                    else
//                    {
//                        [revealController revealToggle:self];
//                    }
//                }
//                
//                else {
//                    if ( ![frontNavigationController.topViewController isKindOfClass:[CategoriesViewController class]] )
//                    {
//                        CategoriesViewController *searchVC = [[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController~6plus" bundle:nil];
//                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
//                        [revealController pushFrontViewController:navigationController animated:YES];
//                    }
//                    // Seems the user attempts to 'switch' to exactly the same controller he came from!
//                    else
//                    {
//                        [revealController revealToggle:self];
//                    }
//                    
//                    
//                }
//                
//                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
//              
//            }
//            
//            
//            if (row == 2)
//            {
//                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
//                if ( ![frontNavigationController.topViewController isKindOfClass:[SupportViewController class]] )
//                {
//                    SupportViewController *supportVC = [[SupportViewController alloc] init];
//                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:supportVC];
//                    [revealController pushFrontViewController:navigationController animated:YES];
//                }
//                // Seems the user attempts to 'switch' to exactly the same controller he came from!
//                else
//                {
//                    [revealController revealToggle:self];
//                }
//            }
//            
//            if (row == 3)
//            {
//                
//                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Are you sure you want to sign out ?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
//                [alert show];
//                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
////                if ( ![frontNavigationController.topViewController isKindOfClass:[TipsViewController class]] )
////                {
////                    TipsViewController *tipsVC = [[TipsViewController alloc] init];
////                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:tipsVC];
////                    [revealController pushFrontViewController:navigationController animated:YES];
////                }
////                // Seems the user attempts to 'switch' to exactly the same controller he came from!
////                else
////                {
////                    [revealController revealToggle:self];
////                }
//            }
//            
////            if (row == 4)
////            {
////                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
////                if ( ![frontNavigationController.topViewController isKindOfClass:[MyLiftsViewController class]] )
////                {
////                    MyLiftsViewController *liftVC = [[MyLiftsViewController alloc] init];
////                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:liftVC];
////                    [revealController pushFrontViewController:navigationController animated:YES];
////                }
////                // Seems the user attempts to 'switch' to exactly the same controller he came from!
////                else
////                {
////                    [revealController revealToggle:self];
////                }
////            }
////            if (row == 5)
////            {
////                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
////                if ( ![frontNavigationController.topViewController isKindOfClass:[MyProfileViewController class]] )
////                {
////                    MyProfileViewController *profileVC = [[MyProfileViewController alloc] init];
////                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:profileVC];
////                    [revealController pushFrontViewController:navigationController animated:YES];
////                }
////                // Seems the user attempts to 'switch' to exactly the same controller he came from!
////                else
////                {
////                    [revealController revealToggle:self];
////                }
////            }
//        
//            
//        //}
//    }
//}
//
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(buttonIndex==0){
//        SWRevealViewController *revealController = self.revealViewController;
//        
//        // We know the frontViewController is a NavigationController
//        UINavigationController *frontNavigationController = (id)revealController.frontViewController;
//        
//        if ([UIScreen mainScreen].bounds.size.height==568) {
//            if(![frontNavigationController.topViewController isKindOfClass:[LoginViewController class]]){
//                LoginViewController *signVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:signVC];
//                [revealController pushFrontViewController:navigationController animated:YES];
//            }
//            else
//            {
//                [revealController revealToggle:self];
//            }
//        }else{
//            if(![frontNavigationController.topViewController isKindOfClass:[LoginViewController class]]){
//                LoginViewController *signVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController~6plus" bundle:nil];
//                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:signVC];
//                [revealController pushFrontViewController:navigationController animated:YES];
//            }
//            else
//            {
//                [revealController revealToggle:self];
//            }
//        }
//        
//        
//        
//    }
//    
//}

@end