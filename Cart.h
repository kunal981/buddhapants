//
//  Cart.h
//  BuddhaPants
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Cart : NSManagedObject

@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSNumber * deleteid;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * qty;
@property (nonatomic, retain) NSString * size;
@property (nonatomic, retain) NSNumber * totalprice;
@property (nonatomic, retain) NSString * varId;
@property (nonatomic, retain) NSString * sizeOfStr;
@property (nonatomic, retain) NSString * varientIDStr;

@end
